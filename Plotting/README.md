# Code explained

The code is structured in 3 separate modules; plotter.py, plotutils.py and sample.py. The plots are built using two .json files, one defining the analysis regions and one defining the samples in ROOT format.  

## Defining samples
Each sample defined in the corresponding .json file is processed by the sample.py module. Each sample is defined by several options, the most important of which being the sample name, filepath, the tree in which the variables are stored and a plotting colour. Additionally, each sample should have a sample type defined, either data, signal or background. An example is given below.
```
"Data": {
            "sampletype": "data",
            "filepath": "/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data.root",
            "treename": "Nominal/BaseSelection_tree_Final",
            "colour": "1",
            "blind": "False",
            "scalefactor": 1.0,
            "unitynorm": "False",
            "includeweights": "",
            "excludeweights": ""
        }
```  
The name of the sample in the example above is "Data", and is of type 'data'. The type of the sample determines whether it is plotted as a marker (sampletype = "data"), added to a background stack (sampletype = "background"), or plotted as a signal line (sampletype = "signal"). The filepath and treename settings set the TFile and TTree from which to build the sample object. The colour setting determines the TColor with which the sample will be plotted (https://root.cern.ch/doc/master/classTColor.html#C01). The blind setting, either "False" or "True", determines if the sample should be included in the plot or not. This is useful for removing a sample from ALL plots. There is a separate blinding setting for each region for blinding the signal region, for example. Unitynorm will normalise the area of the sample distribution to 1,such that shape comparisons can be done between samples. Finally, includeweights and excludeweights enable additional weights to be applied to the sample from the tree, or weights that are included in the MCWEIGHTSTRING to be removed.  
## Defining regions
Regions are defined in a similar way to samples. An example of the region definition file is given below.
```
"regions": {
    "Preselection": {
    "cuts":"(1)*",
    "blinding_cuts": "",
    "blind": "False",
    "forcebins": ""
    },
    "Preselection_binned": {
        "cuts":"(1)*",
        "blinding_cuts": "",
        "blind": "True",
        "forcebins": "1600,1650,1675,1700,1725,1750,1775,1800,1850,2000"
    }
}
```
This example defines two regions, 'Preselection' and 'Preselection_binned'. The cuts setting defines the cuts which are applied to define the region. They should be formatted as "(cut1) * (cut2) * (cut3) * ... (cutN)". The blinding cuts setting enables additional selections to be added to a region. The blind setting, either "True" or "False", defines whether data will be plotted in a region or not. Finally, forcebins, which is a comma-separated list of bin edge values, enables a plot to be made with asymmetric bins. 
## Defining plots
At the start of plotter.py, there are a number of settings:  
```
# DEFINE JSON FILES
        
self.REGIONS_FILE = 'regions.json'
self.SAMPLES_FILE = 'samples.json'

# NORMALISATIONS

self.MCWEIGHTSTRING = '139. * mc_weight * weight'
self.DATAWEIGHTSTRING = '1'   

# SETTINGS

self.ATLASLABEL = 'Internal'
self.LUMILABEL = '#sqrt{s} = 13 TeV, 139 fb^{-1}'
self.PLOTLOG = True
self.PLOTARROWS = True
self.RATIOMIN = 0.0
self.RATIOMAX = 2.0
```
The first two settings determine the files which define the regions and samples. The MCWEIGHTSTRING setting defines how the MC sample predictions are normalised to the luminosity of the dataset. If additional weights are required/need to be removed for a specific MC sample, these can be defined in the corresponding .json file. Finally, plot aesthetics are defined in the final 5 settings.  
Individual plots are defined by calling the newplot method as follows:
```
self.newplot('object_m', 'm_{object}', 'MeV', 'Preselection', 20, 1600, 2000, 1E3, 1E7, False, 'Data:totalSM') 
```
The first argument defines which variable from the TTree is to be plotted. The second and third arguments determine the name for the x-axis and the units of the variable being plotted. 'Preselection' is the name of a region defined in regions.json, and defines what cuts are applied to the TTree before plotting. Different binnings can be defined, with the example above showing a plot with 20 bins between x values of 1600 and 2000 MeV. 1E3 and 1E7 define the minimum and maximum value in the y-axis. There are two final settings; plotsignals and ratioplot. The former, either True or False (note, not a string, but a boolean), will determine if the plot should include any signals defined in your samples file. Finally, the ratioplot setting allows a ratio plot to be included at the bottom of the canvas. The format for this is "numerator_sample:denominator_sample".  
