import ROOT, argparse
from ROOT import TFile, TTree, TH1D, TCanvas, THStack, gROOT, TLegend, TPad, TLine, TArrow

import os
import sample
import plotutils as plotutils

class plotter:

    def __init__(self):

        # DEFINE JSON FILES
        
        self.REGIONS_FILE = 'regions.json'
        self.SAMPLES_FILE = 'samples.json'

        # NORMALISATIONS

        self.MCWEIGHTSTRING = '139. * weight * xsec / ( 1000 * sumofweights )'
        self.DATAWEIGHTSTRING = '1'   

        # SETTINGS

        self.ATLASLABEL = 'Internal'
        self.LUMILABEL = '#sqrt{s} = 13 TeV, 139 fb^{-1}'
        self.PLOTLOG = True
        self.PLOTARROWS = True
        self.RATIOMIN = 0.0
        self.RATIOMAX = 2.0

    def run(self):

        # Set ATLAS style
        gROOT.LoadMacro("ATLAS_Style/atlasstyle-00-03-05/AtlasStyle.C")
        gROOT.LoadMacro("ATLAS_Style/atlasstyle-00-03-05/AtlasUtils.C")
        ROOT.SetAtlasStyle() 
        gROOT.SetBatch()

        # Run plotting
        
        #self.newplot('triplet_mos1_refitted', 'M_{OS, 1}', 'MeV', 'base_selection', 100, 0, 2000, 1E-3, 1E0, True) 
        #self.newplot('triplet_ref_m', 'M_{triplet}', 'MeV', 'base_selection', 100, 0, 4000, 1E-3, 1E0, True) 
        #
        #self.newplot('colljet_m', 'colljet_m', 'MeV', 'test', 100, 0, 30000, 1E-3, 1E0, True) 
        self.newplot('triplet_vertex_pval', 'triplet_vertex_pval', 'MeV', 'Preselection', 100, 0, 1, 1E2, 1E6, True) 
        #self.newplot('triplet_mos1_refitted', 'M_{OS, 1}', 'MeV', 'test', 100, 0, 2000, 1E-3, 1E0, True) 
        #self.newplot('triplet_mos2_refitted', 'M_{OS, 2}', 'MeV', 'test', 100, 0, 2000, 1E-3, 1E0, True) 
        #self.newplot('triplet_mss_refitted', 'M_{OS, ss}', 'MeV', 'test', 100, 0, 2000, 1E-3, 1E0, True) 
        self.newplot('triplet_ref_m', 'M_{triplet}', 'MeV', 'Preselection', 100, 0, 4000, 1E2, 1E6, True) 
        #self.newplot('triplet_ref_m', 'M_{triplet}', 'MeV', 'Loose_selection', 50, 1600, 2000, 1E-3, 1E0, True) 
        self.newplot('triplet_mos1_refitted', 'M_{OS, 1}', 'MeV', 'Preselection', 100, 0, 2000, 1E2, 1E6, True) 
        self.newplot('triplet_mos2_refitted', 'M_{OS, 2}', 'MeV', 'Preselection', 100, 0, 2000, 1E2, 1E6, True) 
        #self.newplot('triplet_mss_refitted', 'M_{SS}', 'MeV', 'Loose_selection', 100, 0, 2000, 1E-3, 1E0, True) 
        #self.newplot('object_m', 'Object mass', 'MeV', 'Preselection_binned', 20, 1600, 2000, 1E3, 1E7, ratioplot='Data:totalSM') 
                       
        #self.newplot('Tt*0.001', 'T_{T}', 'GeV', 'Loose_selection', 50, 0, 100, 1E-3, 1E0, True)
        #
    def newplot(self, variable, variablename, units, region, nbins, xmin, xmax, ymin = 0, ymax = 0, plotsignals = False, ratioplot = ''):
      
        # Create directory for plots
        if region not in os.listdir('./'):
            os.system('mkdir ./{}'.format(region))

        # Create instance of plot utils
        pu = plotutils.plotutils()

        # Setup canvas
        c = TCanvas('c','c',800,600)
        # Setup TLegend
        pu.setuplegend(y0 = 0.6)

        # Setup TPad
        pu.setuppad(self.PLOTLOG, ratioplot)

        # Setup plotutils
        if ('SR' in region or 'sr' in region or 'Sr' in region):
            forceblind = True
        else:
            forceblind = False

        pu.setup(c, self.SAMPLES_FILE, self.REGIONS_FILE, variable, variablename, units, region, self.MCWEIGHTSTRING, nbins, xmin, xmax, ymin, ymax, self.PLOTLOG, plotsignals, self.PLOTARROWS, forceblind)
        # Draw histograms
        pu.drawhists()
        #pu.fit('Data')

        # Draw legend
        pu.legend.Draw()

        # Do ratio plot
        if ratioplot != '':
            pu.plotdatamcratio(self.RATIOMIN, self.RATIOMAX, ratioplot.split(':')[0], ratioplot.split(':')[1])

        # Draw axis labels and ATLAS label    
        pu.decorate(self.ATLASLABEL, self.LUMILABEL, region)

        # Name output file
        suffix = ''
        if ratioplot != '':
            suffix = '_{}'.format(ratioplot)  
        filename_pdf = '{}_{}{}.pdf'.format(variable.split('*')[0], region, suffix)
        c.SaveAs(filename_pdf)

        # Move output file to region folder
        os.system('mv {} {}'.format(filename_pdf, region))

if __name__=='__main__':
    plotter().run()    
