from ROOT import TFile, TTree, TH1D, gROOT, TEntryList, gInterpreter
from ROOT import RooAbsRealLValue, RooRealVar, RooArgSet, RooDataSet
import ROOT

class sample:

    def __init__(self, name, metadata):

        self.name = name
        self.sampletype = metadata['sampletype']
        self.filepath = metadata['filepath']
        self.treename = metadata['treename']
        self.colour = int(metadata['colour'])
        self.blind = True if metadata['blind'] == 'True' else False
        self.scalefactor = metadata['scalefactor']
        self.includeweights = 1.0 if metadata['includeweights'] == '' else metadata['includeweights']
        self.excludeweights = 1.0 if metadata['excludeweights'] == '' else metadata['excludeweights']
        self.unitynorm = True if metadata['unitynorm'] == 'True' else False

    def __gethist__(self, variable, cuts, nominalweights, nbins, xmin, xmax, forcebins):
        
        # Get TFile and TTree
        tempfile = TFile.Open(self.filepath)
        temptree = tempfile.Get(str(self.treename))

        # Construct cuts and weights string
        if self.sampletype == 'data':
            nominalweights = '1'

        cutstring = '({}) * ({}) * ({}) * ({})'.format(cuts, nominalweights, self.includeweights, self.scalefactor)
        #print ("WEIGHTS", nominalweights, self.includeweights, self.scalefactor, cuts)

        # Create histogram using either forced binning or uniform binning
        temphistname = 'temphist_{}'.format(self.name)
        if len(forcebins) == 0:
            temphist = TH1D(temphistname,temphistname,nbins,xmin,xmax)
        else:
            numbins = len(forcebins) - 1
            temphist = TH1D(temphistname,temphistname,numbins,forcebins)

        # Fill histogram from TTree
        temphist.Sumw2()
        temphist.SetTitle(self.name)
        temptree.Draw('{}>>{}'.format(variable, temphistname),cutstring,'HIST')

        # Histogram aesthetics - markers for data, lines for signal, filled histogram for background
        gROOT.cd()
        newhist = temphist.Clone()
        if self.sampletype == 'data':
            newhist.SetMarkerSize(1.5)
        elif self.sampletype == 'background':
            newhist.SetFillColor(self.colour)
            newhist.SetLineColor(0)
        elif self.sampletype == 'signal':
            newhist.SetFillColor(0)
            newhist.SetLineColor(self.colour)   
            newhist.SetLineWidth(3)    
            newhist.SetLineStyle(1)    

        # Save histogram
        self.hist = newhist 


    def __getentrylist__(self, cuts, nominalweights):
        
        # Get TFile and TTree
        tempfile = TFile.Open(self.filepath)
        temptree = tempfile.Get(str(self.treename))

        # Construct cuts and weights string
        if self.sampletype == 'data':
            nominalweights = '1'

        # Get list of events passing the cuts
        cutstring = '({}) * ({}) * ({}) * ({})'.format(cuts, nominalweights, self.includeweights, self.scalefactor)

        entrylist = TEntryList("entrylist", "entrylist")

        temptree.Draw('>>{}'.format(entrylist.GetName()), cutstring, 'entrylist')

        # Save entry list
        self.entrylist = newentrylist

        return


    def __getroodataset__(self, variable, var, cuts, weight, weight_name, xmin, xmax, extraweight = 1.0, maxevents = None):

        # Get TFile and TTree
        tempfile = TFile.Open(self.filepath)
        temptree = tempfile.Get(str(self.treename))

        # Construct cuts and weights string
        if self.sampletype == 'data':
            weight_name = ""

        # Get list of events passing the cuts
        cutstring = '{}'.format(cuts)

        entrylist = TEntryList("entrylist", "entrylist")

        temptree.Draw('>>{}'.format(entrylist.GetName()), cutstring, 'entrylist')

        # Add weight to RooVar
        #weight = RooRealVar("weight", "weight", 1) 
        var_arg = RooArgSet(var, weight)
        dataset = RooDataSet("data", "data", var_arg, "weight")

        cpp = False
        if cpp:
            if maxevents is None: maxevents = -1
            self.dataset = ROOT.makedataset(temptree, entrylist, variable, var, xmin, xmax, extraweight, maxevents)
            print (">>> HERE")
            #ROOT.SetOwnership(self.dataset, True)
            return

        # Loop over event list and get varible from tree
        for i in range(entrylist.GetN() if maxevents is None else maxevents):
            temptree.GetEntry(entrylist.GetEntry(i))
            try:
                for t in range(len(getattr(temptree, variable))):
                    value = getattr(temptree, variable)[t]
                    if xmin < value < xmax:
                        RooAbsRealLValue.__assign__(var, value)
                        if weight_name:
                            #print ("WEIGHT", weight_name, extraweight)
                            dataset.add(var_arg, getattr(temptree, weight_name) * extraweight)
                        else:
                            dataset.add(var_arg, extraweight) 
            except TypeError:
                value = getattr(temptree, variable)
                RooAbsRealLValue.__assign__(var, value)
                dataset.add(var_arg, 1.0)

        self.dataset = dataset
        
        return
