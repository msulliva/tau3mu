#include <vector>
#include <iostream>

#include <TTree.h>
#include <TEntryList.h>
#include <RooRealVar.h>
#include <RooDataSet.h>


RooDataSet* makedataset(TTree* tree, TEntryList* entrylist, const char* variable, RooRealVar* var, float xmin, float xmax, float extraweight = 1, int maxevents = -1) {

  std::vector<float>* value;
  tree->SetBranchAddress(variable, &value);

  RooRealVar* weight = new RooRealVar("weight", "weight", 1); 
  RooArgSet*  var_arg = new RooArgSet(*var, *weight);
  RooDataSet*  dataset = new RooDataSet("data", "data", *var_arg, "weight");

  int nevts = (maxevents < 0 ? entrylist->GetN() : maxevents);
  for (int i=0; i < nevts; i++) {
    tree->GetEntry(entrylist->GetEntry(i));

    for (const float& v : *value) {
      if (xmin < v && v < xmax) {
	//std::cout << v << std::endl;
	*var = v;
	*weight = 1;
	dataset->add(RooArgSet(*var, *weight), extraweight);
      }
    }
    
    // For non vector
    //if (xmin < value && value < xmax) {
    //dataset->add(*var_arg, extraweight);
    //}
  }

  return dataset;
}
