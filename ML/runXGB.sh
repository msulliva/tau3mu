#!/bin/bash

#SAMPLE_DIR=/bundle/data/ATLAS/msullivan/Tau3Mu/samples
SAMPLE_DIR=/bundle/data/ATLAS/msullivan/Tau3Mu/ProductionJan2022/
ODD_DIR=./odd_xgb_slxycut_2021-12-16-13h22/
EVEN_DIR=./even_xgb_slxycut_2021-12-16-13h25/
ODD_MODEL=xgboost_model_odd_xgb_slxycut.json
EVEN_MODEL=xgboost_model_even_xgb_slxycut.json
CONFIG=train.yaml

python3 analysis_xgb.py -i $SAMPLE_DIR/hist-hf-Ds-tau3mu_slxycut_even.root -p $ODD_DIR --model $ODD_MODEL --addweight --train odd
python3 analysis_xgb.py -i $SAMPLE_DIR/hist-hf-Ds-tau3mu_slxycut_odd.root -p $EVEN_DIR --model $EVEN_MODEL --addweight --train even
#python3 analysis_xgb.py -i $SAMPLE_DIR/hist-bb-taunu_slxycut_even.root -p $ODD_DIR --model $ODD_MODEL --addweight --train odd
#python3 analysis_xgb.py -i $SAMPLE_DIR/hist-bb-taunu_slxycut_odd.root -p $EVEN_DIR --model $EVEN_MODEL --addweight --train even
#python3 analysis_xgb.py -i $SAMPLE_DIR/hist-data_unblinded_slxycut_even.root -p $ODD_DIR --model $ODD_MODEL --train odd
#python3 analysis_xgb.py -i $SAMPLE_DIR/hist-data_unblinded_slxycut_odd.root -p $EVEN_DIR --model $EVEN_MODEL --train even

#cd /hepstore/msullivan/Tau3Mu/tuples/20190318/
#cd /bundle/data/ATLAS/msullivan/Tau3Mu/samples/
#hadd -f hist-hf-Ds-tau3mu_slxycut_predictions.root hist-hf-Ds-tau3mu_odd_predictions.root hist-hf-Ds-tau3mu_even_predictions.root
#hadd -f hist-data_slxycut_predictions.root hist-data_unblinded_*predictions*
#hadd -f comb_signal_slxycut_predictions.root hist-bb*predictions* hist-hf*odd*predictions* hist-hf*even*predictions*
