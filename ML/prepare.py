import dataset_loader

# Setup which cuts to apply to the tuples and which features you want to use
cuts_sig_even = '(event_number % 2) == 0 && !(HT!=HT) && !(triplet_corrected_mass_diff!=triplet_corrected_mass_diff) && Sum$(triplet_muon_quality < 4) == 3 && object_n == 1 && triplet_pvalprod>0.00001 && abs(Sum$(triplet_charge))==1 && triplet_vertex_pval > 0.1 && triplet_mos1_refitted < 2900 && triplet_mos2_refitted < 2900 && triplet_mos1_refitted > 325 && triplet_mos2_refitted > 325 && triplet_mss_refitted > 325 && triplet_slxy_sig > 0' #normal cuts
cuts_sig_odd = '(event_number % 2) == 1 && !(HT!=HT) && !(triplet_corrected_mass_diff!=triplet_corrected_mass_diff) && Sum$(triplet_muon_quality < 4) == 3 && object_n == 1 && triplet_pvalprod>0.00001 && abs(Sum$(triplet_charge))==1 && triplet_vertex_pval > 0.1 && triplet_mos1_refitted < 2900 && triplet_mos2_refitted < 2900 && triplet_mos1_refitted > 325 && triplet_mos2_refitted > 325 && triplet_mss_refitted > 325 && triplet_slxy_sig > 0' #normal cuts
cuts_data_even = '(event_number % 2) == 0 && !(HT!=HT) && !(triplet_corrected_mass_diff!=triplet_corrected_mass_diff) && Sum$(triplet_muon_quality < 4) == 3 && object_n == 1 && triplet_pvalprod>0.00001 && abs(Sum$(triplet_charge))==1 && triplet_vertex_pval > 0.1 && triplet_mos1_refitted < 2900 && triplet_mos2_refitted < 2900 && triplet_mos1_refitted > 325 && triplet_mos2_refitted > 325 && triplet_mss_refitted > 325 && triplet_slxy_sig > 0 && (triplet_ref_m < 1690 || triplet_ref_m > 1870)' #normal cuts and blinding cuts
cuts_data_odd = '(event_number % 2) == 1 && !(HT!=HT) && !(triplet_corrected_mass_diff!=triplet_corrected_mass_diff) && Sum$(triplet_muon_quality < 4) == 3 && object_n == 1 && triplet_pvalprod>0.00001 && abs(Sum$(triplet_charge))==1 && triplet_vertex_pval > 0.1 && triplet_mos1_refitted < 2900 && triplet_mos2_refitted < 2900 && triplet_mos1_refitted > 325 && triplet_mos2_refitted > 325 && triplet_mss_refitted > 325 && triplet_slxy_sig > 0 && (triplet_ref_m < 1690 || triplet_ref_m > 1870)' #normal cuts and blinding cuts

features = [ 'triplet_vertex_pval', 'triplet_pvalprod', 'colljet_m', 'triplet_slxy_sig', 'triplet_sa0xy_sig_corrected', 'triplet_ref_pt', 'triplet_life_time_sig', 'triplet_chi2', 'triplet_chi2_4track', 'calo_isolation_02', 'calo_isolation_04', 'track_isolation_02', 'HT', 'Tt_hard', 'track_met_mt', 'triplet_corrected_mass_diff', 'triplet_missM2', 'object_track_met_dphi', 'object_calo_met_dphi', 'triplet_ref_eta', 'mc_channel_number', 'triplet_ref_m']

file_dir = '/bundle/data/ATLAS/msullivan/Tau3Mu/ProductionJan2022/'
tree = 'Nominal/BaseSelection_tree_Final'

dl = dataset_loader.dataset_loader( file_dir, features )

dl.load( 'hist-hf-Ds-tau3mu.root', tree, cuts_sig_even, 'hist-hf-Ds-tau3mu_slxycut_even' )
dl.load( 'hist-hf-Ds-tau3mu.root', tree, cuts_sig_odd, 'hist-hf-Ds-tau3mu_slxycut_odd' )
# dl.load( 'hist-bb-taunu.root', tree, cuts_sig_even, 'hist-bb-taunu_slxycut_even' )
# dl.load( 'hist-bb-taunu.root', tree, cuts_sig_odd, 'hist-bb-taunu_slxycut_odd' )
# #dl.load( 'hist-Wincl-taunu.root', tree, cuts_sig_even, 'hist-Wincl-taunu_even_tripletmass' )
# #dl.load( 'hist-Wincl-taunu.root', tree, cuts_sig_odd, 'hist-Wincl-taunu_odd' )
# dl.load( 'hist-data.root', tree, cuts_data_even, 'hist-data_slxycut_even' )
# dl.load( 'hist-data.root', tree, cuts_data_odd, 'hist-data_slxycut_odd' )

#dl.load( 'hist-data.root', tree, cuts_sig_even, 'hist-data_unblinded_slxycut_even' )
#dl.load( 'hist-data.root', tree, cuts_sig_odd, 'hist-data_unblinded_slxycut_odd' )

# FCC-hh

#file_dir = '/hepstore/msullivan/FCC/samples/'
#tree = 'events'
#fcc_features = ['bjet_pt[0]','bjet_pt[1]','met[0]','met_phi[0]']

#cuts = 'n_bjets == 2'

#loader = dataset_loader.dataset_loader( file_dir, fcc_features )

#loader.load_rdf( 'pwp8_pp_hh_lambda100_5f_hhbbtata.root', tree, cuts, 'hhbbtata' )
#loader.load_rdf( 'mgp8_pp_tt012j_5f.root', tree, cuts, 'ttbar' )