import pandas as pd
import corrmatrix
import root_numpy

from ROOT import *

def main():

    tfile1 = TFile('/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf-Ds-tau3mu_predictions.root')
    ttree = tfile1.Get('BaseSelection_tree_Final')

    features = [ 'triplet_vertex_pval', 'triplet_pvalprod', 'colljet_m', 'triplet_slxy_sig', 'triplet_sa0xy_sig_corrected', 'triplet_ref_pt', 'triplet_life_time_sig', 'triplet_chi2', 'triplet_chi2_4track', 'calo_isolation_02', 'calo_isolation_04', 'track_isolation_02', 'HT', 'Tt_hard', 'track_met_mt', 'triplet_corrected_mass_diff', 'triplet_missM2']
    pred = 'nn_output'



if __name__ == '__main__':

    main()