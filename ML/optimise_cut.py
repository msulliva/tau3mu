import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import root_numpy
import argparse

from ROOT import TFile

def main():

    # Setup arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--signal')
    parser.add_argument('-b', '--background')
    parser.add_argument('-t', '--tree', default='BaseSelection_tree_Final')
    parser.add_argument('-v', '--variable', default='nn_output')
    parser.add_argument('--BR', type=float, default=1.0)
    args = parser.parse_args()
    # Open ROOT files
    sig_file = TFile( args.signal )
    bkg_file = TFile( args.background )
    sig_tree = sig_file.Get( args.tree )
    bkg_tree = bkg_file.Get( args.tree )
    # Get array of NN variable
    sig_array = root_numpy.tree2array( sig_tree, branches=args.variable )
    sig_wgt   = (139. * 0.389 * args.BR) * root_numpy.tree2array( sig_tree, branches='new_xsec' ) / root_numpy.tree2array( sig_tree, branches='new_sumofweights' )
    bkg_array = root_numpy.tree2array( bkg_tree, branches=args.variable )
    # Histogram time
    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    bins = np.linspace(0,1,101)
    bkg_yield, bkg_bins,_ = ax1.hist( bkg_array, bins=bins, alpha=1, label='Bkg', histtype='step' )
    sig_yield, sig_bins,_ = ax1.hist( sig_array, bins=bins, weights=sig_wgt, alpha=1, label='Sig (HF, BR={})'.format( args.BR ), histtype='step' )
    ax1.legend(loc='best')
    ax1.set_ylabel('Events / bin')
    ax1.set_yscale('log')
    sigs, index = calculate_significance( sig_yield, bkg_yield )
    print(sigs)
    ax2.scatter( np.linspace(0, 0.99, len(sigs)), sigs, label='Optimal cut: {} ({})'.format(bins[index], round(sigs[index], 3)) )
    ax2.legend(loc='best')
    ax2.set_xlabel('DNN output')
    ax2.set_ylabel('s/sqrt(b)')
    plt.savefig('significance_scan.png', dpi=300)

def calculate_significance( sig_yield, bkg_yield ):

    significances = []
    max_sig = 0
    index = -99
    for i in range(len(sig_yield)):

        s = sum(sig_yield[i:len(sig_yield)])
        b = sum(bkg_yield[i:len(bkg_yield)])
        print('{}, {}, {}'.format(s, b, i))
        if s > 0 and b > 0:
            sig = s/np.sqrt(b)#b * (0.1*b)*(0.1*b))
        else:
            sig = 0

        if sig > max_sig:
            max_sig = sig
            index = i

        significances.append( sig )

    return significances, index

if __name__ == '__main__':
    main()