import ROOT, io, sys, os
import numpy as np
import matplotlib
#matplotlib.use("TkAgg")
#import matplotlib.pyplot as pt
#import time
from tensorflow.keras.models import load_model
from ROOT import gROOT, TFile, TTree, TH1D, TColor, TMVA, TString
from random import shuffle
from array import array
import root_numpy
#from sklearn import preprocessing
import progressbar
import argparse
#import xgboost as xgb

import utils

__author__ = "Matt Sullivan"
__doc__ = ""

def main():

	# set random seed
	test_seed = 42
	from numpy.random import seed
	seed(test_seed)
	from tensorflow.random import set_seed
	set_seed(test_seed)

	parser = argparse.ArgumentParser(description='XGBoost/Keras analysis script')

	parser.add_argument('-i', action="store", dest="input_file")
	parser.add_argument('-t', action="store", dest="tree_name", default="BaseSelection_tree_Final")
	parser.add_argument('-c', action="store", dest="config_name", default="train.yaml")
	parser.add_argument('-p', action="store", dest="config_path")
	parser.add_argument('--model', action="store", dest="model", default="keras_model.h5")
	parser.add_argument('--train', action="store", dest="train")
	parser.add_argument('--addweight', action="store_true")
	args = parser.parse_args()

	train_utils = utils.utils()
	config = str(args.config_path) + str(args.config_name)
	train_utils.read_config(config, args.train)

	# Copy input root file

	newFile = args.input_file.split(".root")[0] + "_predictions.root"
	command = "cp " + args.input_file + " " + newFile
	os.system(command)

	# Load ROOT file 

	f = TFile(newFile, 'update')
	t = f.Get(args.tree_name)
	cuts = ''#'Sum$(triplet_muon_quality < 4) == 3 && object_n == 1 && triplet_vertex_pval>0.05 && triplet_pvalprod>0.00001 && colljet_m != 0 && colljet_m<10000 && triplet_slxy_sig>1' # cuts to be applied to new tree
	new_tree = t.CopyTree( cuts ) 
	new_tree.Write()

	train_utils.info_message( "Prepared new file with trimmed tree" )

	# Setup output arrays and branches
	weights = array('f', [0])
	nn_output = array('f', [0])
	if args.addweight:
		weights_branch = new_tree.Branch("lumi_scale",weights,"lumi_scale/F")
	nn_output_branch = new_tree.Branch("nn_output",nn_output,"nn_output/F")

	# Load Keras model
	keras_model = load_model('{}/{}'.format(args.config_path, args.model))

	train_utils.info_message( "Loading data for prediction" )

	features = []
	for feature in train_utils.feature_list:
		if 'dr' in feature:
			temp_feature = feature.split('_')[-1]
			feature = 'triplet_muon_dr[{}]'.format( temp_feature )
			print( feature )
		features.append( feature.replace( ' ','' ))

	tree_data = root_numpy.tree2array( new_tree, branches=features )
	data = []
	for entry in range(new_tree.GetEntries()):
		event = []
		for column in tree_data[ entry ]:
			event.append( column[0] )
		data.append( event )
	data = np.asarray(data)
	#shuffle(data)

	train_utils.info_message( "Doing predictions" )

	X = train_utils.standardise_data(data[:,0:train_utils.n_features], args.config_path+'scale_params.txt')

	keras_prediction = []
	for entry in keras_model.predict(X):
		keras_prediction.append(entry)

	train_utils.info_message( "Filling tree" )
	for entry in range(new_tree.GetEntries()):
		new_tree.GetEntry(entry)
		nn_output[0] = keras_prediction[entry][-1]
		nn_output_branch.Fill()
		if args.addweight:
			weight = new_tree.GetLeaf('weight').GetValue()
			xsec = new_tree.GetLeaf('new_xsec').GetValue()
			sow = new_tree.GetLeaf('new_sumofweights').GetValue()
			weights[0] = (0.389 * weight * xsec) / (sow)
			#weights[0] = (weight * xsec) / (sow)
			weights_branch.Fill()

	new_tree.Write()
	f.Close()
	train_utils.info_message('Finished analysis, exiting')

if __name__ == '__main__':
    main()
