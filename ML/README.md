# Preparing files for use in NN code

To prepare files for use in the DNN code, you need to convert and flatten your files, as in prepare.py. This code converts ROOT files into pickle files, applying cuts to the trees and removing any unnecessary variables.

## Flat ntuples

If you're using a standard ROOT file, i.e. one with TTrees, the following gives an example of how to prepare the samples. First, define your cuts and list of features as shown below. Then, create an instance of the dataset_loader class, passing in the file directory list of features. Then, use the .load function, passing in the name of the name (must be present in the file directory), the name of the TTree, the name of the cuts string and finally the name of the output file. The output is saved in .pkl format in the file directory given earlier, containing a flat pandas dataframe of the features. 
```
import dataset_loader

file_dir = '/hepstore/msullivan/Tau3Mu/tuples/20190318/'
tree = 'Nominal/BaseSelection_tree_Final'

cuts = 'Sum$(triplet_muon_quality < 4) == 3 && object_n == 1 && triplet_vertex_pval>0.05 && triplet_pvalprod>0.00001 && colljet_m != 0 && colljet_m<10000 && triplet_slxy_sig>1'

features = [ 'triplet_vertex_pval', 'triplet_pvalprod', 'colljet_m', 'triplet_slxy_sig', 'triplet_sa0xy_sig_corrected', 'triplet_ref_pt', 'triplet_life_time_sig', 'triplet_chi2', 'triplet_chi2_4track', 'calo_isolation_02', 'calo_isolation_04', 'track_isolation_02', 'HT', 'Tt_hard', 'track_met_mt', 'triplet_corrected_mass_diff', 'triplet_missM2', 'mc_channel_number']

dl = dataset_loader.dataset_loader( file_dir, features )
dl.load( 'hist-hf-Ds-tau3mu.root', tree, cuts, 'hist-hf-Ds-tau3mu_prepared' )
dl.load( 'hist-data.root', tree, cuts, 'hist-data_prepared' )

```
## ROOT dataframes

When using a ROOT file containing a ROOT dataframe, the code needed to prepare the files changes slightly. Simply use dl.load_rdf, passing in the same arguments as previously. 

## Preparing samples for tau -> 3 mu

To prepare training/testing samples with fixed cc/bb fractions, run:
```
python3 prepare_train_test.py
```
This will create fixed training and test samples where the signal is composed of 80% cc and 20% bb events.

# Running training

To run the training, for example, use the following command:

```
python3 train_keras.py -c train.yaml -t train0
```

An example of a training configuration is shown in train.yaml. The above command will run the configuration 'train0' from the train.yaml file. 

## Input files

To run a training with inputs corresponding to different classes, pass in the file names into the files section, and set fixed_samples to false.
```
    classes: [0, 1]
    files: [hist-data_prepared, hist-hf-Ds-tau3mu_prepared]
    fixed_samples: False
    class_weights: {0: 1.0, 1: 1.0}
    param: ''
```
If you wish to use pre-produced training and test samples, do the following:
```
    classes: [0, 1]
    files: [train_set, test_set]
    fixed_samples: True
    class_weights: {0: 1.0, 1: 1.0}
    param: ''
```
NOTE: the first file in the files list will be taken as the training set, and the second file taken as the test set. Set fixed_samples to True. 
## Running as a PNN

To run as a PNN, the parameter of interest must be included in the config as follows:
```
    classes: [0, 1]
    files: [hist-data_prepared, hist-hf-Ds-tau3mu_prepared]
    fixed_samples: False
    class_weights: {0: 1.0, 1: 1.0}
    param: 'bsm_mass'
```
# Producing post-training plots

To produce the post-training plots, run:
```
python3 make_plots.py -d train0_<TIMESTAMP>
```
The only argument to be passed in is the directory which is produced after the training. This will produce plots of the input variables, a correlation matrix of the inputs, a ROC curve and a plot of the NN score on the training and test sets.

# Running analysis code
