import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

def merge( bkg, HF, directb, W, name ):

    # Open files
    bkg_file = bkg #'/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data_prepared.pkl'
    HF_file = HF #'/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf-Ds-tau3mu_prepared.pkl'
    directb_file = directb
    W_file = W

    bkg_data = pd.read_pickle( bkg_file )
    HF_data = pd.read_pickle( HF_file )
    directb_data = pd.read_pickle( directb_file )
    W_data = pd.read_pickle( W_file )

    # prepare labels
    bkg_labels = np.empty( bkg_data.shape[0] )
    bkg_labels.fill( 0 )
    bkg_data['label'] = bkg_labels

    HF_labels = np.empty( HF_data.shape[0] )
    HF_labels.fill( 1 )
    HF_data['label'] = HF_labels

    directb_labels = np.empty( directb_data.shape[0] )
    directb_labels.fill( 1 )
    directb_data['label'] = directb_labels

    W_labels = np.empty( W_data.shape[0] )
    W_labels.fill( 2 )
    W_data['label'] = W_labels

    full_set = pd.concat( [bkg_data, HF_data, directb_data, W_data])
    full_set.to_pickle( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/train_set_multi_{}.pkl'.format( name ) )

    print( '------' )

bkg_odd = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-data_odd.pkl' 
HF_odd = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-hf-Ds-tau3mu_odd.pkl' 
directb_odd = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-bb-taunu_odd.pkl'
W_odd = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-Wincl-taunu_odd.pkl'
bkg_even = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-data_even.pkl' 
HF_even = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-hf-Ds-tau3mu_even.pkl' 
directb_even = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-bb-taunu_even.pkl'
W_even = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-Wincl-taunu_even.pkl'

merge( bkg_odd, HF_odd, directb_odd, W_odd, 'odd' )
merge( bkg_even, HF_even, directb_even, W_even, 'even' )
