import pandas as pd
import numpy as np
import matplotlib.pyplot as plt    

class corrmatrix:
    
    def draw(self, df, name):

            columns = df.columns

            arr = np.empty((len(columns),len(columns)))

            for x, column_1 in enumerate(columns):

                for y, column_2 in enumerate(columns):
                    
                    xi = df[column_1]
                    yi = df[column_2]
                    xbar = df[column_1].mean()
                    ybar = df[column_2].mean()
                    
                    corr = ( (xi.subtract(xbar)*yi.subtract(ybar)).sum() ) / ( np.sqrt( (xi.subtract(xbar)*xi.subtract(xbar)).sum() ) * np.sqrt( (yi.subtract(ybar)*yi.subtract(ybar)).sum() ) )
                    arr[x, y] = corr

            fig, ax = plt.subplots(figsize=(30, 30))
            im = ax.imshow( arr )

            im.set_clim(-1, 1)

            ax.set_xticks(np.linspace(0,len(columns)-1,len(columns)))
            ax.set_xticklabels(columns, rotation=90)
            ax.set_yticks(np.linspace(0,len(columns)-1,len(columns)))
            ax.set_yticklabels(columns)

            n_columns = len(columns)
            for x in range(n_columns):
                for y in range(n_columns):
                    text = ax.text(y, x, round(arr[x, y], 3), ha="center", va="center", color="w")

            fig.tight_layout()
            fig.colorbar(im, ax=ax, shrink=0.75)
            fig.show()
            fig.subplots_adjust(bottom=0.1, left=0.15)
            fig.savefig('{}.png'.format(name),dpi=200)