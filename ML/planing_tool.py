import multiprocessing as mp
import argparse
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from keras import regularizers
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense,Activation,Dropout
from keras.regularizers import l2
from tensorflow.keras.optimizers import Adam
from keras.utils import plot_model, to_categorical
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from sklearn.metrics import roc_curve, auc, accuracy_score

def standardise_data( data, file, index ):

    scale_file = open( file,'r')
    if scaler == 'minmax':
        for line in scale_file:
            if 'maxs' in line:
                maxs = np.fromstring(str(line.replace('maxs: ','').replace('[','').replace(']','')), sep=',')
            elif 'mins' in line:
                mins = np.fromstring(str(line.replace('mins: ','').replace('[','').replace(']','')), sep=',')
        data_standardised = matt_minmax_scaler(maxs, mins, data, index)	
    elif scaler == 'standard':	
        for line in scale_file:
            if 'means' in line:
                means = np.fromstring(str(line.replace('means: ','').replace('[','').replace(']','')), sep=',')
            elif 'stds' in line:
                stds = np.fromstring(str(line.replace('stds: ','').replace('[','').replace(']','')), sep=',')	
        data_standardised = matt_std_scaler(means, stds, data, index)	
    return data_standardised

def matt_std_scaler( means, stds, data, index ):

    temp = []
    for row in range(data.shape[0]):
        entry = []
        for column in range(data.shape[1]):
            value = data[row, column]
            if column == index:
                np.random.seed()
                value = np.random.normal(means[column], stds[column])
            else:
                value = (value - means[column]) / stds[column] 
            entry.append(value)	
        temp.append(entry)	
    temp = np.asarray(temp)	
    return temp

def matt_minmax_scaler(maxs, mins, data, index ):

    temp = []
    for row in range(data.shape[0]):
        entry = []
        for column in range(data.shape[1]):
            value = data[row, column]
            if column == index:
                value = np.random.seed()
                value = np.random.uniform(0,1)
            else:
                value = (value - mins[column]) / (maxs[column] - mins[column])
            entry.append(value)	
        temp.append(entry)	
    temp = np.asarray(temp)	
    return temp   

def plane( index ):

    # Load data
    #data, labels = utils.load_pickles()
    #X_train, X_test, y_train, y_test = train_test_split(data.values, labels, test_size=0.33, random_state=7) 
    train_set = pd.read_pickle( training_file )
    test_set = pd.read_pickle( test_file )
    var = train_set.columns[index]

    print( 'Will plane: {}'.format(var))

    X_train = train_set.drop( ['label','preds'], axis=1).values
    X_test = test_set.drop( ['label','preds'], axis=1).values
    y_train = train_set['label'].values
    y_test = test_set['label'].values

    # Keras: setup
    model = load_model( 'train1_2021-2-24-13h49//keras_model_train1.h5' )

    train_score, train_acc = model.evaluate(standardise_data(X_train, scale_file, index), y_train)
    test_score, test_acc = model.evaluate(standardise_data(X_test, scale_file, index), y_test)

    return (var, train_score, train_acc, test_score, test_acc)

def plot_inputs( training_file, test_file, scale_file, n_features ):

    train_set = pd.read_pickle( training_file )
    test_set = pd.read_pickle( test_file )

    columns = train_set.columns

    X_train_sig = train_set[train_set['label'] == 1]
    X_train_bkg = train_set[train_set['label'] == 0]
    X_test_sig = test_set[test_set['label'] == 1]
    X_test_bkg = test_set[test_set['label'] == 0]

    X_train_sig = X_train_sig.drop( ['label','preds'], axis=1).values
    X_train_bkg = X_train_bkg.drop( ['label','preds'], axis=1).values
    X_test_sig = X_test_sig.drop( ['label','preds'], axis=1).values
    X_test_bkg = X_test_bkg.drop( ['label','preds'], axis=1).values
    index = -99

    train_std_data_sig = standardise_data( X_train_sig, scale_file, index )
    train_std_data_bkg = standardise_data( X_train_bkg, scale_file, index )
    test_std_data_sig = standardise_data( X_test_sig, scale_file, index )
    test_std_data_bkg = standardise_data( X_test_bkg, scale_file, index )

    for i in range(n_features):

        plt.gcf().clear()

        train_data_sig = train_std_data_sig[:,i]
        train_data_bkg = train_std_data_bkg[:,i]
        test_data_sig = test_std_data_sig[:,i]
        test_data_bkg = test_std_data_bkg[:,i]

        bins_lo = min(train_data_sig.min(), test_data_sig.min(), train_data_bkg.min(), test_data_bkg.min())
        bins_hi = min(train_data_sig.max(), test_data_sig.max(), train_data_bkg.max(), test_data_bkg.max())
        n_bins = 50
        bins = np.linspace(bins_lo, bins_hi, n_bins+1)
        plt.hist( train_data_sig, bins, label='Training (sig)', color='tab:orange', alpha=0.5 )
        plt.hist( train_data_bkg, bins, label='Training (bkg)', color='tab:blue', alpha=0.5 )
        plt.hist( test_data_sig, bins, label='Test (sig)', color='tab:orange', histtype='step' )
        plt.hist( test_data_bkg, bins, label='Test (bkg)', color='tab:blue', histtype='step' )
        plt.yscale('log')
        plt.xlabel(columns[i])
        plt.legend(loc='best')
        plt.savefig('{}_transformed.png'.format(columns[i]), dpi=200)

def make_plot( results ):

    fig = plt.figure(figsize=(10,10))
    labels = []
    train_accs = []
    test_accs = []
    for index, result in enumerate(results):
        var = result[0]
        train_acc = result[2]
        test_acc = result[4]
        train_accs.append( train_acc )
        test_accs.append( test_acc )
        label = var
        plt.scatter( index, train_acc, c='tab:orange', s=40)
        plt.scatter( index, test_acc, c='tab:blue', s=40)
        plt.text( index-0.5, 0.9, '{}'.format( round(train_acc, 3)), rotation=45., rotation_mode='anchor', color='tab:orange')
        plt.text( index-0.5, 0.80, '{}'.format( round(test_acc, 3)), rotation=45., rotation_mode='anchor', color='tab:blue')
        labels.append( label )
    plt.xticks(features, labels, rotation='vertical')
    ymin = min( min(train_accs), min(test_accs) )
    ymax = max( max(train_accs), max(test_accs) )
    plt.ylim(0.8*ymin, 0.95)
    #plt.legend(ncol=2, loc='best')
    plt.tight_layout()
    #plt.margins(0.1)
    plt.savefig('planing_result.png', dpi=500)

# To be automated
classes = 0,1
training_file = 'train1_2021-2-24-13h49/train_set.pkl' 
test_file = 'train1_2021-2-24-13h49/test_set.pkl' 
scale_file = 'train1_2021-2-24-13h49/scale_params.txt'
n_features = 17
scaler = 'minmax'
features = np.linspace(0, n_features-1, n_features, dtype=int)

import os
os.system('mkdir planing')

pool = mp.Pool( n_features )
results = pool.map( plane, features )

for result in results:
    print( result )

make_plot( results )

plot_inputs( training_file, test_file, scale_file, n_features )
os.system('mv *.png planing/')