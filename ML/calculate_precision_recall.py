import numpy as np
import pandas as pd
import ROOT

data_file = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/11June_allvars/hist-data_predictions.root'
sig_file = '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/11June_allvars/comb_signal_predictions.root'

data_tfile = ROOT.TFile( data_file )
sig_tfile = ROOT.TFile( sig_file )

data_tree = data_tfile.Get('BaseSelection_tree_Final')
sig_tree = sig_tfile.Get('BaseSelection_tree_Final')

ntp = 0
nfp = 0
ntn = 0
nfn = 0

# Get ntp
ntp = sig_tree.GetEntries('nn_output > 0.5')
nfp = data_tree.GetEntries('nn_output > 0.5')
ntn = data_tree.GetEntries('nn_output <= 0.5')
nfn = sig_tree.GetEntries('nn_output <= 0.5')

print( ntp )
print( nfp )
print( ntn )
print( nfn )

precision = ntp / ( ntp + nfp )
recall = ntp / ( ntp + nfn )
f1 = 2 * ( precision * recall ) / ( precision + recall )

print( 'Precision: {}'.format( precision ) )
print ( 'Recall: {}'.format( recall ) )
print( 'F1 score: {}'.format( f1 ) )