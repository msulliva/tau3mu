

# Libraries
import argparse
import xgboost
import utils
import matplotlib.pyplot as plt
import sklearn
import numpy as np

__author__ = "Matt Sullivan"
__doc__ = ""

def f1(y_pred, dtrain):

    y_true = dtrain.get_label()
    return 'f1', sklearn.metrics.f1_score( y_true, np.round(y_pred), average='micro')


def main():

    # Initialise utils
    train_utils = utils.utils()

    # Load config file
    parser = argparse.ArgumentParser(description='XGBoost training script')
    parser.add_argument('-c', action="store", dest="config_file")
    parser.add_argument('-t', action="store", dest="training")
    parser.add_argument('--gpu', action="store_true")
    args = parser.parse_args()
    train_utils.read_config(args.config_file, args.training)

    # Make string from current time + date
    unique_string = train_utils.set_name()

    # Load data
    if train_utils.sample_weights == '':
        X_train, X_test, y_train, y_test, columns = train_utils.load_pickles()
    else:
        X_train, X_test, y_train, y_test, w_train, w_test, columns = train_utils.load_pickles()

    train_sets = []
    train_labels = []
    train_preds = []
    test_sets = []
    test_labels = []
    test_preds = []

    dtrain = xgboost.DMatrix(X_train, label=y_train, feature_names = columns)
    dtest = xgboost.DMatrix(X_test, label=y_test, feature_names = columns)

    num_round = 10000
    param = {'max_depth': 8, 'eta': 0.1, 'subsample': 0.8, 'objective': 'multi:softprob', 'num_class': 3}#, 'disable_default_eval_metric': 1}
    param['nthread'] = 4
    param['eval_metric'] = ['merror', 'mlogloss']
    #param['scale_pos_weight'] = 6.0
    if args.gpu:
        param['tree_method'] = 'gpu_hist'
    evallist = [ (dtrain, 'train'), (dtest, 'eval') ]

    #clf = xgboost.XGBClassifier( param, n_esimators = num_round )
    #clf.fit( X_train, y_train, evallist, eval_metric = f1)

    bst = xgboost.train(param, dtrain, num_round, evallist, early_stopping_rounds=10 )
    
    bst.dump_model('dump.raw.txt')

    print( 'SCORE: {}'.format(bst.best_score ) )

    train_pred = bst.predict( dtrain, iteration_range=(0, bst.best_iteration) )
    test_pred = bst.predict( dtest, iteration_range=(0, bst.best_iteration) )

    # add outputs
    train_sets.append( X_train )
    test_sets.append( X_test )
    train_labels.append( y_train )
    test_labels.append( y_test )
    train_preds.append( train_pred )
    test_preds.append( test_pred )

    # make feature importance plot
    xgboost.plot_importance(bst)
    plt.tight_layout()
    plt.savefig('feature_importance.png', dpi=100)
    
    # dump model
    bst.save_model('xgboost_model_{}.json'.format(train_utils.model_name))

	# write output
    if train_utils.sample_weights == '':
        w_train = ''
    train_utils.write_output( train_sets, test_sets, train_labels, test_labels, train_preds, test_preds, w_train, columns  ) 

if __name__ == '__main__':
    main()