

# Libraries
import argparse
import xgboost
import utils
import matplotlib.pyplot as plt
import sklearn
from sklearn.metrics import confusion_matrix, roc_curve, roc_auc_score, f1_score, precision_recall_curve
from sklearn.model_selection import GridSearchCV
import numpy as np
import itertools

__author__ = "Matt Sullivan"
__doc__ = ""

def f1(y_pred, dtrain):

    y_true = dtrain.get_label()
    return 'f1', sklearn.metrics.f1_score( y_true, np.round(y_pred), average='micro')


def main():

    # Initialise utils
    train_utils = utils.utils()

    # Load config file
    parser = argparse.ArgumentParser(description='XGBoost training script')
    parser.add_argument('-c', action="store", dest="config_file")
    parser.add_argument('-t', action="store", dest="training")
    parser.add_argument('--gpu', action="store_true", dest="gpu")
    args = parser.parse_args()
    train_utils.read_config(args.config_file, args.training)

    # Make string from current time + date
    unique_string = train_utils.set_name()

    # Load data
    if train_utils.sample_weights == '':
        X_train, X_test, y_train, y_test, columns = train_utils.load_pickles()
    else:
        X_train, X_test, y_train, y_test, w_train, w_test, columns = train_utils.load_pickles()

    train_sets = []
    train_labels = []
    train_preds = []
    test_sets = []
    test_labels = []
    test_preds = []

    dtrain = xgboost.DMatrix(X_train, label=y_train, feature_names = columns)
    dtest = xgboost.DMatrix(X_test, label=y_test, feature_names = columns)

    num_round = 50000
    #param['eval_metric'] = ['auc','logloss']
    #param['scale_pos_weight'] = 6.0
    #if args.gpu:
        #param['tree_method'] = 'gpu_hist'
    evallist = [ (dtrain, 'train'), (dtest, 'eval') ]

    #clf = xgboost.XGBClassifier( param, n_esimators = num_round )
    #clf.fit( X_train, y_train, evallist, eval_metric = f1)

    # cv_params = {'max_depth': [8,10,12,14,16], 'min_child_weight': [2,3,4], 'learning_rate': [0.2,0.25,0.3]}
    # fix_params = {'seed': 42, 'subsample': 0.8, 'n_estimators': 100, 'objective': 'binary:logistic', 'tree_method': 'gpu_hist'}
    # csv = GridSearchCV(xgboost.XGBClassifier(**fix_params), cv_params, scoring = 'accuracy', cv = 5)

    # csv.fit( X_train, y_train )
    # print( csv.best_params_ )

    params_dict = {
        "max_depth": [4,6,8,10,12,14],
        "min_child_weight": [2,3,4],
        "learning_rate": [0.05,0.1,0.2,0.25,0.3]
    }
    fixed_dict = {
        'seed': 42,
        #'subsample': 0.8,
        'objective': 'binary:logistic',
        'nthread': 4,
        'eval_metric': ['auc','logloss']
    }
    if args.gpu:
        fixed_dict['tree_method'] = 'gpu_hist'


    # N_trials = 1
    # for trial in trials_dict:
    #     N_trials *= len(trials_dict[trial])
    # print( 'N trials: {}'.format( N_trials ) )

    keys, values = zip(*params_dict.items())
    results = open( 'xgboost_optimisation_slxycut.txt', 'w+' )
    results.write( 'KEYS: {}\n'.format( keys ) )

    for trial in itertools.product( *values ):
        t = dict(zip(keys, trial))
        t.update( fixed_dict )
        bst = xgboost.train(t, dtrain, num_round, evallist, early_stopping_rounds=10 )
        print( 'SCORE for ({}): {}'.format( list( trial ), bst.best_score ) )
        results.write( 'SCORE for ({}): {}\n'.format( list( trial ), bst.best_score ) )

if __name__ == '__main__':
    main()
