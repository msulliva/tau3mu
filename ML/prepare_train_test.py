import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

def do_split( bkg, sig, name='nominal', reweight = False ):

    # Force entries
    set_cc_entries = 0

    # Open files
    bkg_file = bkg #'/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data_prepared.pkl'
    signal_file = sig #'/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf-Ds-tau3mu_prepared.pkl'
    bkg_data = pd.read_pickle( bkg_file )
    signal_data = pd.read_pickle( signal_file )

    # Get initial entries
    print( '------' )
    print( 'Initial background events: {}'.format( bkg_data.shape[0] ) )
    print( 'Initial signal events: {}'.format( signal_data.shape[0] ) )

    # prepare labels
    bkg_labels = np.empty( bkg_data.shape[0] )
    bkg_labels.fill( 0 )
    bkg_data['label'] = bkg_labels

    signal_labels = np.empty( signal_data.shape[0] )
    signal_labels.fill( 1 )
    signal_data['label'] = signal_labels

    # prepare sample splitting
    sample_split = 0.66
    cc_bb_split = 0.8

    # get nentries
    ccNeg_data = signal_data[signal_data['mc_channel_number'] == 300560]
    ccPos_data = signal_data[signal_data['mc_channel_number'] == 300561]
    bbNeg_data = signal_data[signal_data['mc_channel_number'] == 300562]
    bbPos_data = signal_data[signal_data['mc_channel_number'] == 300563]

    # get N entries if required
    if set_cc_entries > 0:

        ccNeg_data = ccNeg_data.sample( n=int(set_cc_entries * 0.5 * cc_bb_split) )
        ccPos_data = ccNeg_data.sample( n=int(set_cc_entries * 0.5 * cc_bb_split) )

    n_ccNeg = ccNeg_data.shape[0]
    n_ccPos = ccPos_data.shape[0]
    n_bbNeg = bbNeg_data.shape[0]
    n_bbPos = bbPos_data.shape[0]

    # calculate required N(cc) and N(bb)
    n_cc = n_ccNeg + n_ccPos
    n_bb = ((1 - cc_bb_split)/cc_bb_split) * n_cc

    # take first N(bb)/2 events for each bb sample
    if reweight:
        bbNeg_weight = n_bb / ( 2 * n_bbNeg )
        bbPos_weight = n_bb / ( 2 * n_bbPos ) 
        ccNeg_data['weight'] = 1.0
        ccPos_data['weight'] = 1.0
        bbNeg_data['weight'] = bbNeg_weight
        bbPos_data['weight'] = bbPos_weight
    else: 
        bbNeg_data = bbNeg_data.sample( n=int(n_bb/2) )
        bbPos_data = bbPos_data.sample( n=int(n_bb/2) )

    # merge charge conjugate samples
    ccData = pd.concat( [ccNeg_data, ccPos_data] )
    bbData = pd.concat( [bbNeg_data, bbPos_data] )

    print( 'cc events used: {}'.format( ccData.shape[0] ) )
    print( 'bb events used: {}'.format( bbData.shape[0] ) )

    # split cc and bb into train/test split first to ensure correct composition
    bb_train, bb_test = train_test_split( bbData, train_size=sample_split )
    cc_train, cc_test = train_test_split( ccData, train_size=sample_split )

    dodirectb = True
    if dodirectb:
        if 'odd' in sig:
            directb = pd.read_pickle( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-bb-taunu_slxycut_odd.pkl' )
        else:
            directb = pd.read_pickle( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-bb-taunu_slxycut_even.pkl' )
        print( 'direct b events used: {}'.format( directb.shape[0] ) )
        directb['weight'] = 1.0
        directb_labels = np.empty( directb.shape[0] )
        directb_labels.fill( 1 )
        directb['label'] = directb_labels
        directb_train, directb_test = train_test_split( directb, train_size=sample_split )

    
    # merge cc and bb training/test samples
    signal_train = pd.concat( [bb_train, cc_train] )
    signal_test = pd.concat( [bb_test, cc_test] )
    if dodirectb:
        signal_train = pd.concat( [signal_train, directb_train])
        signal_test = pd.concat( [signal_test, directb_test])

    # calculate N(signal) to get equal background sample
    n_sig = signal_train.shape[0] + signal_test.shape[0]
    if reweight:
        bkg_data['weight'] = 1.0

    # use all data?
    alldata = True
    if alldata:
        totalsig = int(n_cc + bbPos_data['weight'].sum() + bbNeg_data['weight'].sum())
        totalbkg = bkg_data.shape[0]
        sigweight = totalbkg/totalsig
        print( 'Total signal weight = {}/{} = {}'.format( totalbkg, totalsig, sigweight ) )
        signal_train['weight'] = signal_train['weight'] * sigweight
        signal_test['weight'] = signal_test['weight'] * sigweight
        print( 'Total signal (weighted, train): {}'.format( signal_train['weight'].sum() ) )
        print( 'Total signal (weighted, test): {}'.format( signal_test['weight'].sum() ) )
    else:
        bkg_data = bkg_data.sample( n=n_sig )
    bkg_train, bkg_test = train_test_split( bkg_data, train_size=sample_split )

    print( 'background events used: {}'.format( bkg_data.shape[0] ) )

    train_set = pd.concat( [signal_train, bkg_train] )
    test_set = pd.concat( [signal_test, bkg_test] )
    full_set = pd.concat( [signal_train, bkg_train, signal_test, bkg_test])

    print( 'training events used: {}'.format( train_set.shape[0] ) )
    print( 'test events used: {}'.format( test_set.shape[0] ) )
    print( 'Full dataset events: {}'.format( full_set.shape[0] ) )

    #train_set.to_pickle( '/hepstore/msullivan/Tau3Mu/tuples/20190318/train_set_{}.pkl'.format( name ) )
    #test_set.to_pickle( '/hepstore/msullivan/Tau3Mu/tuples/20190318/test_set_{}.pkl'.format( name ) )
    train_set.to_pickle( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/train_set_{}_slxycut.pkl'.format( name ) )
    test_set.to_pickle( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/test_set_{}_slxycut.pkl'.format( name ) )
    full_set.to_pickle( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/train_set_all_{}_slxycut.pkl'.format( name ) )

    print( '------' )

stratify = True

if stratify:

    #do_split( '/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data_even.pkl', '/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf-Ds-tau3mu_even.pkl', 'even', True)
    #do_split( '/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data_odd.pkl', '/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf-Ds-tau3mu_odd.pkl', 'odd', True)
    do_split( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-data_slxycut_even.pkl', '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-hf-Ds-tau3mu_slxycut_even.pkl', 'even', True)
    do_split( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-data_slxycut_odd.pkl', '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-hf-Ds-tau3mu_slxycut_odd.pkl', 'odd', True)

else:

    #do_split( '/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data_prepared.pkl', '/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf-Ds-tau3mu_prepared.pkl' )
    do_split( '/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data_prepared.pkl', '/bundle/data/ATLAS/msullivan/samples/Tau3Mu/hist-hf-Ds-tau3mu_prepared.pkl' )
