import sys, configparser, os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from random import shuffle
from datetime import datetime
from sklearn import preprocessing
from sklearn.model_selection import train_test_split

import tensorflow as tf
from keras import regularizers
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Activation,Dropout
from keras.regularizers import l2
from sklearn.metrics import roc_curve, auc, accuracy_score

import yaml

__author__ = "Matt Sullivan"
__doc__ = ""

class utils:

	def standardise_data(self, data, file = ''):

		# Get transformation type and do transformation if appropriate
		maxs = []
		mins = []
		means = []
		stds = []

		if (self.variable_transform_type == 'none'):
			data_standardised = data

		elif (self.variable_transform_type == 'minmax'):
			if (file == ''):
				scale_params = open('scale_params.txt','w')
				data_standardised = []
				for column in range(data.shape[1]):
					maxs.append(np.amax(data[:,column]))
					mins.append(np.amin(data[:,column]))
				scale_params.write('maxs: ' + str(maxs) + '\n')
				scale_params.write('mins: ' + str(mins) + '\n')
				data_standardised = self.matt_minmax_scaler(maxs, mins, data)
			else:
				scale_file = open(file,'r')
				for line in scale_file:
					if 'maxs' in line:
						maxs = np.fromstring(str(line.replace('maxs: ','').replace('[','').replace(']','')), sep=',')
					elif 'mins' in line:
						mins = np.fromstring(str(line.replace('mins: ','').replace('[','').replace(']','')), sep=',')	
				data_standardised = self.matt_minmax_scaler(maxs, mins, data)			

		elif (self.variable_transform_type == 'standard'):
			if (file == ''):
				scale_params = open('scale_params.txt','w')
				data_standardised = []
				for column in range(data.shape[1]):
					means.append(np.mean(data[:,column]))
					stds.append(np.std(data[:,column]))
				data_standardised = self.matt_std_scaler(means, stds, data)
				scale_params.write('means: ' + str(means) + '\n')
				scale_params.write('stds: ' + str(stds) + '\n')
			else:
				scale_file = open(file,'r')
				for line in scale_file:
					if 'means' in line:
						means = np.fromstring(str(line.replace('means: ','').replace('[','').replace(']','')), sep=',')
					elif 'stds' in line:
						stds = np.fromstring(str(line.replace('stds: ','').replace('[','').replace(']','')), sep=',')	
				data_standardised = self.matt_std_scaler(means, stds, data)	

		else:
			sys.exit("\033[1;31;40mCONFIG ERROR: Check your config file, transformation type should be either 'none', 'standard' or 'minmax'\033[0;0m");
		# Return data
		return data_standardised

	def matt_minmax_scaler(self, maxs, mins, data):

		temp = []
		for row in range(data.shape[0]):
			entry = []
			for column in range(data.shape[1]):
				if column == self.param_index:
					continue
				value = data[row, column]
				value = (value - mins[column]) / (maxs[column] - mins[column])
				entry.append(value)	
			temp.append(entry)	
		temp = np.asarray(temp)	
		return temp

	def matt_std_scaler(self, means, stds, data):

		temp = []
		for row in range(data.shape[0]):
			entry = []
			for column in range(data.shape[1]):
				if column == self.param_index:
					continue
				value = data[row, column]
				value = (value - means[column]) / stds[column] 
				entry.append(value)	
			temp.append(entry)	
		temp = np.asarray(temp)	
		return temp

	def load_pickles(self):

		if self.fixed_samples:

			if len(self.files) != 2:
				sys.exit("\033[1;31;40mCONFIG ERROR: Ensure you have provided a training file and a test file\033[0;0m")

			train_file =  '{}/{}.pkl'.format( self.file_dir, self.files[0] )
			test_file =  '{}/{}.pkl'.format( self.file_dir, self.files[1] )

			if os.path.exists( train_file ): 

				self.info_message( 'Found training data' )
				train_data = pd.read_pickle( train_file )
				
				if self.param != '':
					train_param = train_data[self.param]

				if self.sample_weights != '':
					training_weights = train_data['weight']

				for feature in train_data.columns:

					if feature not in self.feature_list and feature != 'label':

						self.info_message( "Found column which is not in feature list, dropping {}".format( feature ) )
						train_data = train_data.drop( feature, axis=1 )

			else: 
				sys.exit("\033[1;31;40mCONFIG ERROR: Could not find training data\033[0;0m")

			if os.path.exists( test_file ): 

				self.info_message( 'Found test data' )
				test_data = pd.read_pickle( test_file )

				if self.param != '':
					test_param = test_data[self.param]

				if self.sample_weights != '':
					test_weights = test_data['weight']

				for feature in test_data.columns:

					if feature not in self.feature_list and feature != 'label':

						self.info_message( "Found column which is not in feature list, dropping {}".format( feature ) )
						test_data = test_data.drop( feature, axis=1 )

			else: 
				sys.exit("\033[1;31;40mCONFIG ERROR: Could not find test data\033[0;0m")

			if self.param != '':
				train_data['param'] = train_param
				test_data['param'] = test_param
				self.param_index = list(train_data.columns).index('param')

			#train_data = train_data.sample(frac=1, random_state=42)

			y_train = train_data['label'].values
			y_test = test_data['label'].values
			X_train = train_data.drop(['label'], axis=1)
			X_test = test_data.drop(['label'], axis=1)
			columns = X_train.columns

			if self.sample_weights == '':

				return X_train.to_numpy(), X_test.to_numpy(), y_train, y_test, columns

			else:

				w_train = training_weights.values
				w_test = test_weights.values
				return X_train.to_numpy(), X_test.to_numpy(), y_train, y_test, w_train, w_test, columns

		else:

			# Load pickled files

			if len(self.files)!= len(self.classes):
				sys.exit("\033[1;31;40mCONFIG ERROR: Number of files and classes is not equal\033[0;0m")
			else:
				print('INFO: Using ' + str(len(self.classes)) + ' classes')

			temp_files = []
			temp_labels = []

			for index, input_file in enumerate( self.files ):

				pickle_file = '{}/{}.pkl'.format( self.file_dir, input_file )

				if os.path.exists( pickle_file ):

					self.info_message( 'Found pickled file' )
					tempdf = pd.read_pickle( pickle_file )

					if self.param != '':
						param = tempdf[self.param]

					if self.sample_weights != '':
						weights = tempdf[self.sample_weights]
					
					for feature in tempdf.columns:

						if feature not in self.feature_list:

							self.info_message( "Found column which is not in feature list, dropping {}".format( feature ) )
							tempdf = tempdf.drop( feature, axis=1 )

					if self.param != '':
						tempdf['param'] = param
						self.param_index = list(tempdf.columns).index('param')

					if tempdf.shape[0] > self.force_entries:

						tempdf = tempdf.sample( n = self.force_entries )

					templabels = np.empty( tempdf.shape[0] )
					templabels.fill( self.classes[ index ] )
					tempdf['label'] = templabels
					temp_files.append( tempdf )

			data = pd.concat( temp_files )

			temp_labels = data['label']
			temp_data = data.drop(['label'], axis=1)
			columns = temp_data.columns

			if self.sample_weights == '':

				X_train, X_test, y_train, y_test = train_test_split(temp_data.values, temp_labels.values, test_size=0.33, random_state=7)
				return X_train, X_test, y_train, y_test, columns

			else:

				X_train, X_test, y_train, y_test, w_train, w_test = train_test_split(temp_data.values, temp_labels.values, weights, test_size=0.33, random_state=7)

			

	def read_config(self, config_file, training):

		self.config = config_file

		config_file = open( self.config )
		config_yaml = yaml.full_load( config_file )

		_global = config_yaml['global']
		_training = config_yaml['training'][training]
		_inputs = _training['inputs']
		_general = _training['general']
		_model = _training['model']

		self.info_message( 'Starting training: {}'.format( training ) )

		# Get global variables
		self.work_dir                    = _global['work_dir']
		self.file_dir                    = _global['file_dir']
		self.feature_list                = _global['features']
		self.n_features                  = len( self.feature_list )

		# Get input files
		self.classes                     = _inputs['classes']
		self.n_classes                   = len( self.classes )
		self.files                       = _inputs['files']
		self.fixed_samples               = _inputs['fixed_samples']
		self.class_weights               = _inputs['class_weights']
		self.sample_weights              = _inputs['sample_weights']
		self.param                       = _inputs['param']
		self.param_index                 = -99

		# Get general training info
		self.force_entries               = _general['force_entries']
		self.reweight_classes            = _general['reweight_classes']
		self.variable_transform_type     = _general['variable_transform']

		# Get model parameters
		self.model_name                  = training
		self.n_layers                    = _model['layers']
		self.DNN_nodes_hidden            = _model['nodes']
		self.DNN_kernel_initialiser      = _model['kernel_init']
		self.DNN_hidden_layer_activation = _model['activation']
		self.DNN_dropout                 = _model['dropout']
		self.DNN_early_stopping          = _model['early_stopping_patience']
		self.DNN_batch_size              = _model['batch_size']
		self.DNN_epochs                  = _model['epochs']
		self.DNN_LR                      = _model['learn_rate']
		self.l2_reg                      = _model['l2_reg']

	def define_model(self):

		# Define DNN model
		model = Sequential()
		initialiser = tf.keras.initializers.HeNormal()
		self.DNN_kernel_initialiser = initialiser
		# Add input layer
		model.add(Dense(self.DNN_nodes_hidden,
										kernel_initializer=self.DNN_kernel_initialiser,
										activation=self.DNN_hidden_layer_activation,
										kernel_regularizer=l2(float(self.l2_reg)),
										input_dim=self.n_features))
		# Add hidden layer(s)
		for i in range(self.n_layers):

			model.add(Dropout(self.DNN_dropout))
			model.add(Dense(self.DNN_nodes_hidden,
											kernel_initializer=self.DNN_kernel_initialiser,
											activation=self.DNN_hidden_layer_activation,
											kernel_regularizer=l2(float(self.l2_reg))))
		# model.add(Dense(16,
		#  									kernel_initializer=self.DNN_kernel_initialiser,
		#  									activation=self.DNN_hidden_layer_activation,
		#  									kernel_regularizer=l2(self.l2_reg)))
		model.add(Dropout(self.DNN_dropout))

 		# Add output layer
		if len(self.classes) == 2:
			model.add(Dense(1, activation='sigmoid'))
		elif len(self.classes) > 2:
			model.add(Dense(len(self.classes),
										activation='softmax'))

		# Print model summary
		model.summary()

		return model

	def set_name(self):

		d = datetime.now()

        # Get time & date for unique naming of outputs
		unique_string = self.model_name
		unique_string += "_"
		unique_string += str(d.year) + "-"
		unique_string += str(d.month) + "-"
		unique_string += str(d.day) + "-"
		unique_string += str(d.hour) + "h"
		if d.minute < 10:
			unique_string += ("0" + str(d.minute))
		else:
			unique_string += str(d.minute)
		return unique_string

	def info_message(self, message):

		print("\033[0;32mINFO: " + message + "\033[0;0m")

	def write_output( self, train_sets, test_sets, train_labels, test_labels, train_preds, test_preds, train_weights, columns ):

		# Create merged DFs

		for train_set, test_set, train_label, test_label, train_pred, test_pred in zip(train_sets, test_sets, train_labels, test_labels, train_preds, test_preds):

			print( train_label )

			if self.n_classes == 2:

				train_df = pd.DataFrame( train_set, columns = columns )
				train_df['label'] = train_label
				train_df['preds'] = train_pred
				if train_weights != '':
					train_df['weight'] = train_weights

				test_df = pd.DataFrame( test_set, columns = columns )
				test_df['label'] = test_label
				test_df['preds'] = test_pred

			else:

				train_df = pd.DataFrame( train_set, columns = columns )
				if isinstance(train_label[0], np.float64):
					train_df['label'] = [int(i) for i in train_label]
				else:
					train_df['label'] = [np.argmax(i) for i in train_label]
				for _class in range( self.n_classes ):
					train_df['preds_{}'.format(_class)] = train_pred[:,_class]

				test_df = pd.DataFrame( test_set, columns = columns )
				if isinstance(test_label[0], np.float64):
					test_df['label'] = [int(i) for i in test_label]
				else:
					test_df['label'] = [np.argmax(i) for i in test_label]
				for _class in range( self.n_classes ):
					test_df['preds_{}'.format(_class)] = test_pred[:,_class]

			train_df.to_pickle( 'train_set.pkl' )
			test_df.to_pickle( 'test_set.pkl' )

		unique_string = self.set_name()

		os.system('mkdir ' + unique_string)
		os.system('mkdir ' + unique_string + '/plots')
		os.system('mv *.pdf ' + unique_string + '/plots/')
		os.system('mv *.png ' + unique_string + '/plots/')
		os.system('mv *_set.pkl ' + unique_string)
		os.system('mv *.h5 ' + unique_string)
		os.system('mv *.json ' + unique_string)
		os.system('mv *.txt ' + unique_string)
		os.system('mv *.model ' + unique_string)
		os.system('cp ' + self.config + ' ' + unique_string)

		self.info_message('Finished writing output, exiting...')

	def __init__(self):

		self.info_message('Initialised utilites class')