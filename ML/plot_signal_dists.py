import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def make_plot( df1, df2, column, optimal_cut, training=True ):

    fig = plt.figure()
    df1_bkg = df1[(df1['preds'] > optimal_cut) & (df1['label'] == 0)]
    df2_bkg = df2[(df2['preds'] > optimal_cut) & (df2['label'] == 0)]
    df1_sig = df1[(df1['preds'] > optimal_cut) & (df1['label'] == 1)]
    df2_sig = df2[(df2['preds'] > optimal_cut) & (df2['label'] == 1)]

    min_val = min( df1_bkg[column].min(), df2_bkg[column].min(), df1_sig[column].min(), df2_sig[column].min() )
    max_val = min( df1_bkg[column].max(), df2_bkg[column].max(), df1_sig[column].max(), df2_sig[column].max() )
    nbins = 50
    bins = np.linspace( min_val, max_val, nbins+1 )

    print( df1_bkg.shape[0] )
    print( df1_sig.shape[0] )
    print( df2_bkg.shape[0] )
    print( df2_sig.shape[0] )

    plt.hist( df1_bkg[column].values, bins=bins, label = 'df1 (bkg)', histtype='step' )
    plt.hist( df2_bkg[column].values, bins=bins, label = 'df2 (bkg)', histtype='step' )
    plt.hist( df1_sig[column].values, bins=bins, label = 'df1 (sig)', histtype='step' )
    plt.hist( df2_sig[column].values, bins=bins, label = 'df2 (sig)', histtype='step' )
    plt.xlabel( column )
    #plt.yscale('log')
    plt.legend( loc='best' )
    if training:
        plt.savefig( 'comparison/training_{}.png'.format( column ), dpi=200 )
    else:
        plt.savefig( 'comparison/test_{}.png'.format( column ), dpi=200 )

optimal_cut = 0.83

file1 = 'even_2021-4-22-15h47/train_set.pkl'
file2 = 'even_2021-4-22-17h25/train_set.pkl'

df1 = pd.read_pickle( file1 )
df2 = pd.read_pickle( file2 )

for column in df1.columns:

    if column == 'preds' or column == 'label':
        continue
    print( column )

    make_plot( df1, df2, column, optimal_cut )

file1 = 'even_2021-4-16-17h24/test_set.pkl'
file2 = 'even_2021-4-20-9h50/test_set.pkl'

df1 = pd.read_pickle( file1 )
df2 = pd.read_pickle( file2 )

for column in df1.columns:

    if column == 'preds' or column == 'label':
        continue
    print( column )

    make_plot( df1, df2, column, optimal_cut, False )