

# Libraries
import argparse
import xgboost
import utils
import matplotlib.pyplot as plt

__author__ = "Matt Sullivan"
__doc__ = ""

def main():

    # Initialise utils
    train_utils = utils.utils()

    # Load config file
    parser = argparse.ArgumentParser(description='XGBoost training script')
    parser.add_argument('-c', action="store", dest="config_file")
    parser.add_argument('-t', action="store", dest="training")
    args = parser.parse_args()
    train_utils.read_config(args.config_file, args.training)

    # Load data
    if train_utils.sample_weights == '':
        X_train, X_test, y_train, y_test, columns = train_utils.load_pickles()
    else:
        X_train, X_test, y_train, y_test, w_train, w_test, columns = train_utils.load_pickles()

    dtrain = xgboost.DMatrix(X_train, label=y_train, feature_names = columns)
    dtest = xgboost.DMatrix(X_test, label=y_test, feature_names = columns)

    results = {}

    etas = [ 1, 0.5, 0.1, 0.05, 0.01, 0.005 ]
    depths = [ 2,4,6,8 ]
    subsamples = [ 0.2, 0.4, 0.6, 0.8, 1.0 ]

    num_round = 10000
    param = {'objective': 'binary:logistic'}
    param['nthread'] = 4
    param['eval_metric'] = ['auc','logloss']
    evallist = [ (dtrain, 'train'), (dtest, 'eval') ] 

    do_eta = False
    do_depth = False
    do_subsample = True

    if do_eta:
        for eta in etas:

            param['eta'] = eta
            param['max_depth'] = 6
            param['subsample'] = 0.8
            bst = xgboost.train(param, dtrain, num_round, evallist, early_stopping_rounds=10)   
            results[eta] = bst.best_score

    if do_depth:
        for depth in depths:

            param['eta'] = 0.1
            param['max_depth'] = depth
            param['subsample'] = 0.8
            bst = xgboost.train(param, dtrain, num_round, evallist, early_stopping_rounds=10)   
            results[depth] = bst.best_score

    if do_subsample:
        for fraction in subsamples:

            param['eta'] = 0.1
            param['max_depth'] = 8
            param['subsample'] = fraction
            bst = xgboost.train(param, dtrain, num_round, evallist, early_stopping_rounds=10)   
            results[fraction] = bst.best_score


    for config in results:

        print( 'Config {}: {}'.format( config, results[config] ) )

if __name__ == '__main__':
    main()