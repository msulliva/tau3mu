import argparse
import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.metrics import roc_curve, auc, roc_auc_score, accuracy_score

class make_plots:

    def __init__(self):

        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('-d', '--dir')
        self.parser.add_argument('--multiclass', action='store_true')
        self.args = self.parser.parse_args()
        os.chdir(self.args.dir)
        self.train_name = 'train_set.pkl'
        self.test_name = 'test_set.pkl'

    def run(self): 

        self.get_accuracy()
        self.make_roc()
        self.make_plots()
        self.make_correlations()

    def get_accuracy(self):


        train_df = pd.read_pickle( self.train_name )
        test_df = pd.read_pickle( self.test_name )
        classes = train_df['label'].unique()

        if len( classes ) == 2:

            train_score = accuracy_score( train_df['label'].values, train_df['preds'].values.round() )
            test_score = accuracy_score( test_df['label'].values, test_df['preds'].values.round() )

            train_score = round( train_score, 4 )* 100
            test_score = round( test_score, 4 )* 100

            print( 'Training accuracy: {}%'.format( train_score ) )
            print( 'Testing accuracy: {}%'.format( test_score ) )

        else:

            for _class in classes:

                train_score = accuracy_score( train_df['label'].values, train_df['preds_{}'.format(_class)].values.round() )
                test_score = accuracy_score( test_df['label'].values, test_df['preds_{}'.format(_class)].values.round() )

                train_score = round( train_score, 4 )* 100
                test_score = round( test_score, 4 )* 100

                print( 'Training accuracy: {}%'.format( train_score ) )
                print( 'Testing accuracy: {}%'.format( test_score ) )

    def make_roc(self):

        train_df = pd.read_pickle( self.train_name )
        classes = train_df['label'].unique()
        print( classes )
        if len( classes ) == 2:
            keras_fpr, keras_tpr, keras_thresholds = roc_curve(train_df['label'].values, train_df['preds'].values)
            keras_auc = roc_auc_score( train_df['label'].values, train_df['preds'].values )
            plt.plot( keras_tpr, 1 - keras_fpr, label = 'AUC: {}'.format(round(keras_auc,4)) )
            plt.xlabel( 'Signal efficiency' )
            plt.ylabel( 'Background rejection' )
            plt.legend(loc='best')
            plt.savefig('plots/ROC.png')
            plt.gcf().clear()

    def make_plots(self):

        train_df = pd.read_pickle( self.train_name )
        test_df = pd.read_pickle( self.test_name )

        colours = ['r','c','g','b','m']

        columns = train_df.columns

        for column in columns:

            if column == 'label' or 'preds' in column:
                continue

            bin_low = train_df[column].min()
            bin_hi = train_df[column].max()
            bin_n = 50

            bins = np.linspace( bin_low, bin_hi, bin_n)

            classes = train_df['label'].unique()

            for classif in classes:

                classif = int(classif)

                plt.hist( train_df[train_df ['label']==classif][column], 
						bins = bins,
						label = classif,
						histtype='step',
						color=colours[classif],
						alpha = 0.7 )
			
            plt.xlabel( column )
            plt.ylabel( 'Events / bin')
            plt.legend(loc='best')
            plt.savefig('plots/{}.png'.format(column))
            plt.gcf().clear()

        if len( classes ) == 2:
            bins = np.linspace(0,1,50)
            plt.hist( train_df[train_df['label']==0]['preds'], label = 'Class 0 (train)', bins = bins, histtype='step', alpha=0.7 )
            plt.hist( train_df[train_df['label']==1]['preds'], label = 'Class 1 (train)', bins = bins, histtype='step', alpha=0.7 )
            plt.hist( test_df[test_df['label']==0]['preds'], label = 'Class 0 (test)', bins = bins, histtype='step', alpha=0.7 )
            plt.hist( test_df[test_df['label']==1]['preds'], label = 'Class 1 (test)', bins = bins, histtype='step', alpha=0.7 )
            #plt.yscale('log')
            plt.xlabel( 'Prediction')
            plt.legend(loc='upper center')
            plt.savefig('plots/distribution.png')
        elif len( classes ) == 3:
            bins = np.linspace(0,1,50)
            plt.hist( 1-train_df[train_df['label']==0]['preds_0'], label = 'Class 0 (train)', bins = bins, histtype='step', alpha=0.7 )
            plt.hist( train_df[train_df['label']==1]['preds_1'], label = 'Class 1 (train)', bins = bins, histtype='step', alpha=0.7 )
            plt.hist( train_df[train_df['label']==2]['preds_2'], label = 'Class 2 (train)', bins = bins, histtype='step', alpha=0.7 )
            plt.hist( 1-test_df[test_df['label']==0]['preds_0'], label = 'Class 0 (test)', bins = bins, histtype='step', alpha=0.7 )
            plt.hist( test_df[test_df['label']==1]['preds_1'], label = 'Class 1 (test)', bins = bins, histtype='step', alpha=0.7 )
            plt.hist( test_df[test_df['label']==2]['preds_2'], label = 'Class 2 (test)', bins = bins, histtype='step', alpha=0.7 )
            plt.yscale('log')
            plt.xlabel( 'Prediction')
            plt.legend(loc='upper center')
            plt.savefig('plots/distribution.png')

    def make_correlations(self):

        import corrmatrix

        cm = corrmatrix.corrmatrix()
    
        train_df = pd.read_pickle( self.train_name )
        test_df = pd.read_pickle( self.test_name )
        train_df = train_df.drop(['label','preds'], axis=1)
        test_df = test_df.drop(['label','preds'], axis=1)
        cm.draw(train_df, 'plots/corrMatrix_train')
        cm.draw(test_df, 'plots/corrMatrix_test')

if __name__ == '__main__':
    make_plots().run()

