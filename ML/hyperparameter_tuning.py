import multiprocessing as mp
import argparse
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from keras import regularizers
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers import Dense,Activation,Dropout
from tensorflow.keras.optimizers import Adam
from keras.regularizers import l2
from sklearn.metrics import roc_curve, auc, accuracy_score

def standardise_data( data, file, std=False ):

    scale_file = open( file,'r')
    if std:
        for line in scale_file:
            if 'means' in line:
                means = np.fromstring(str(line.replace('means: ','').replace('[','').replace(']','')), sep=',')
            elif 'stds' in line:
                stds = np.fromstring(str(line.replace('stds: ','').replace('[','').replace(']','')), sep=',')	
    else:
        for line in scale_file:
            if 'maxs' in line:
                maxs = np.fromstring(str(line.replace('maxs: ','').replace('[','').replace(']','')), sep=',')
            elif 'mins' in line:
                mins = np.fromstring(str(line.replace('mins: ','').replace('[','').replace(']','')), sep=',')	
    #data_standardised = matt_std_scaler(means, stds, data)	
    data_standardised = matt_minmax_scaler(maxs, mins, data)	
    return data_standardised

def matt_std_scaler( means, stds, data ):

    temp = []
    for row in range(data.shape[0]):
        entry = []
        for column in range(data.shape[1]):
            value = data[row, column]
            value = (value - means[column]) / stds[column] 
            entry.append(value)	
        temp.append(entry)	
    temp = np.asarray(temp)	
    return temp

def matt_minmax_scaler( maxs, mins, data):

	temp = []
	for row in range(data.shape[0]):
		entry = []
		for column in range(data.shape[1]):
			value = data[row, column]
			value = (value - mins[column]) / (maxs[column] - mins[column])
			entry.append(value)	
		temp.append(entry)	
	temp = np.asarray(temp)	
	return temp

def train( model ):

    # Load data
    #data, labels = utils.load_pickles()
    #X_train, X_test, y_train, y_test = train_test_split(data.values, labels, test_size=0.33, random_state=7) 
    train_set = pd.read_pickle( training_file )
    test_set = pd.read_pickle( test_file )

    X_train = train_set.drop( ['label','preds','weight'], axis=1).values
    X_test = test_set.drop( ['label','preds'], axis=1).values
    y_train = train_set['label'].values
    y_test = test_set['label'].values
    w_train = train_set['weight']

    # Keras: setup
    model_index = model['index']
    LR = model['LR']
    model_name = './model{}.h5'.format( model_index )
    model = load_model( model_name, compile=False )

    earlystop = EarlyStopping(monitor='val_loss', min_delta = 0.0001, patience = 10, verbose = 1, mode = 'auto')
    #modelcheckpoint = ModelCheckpoint(unique_string+'.h5', monitor='val_loss', save_best_only=True)
    callbacks_list = [earlystop]#,modelcheckpoint]
    adam = Adam(lr=LR, beta_1=0.9, beta_2=0.999, decay=0.0)

    # Keras: binary vs multiclass configuration
    if len(classes) == 2:
        print("\033[0;32mINFO: Keras configured for binary classification\033[0;0m")
        model.compile(optimizer=adam,loss='binary_crossentropy',metrics=['accuracy'])
    elif len(classes) > 2:
        print("\033[0;32mINFO: Keras configured for multiclass classification\033[0;0m")
        model.compile(optimizer=adam,loss='categorical_crossentropy',metrics=['accuracy'])

    # Train classifiers using GPU
    if len(classes) > 2:
        y_train = to_categorical(y_train)
        y_test  = to_categorical(y_test)
    bst_keras   = model.fit(standardise_data(X_train, scale_file),
            y_train,
            epochs = DNN_epochs,
            batch_size = DNN_batch_size,
            callbacks = callbacks_list,
            verbose = 2,
            validation_data=(standardise_data(X_test, scale_file),y_test),
            sample_weight=w_train)
            #)#,
            #class_weight={0: 1, 1:1., 2:1.})
    keras_res = bst_keras

    # save models + importances
    #keras_model.save('keras_model.h5')

    # make predictions
    #keras_train_preds = model.predict(standardise_data(X_train, scale_file))
    #keras_test_preds  = model.predict(standardise_data(X_test, scale_file))

    train_score, train_acc = model.evaluate(standardise_data(X_train, scale_file), y_train)
    test_score, test_acc = model.evaluate(standardise_data(X_test, scale_file), y_test)

    # DEBUG plots
    #np.random.seed()
    #train_score = np.random.uniform(0,1)
    #test_score = np.random.uniform(0,1)
    #train_acc = np.random.uniform(0,1)
    #test_acc = np.random.uniform(0,1)

    return (model_index, train_score, train_acc, test_score, test_acc)

def define_model( n_layers, n_nodes, dropout ):

        # Define DNN model
    model = Sequential()
	# Add input layer
    model.add(Dense(n_nodes,
                    kernel_initializer=DNN_kernel_initialiser,
                    activation=DNN_hidden_layer_activation,
                    kernel_regularizer=l2(1e-5),
                    input_dim=n_features))
	# Add hidden layer(s)
    for i in range(n_layers):

        model.add(Dropout(dropout))
        model.add(Dense(n_nodes,
                    kernel_initializer=DNN_kernel_initialiser,
                    activation=DNN_hidden_layer_activation,
                    kernel_regularizer=l2(1e-5)))

    model.add(Dropout(dropout))

    # Add output layer
    if len(classes) == 2:
        model.add(Dense(1, activation='sigmoid'))   
    elif len(classes) > 2:
        model.add(Dense(len(classes), activation='softmax'))

    ## Print model summary
    #model.summary()

    return model

def make_plot( results ):

    fig = plt.figure(figsize=(10,10))
    labels = []
    train_accs = []
    test_accs = []
    for result in results:
        index = result[0]
        train_acc = result[2]
        test_acc = result[4]
        train_accs.append( train_acc )
        test_accs.append( test_acc )
        label = 'LR {}, {} layers, {} nodes, {} dropout'.format( train_params[index]['LR'], train_params[index]['layers'], train_params[index]['nodes'], train_params[index]['dropout'] )
        plt.scatter( index, train_acc, c='tab:orange', s=40)#, label='Model {}, train acc {}, test acc {}'.format(index, round(train_acc,3), round(test_acc,3)))
        plt.scatter( index, test_acc, c='tab:blue', s=40)
        plt.text( index-0.5, train_acc + 0.04, '{}'.format( round(train_acc, 3)), rotation=45., rotation_mode='anchor', c='tab:orange')
        plt.text( index-0.5, test_acc - 0.04, '{}'.format( round(test_acc, 3)), rotation=45., rotation_mode='anchor', c='tab:blue')
        labels.append( label )
    plt.xticks(models, labels, rotation='vertical')
    ymin = min( min(train_accs), min(test_accs) )
    ymax = max( max(train_accs), max(test_accs) )
    plt.ylim(0.8*ymin, 0.95)
    #plt.legend(ncol=2, loc='best')
    plt.tight_layout()
    #plt.margins(0.1)
    plt.savefig('result.png', dpi=500)

# To be automated
classes = 0,1
training_file = 'even_2021-4-20-14h51/train_set.pkl' 
test_file = 'even_2021-4-20-14h51/test_set.pkl' 
scale_file = 'even_2021-4-20-14h51/scale_params.txt'
n_features = 17

# Fixed for now
DNN_epochs = 10000
DNN_batch_size = 32
DNN_kernel_initialiser = 'random_normal'
DNN_hidden_layer_activation = 'relu' 

# training params
LRs = [0.01, 0.001, 0.0001, 0.00001]
train_params = []
# model params
layers = [2, 4]
nodes = [10, 32, 128]
dropouts = [0.2]
# calculate total number of trainings
n_trainings = len(LRs) * len(layers) * len(nodes) * len(dropouts)
# generate model indices
models = np.linspace(0, n_trainings-1, n_trainings, dtype = int)
# create a pool of n_models 
#pool = mp.Pool( n_trainings )
# create models to avoid pickling error
import itertools
trainings = list(itertools.product(LRs, layers, nodes, dropouts))
for index, training in enumerate(trainings):

    # Get parameters
    LR = training[0]
    layer = training[1]
    node = training[2]
    dropout = training[3]
    # Store parameters for later
    param_dict = {}
    param_dict['index'] = index
    param_dict['LR'] = LR
    param_dict['layers'] = layer
    param_dict['nodes'] = node
    param_dict['dropout'] = dropout
    train_params.append( param_dict )
    # Define model and save
    #temp_model = define_model( layer, node, dropout )
    #temp_model.save('model{}.h5'.format(index, LR))

print("Initialised tuning")
#results = pool.map( train, train_params )

#for result in results:
#    print( result )

results = [
(0, 0.4643166661262512, 0.8128090500831604, 0.4673774242401123, 0.812070369720459),
(1, 0.42311087250709534, 0.8176603317260742, 0.42518165707588196, 0.8166531324386597),
(2, 0.40719497203826904, 0.8140380382537842, 0.40845492482185364, 0.8134326338768005),
(3, 0.4716522991657257, 0.8204692006111145, 0.47387731075286865, 0.8190947771072388),
(4, 0.42337045073509216, 0.8045532703399658, 0.4249248206615448, 0.8035858869552612),
(5, 0.44044166803359985, 0.8065056800842285, 0.44229814410209656, 0.8058493733406067),
(6, 0.402665376663208, 0.8066279888153076, 0.40423038601875305, 0.8065165281295776),
(7, 0.39318764209747314, 0.8137357831001282, 0.3949858248233795, 0.8128283619880676),
(8, 0.4114116132259369, 0.804760217666626, 0.41360044479370117, 0.8043159246444702),
(9, 0.4554177224636078, 0.8224053978919983, 0.45798352360725403, 0.8211276531219482),
(10, 0.3951674997806549, 0.8164042830467224, 0.3970334231853485, 0.8157729506492615),
(11, 0.4114192724227905, 0.8095934987068176, 0.413218230009079, 0.8091607093811035),
(12, 0.41149595379829407, 0.8096906542778015, 0.4138600826263428, 0.8078438639640808),
(13, 0.38099056482315063, 0.8247626423835754, 0.38340967893600464, 0.8243482112884521),
(14, 0.38418126106262207, 0.8254284262657166, 0.38650500774383545, 0.8244529962539673),
(15, 0.405933141708374, 0.8303084969520569, 0.4088876247406006, 0.8290532827377319),
(16, 0.38535913825035095, 0.8215632438659668, 0.3876742422580719, 0.82113116979599),
(17, 0.3846723735332489, 0.8265962600708008, 0.3873737156391144, 0.8257314562797546),
(18, 0.4059055745601654, 0.8146588802337646, 0.408286452293396, 0.8127759695053101),
(19, 0.4046100080013275, 0.8153588175773621, 0.40678831934928894, 0.8145818114280701),
(20, 0.37120679020881653, 0.8300079703330994, 0.3741280734539032, 0.8295667171478271),
(21, 0.42045751214027405, 0.8281725645065308, 0.4232814311981201, 0.8268002867698669),
(22, 0.39105740189552307, 0.8134424686431885, 0.39339300990104675, 0.8125768303871155),
(23, 0.3732650876045227, 0.8294375538825989, 0.3762877583503723, 0.8289170265197754),

]

make_plot( results )