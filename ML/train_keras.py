

# Libraries
import argparse
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical #plot_model, 
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint

# Import utilities class
import utils

__author__ = "Matt Sullivan"
__doc__ = ""

def main():

	# set random seed
	test_seed = 42
	from numpy.random import seed
	seed(test_seed)
	from tensorflow.random import set_seed
	set_seed(test_seed)

	# Initialise utils
	train_utils = utils.utils()

	# Load config file
	parser = argparse.ArgumentParser(description='Keras training script')
	parser.add_argument('-c', action="store", dest="config_file")
	parser.add_argument('-t', action="store", dest="training")
	args = parser.parse_args()
	train_utils.read_config(args.config_file, args.training)

	# Make string from current time + date
	unique_string = train_utils.set_name()

	# Load data
	if train_utils.sample_weights == '':
		X_train, X_test, y_train, y_test, columns = train_utils.load_pickles()
	else:
		X_train, X_test, y_train, y_test, w_train, w_test, columns = train_utils.load_pickles()

	train_sets = []
	train_labels = []
	train_preds = []
	test_sets = []
	test_labels = []
	test_preds = []
 
	scale_params = 'scale_params.txt'
	# Keras: setup
	earlystop = EarlyStopping(monitor='val_loss', min_delta = 0.0001, patience = train_utils.DNN_early_stopping, verbose = 1, mode = 'auto')
	modelcheckpoint = ModelCheckpoint('keras_model_best.h5'.format(train_utils.model_name), monitor='val_loss', save_best_only=True)
	callbacks_list = [earlystop,modelcheckpoint]
	keras_model = train_utils.define_model()
	adam = Adam(lr=train_utils.DNN_LR, beta_1=0.9, beta_2=0.999, decay=0.0)

	# Keras: binary vs multiclass configuration
	if len(train_utils.classes) == 2:
		print("\033[0;32mINFO: Keras configured for binary classification\033[0;0m")
		keras_model.compile(optimizer=adam,loss='binary_crossentropy',metrics=['accuracy'])
	elif len(train_utils.classes) > 2:
		print("\033[0;32mINFO: Keras configured for multiclass classification\033[0;0m")
		keras_model.compile(optimizer=adam,loss='categorical_crossentropy',metrics=['accuracy'])

	if train_utils.sample_weights == '':

		# Train classifiers using GPU
		if len(train_utils.classes) > 2:
			y_train = to_categorical(y_train)
			y_test  = to_categorical(y_test)
		bst_keras   = keras_model.fit(train_utils.standardise_data(X_train),
						y_train,
						epochs = train_utils.DNN_epochs,
						batch_size = train_utils.DNN_batch_size,
						callbacks = callbacks_list,
						verbose = 2,
						validation_data=(train_utils.standardise_data(X_test, scale_params),y_test),
						shuffle=True,
						class_weight=train_utils.class_weights)
		keras_res = bst_keras

	else:

		# Train classifiers using GPU
		if len(train_utils.classes) > 2:
			y_train = to_categorical(y_train)
			y_test  = to_categorical(y_test)
		bst_keras   = keras_model.fit(train_utils.standardise_data(X_train),
						y_train,
						epochs = train_utils.DNN_epochs,
						batch_size = train_utils.DNN_batch_size,
						callbacks = callbacks_list,
						verbose = 2,
						validation_data=(train_utils.standardise_data(X_test, scale_params),y_test),
						shuffle=True,
						class_weight=train_utils.class_weights,
						sample_weight=w_train)
		keras_res = bst_keras

	# save models + importances
	keras_model.save('keras_model_{}.h5'.format(train_utils.model_name))

	# make predictions
	keras_train_preds = keras_model.predict(train_utils.standardise_data(X_train, scale_params))
	keras_test_preds  = keras_model.predict(train_utils.standardise_data(X_test, scale_params))

	# add outputs
	train_sets.append( X_train )
	test_sets.append( X_test )
	train_labels.append( y_train )
	test_labels.append( y_test )
	train_preds.append( keras_train_preds )
	test_preds.append( keras_test_preds )

	# write output
	if train_utils.sample_weights == '':
		w_train = ''
	train_utils.write_output( train_sets, test_sets, train_labels, test_labels, train_preds, test_preds, w_train, columns  )

if __name__ == "__main__":
	main()
