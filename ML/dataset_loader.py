import pandas as pd
import numpy as np

class dataset_loader:

    def __init__( self, file_dir, features ):

        self.file_dir = file_dir
        self.features = features

    def load( self, input, tree, cuts, output ):

        from ROOT import TFile, TTree

        tfile = TFile( '{}/{}'.format( self.file_dir, input ) )
        ttree = tfile.Get( tree )  

        # Determine if trimming is needed
        if ( ttree.GetEntries() != ttree.GetEntries( cuts ) ):

            output_file_name = '{}/{}'.format( self.file_dir, output )
            output_file = TFile( '{}.root'.format( output_file_name ), 'RECREATE' )

            if self.features != '':
                #ttree.SetBranchStatus( "*", 0 )
                for feature in self.features: 
                    if '[' in feature and ']' in feature:
                        feature = feature.split('[')[0]
                    #ttree.SetBranchStatus( feature, 1 )

            output_tree = ttree.CopyTree( cuts )
            output_tree.SetBranchStatus("*", 0)
            for feature in self.features:
                if '[' in feature and ']' in feature:
                    feature = feature.split('[')[0]
                ttree.SetBranchStatus( feature, 1 )
                
            output_tree.Write()
            df = self.process_tree( output_tree, output_file_name )
            output_file.Write()
            output_file.Close()
            return df
        
        return False

    def process_tree( self, tree, name ):

        import root_numpy
        import numpy as np
        import sys
        import progressbar

        branches = []
        flats = []
        vectors = []

        for feature in self.features:
            if '[' in feature and ']' in feature:
                print( 'Found vector: {} '.format( feature ) )
                branches.append( feature.replace('[','_').replace(']','') )
                vectors.append( feature )
            else:
                branches.append( feature )
                flats.append( feature )

        tempdf = pd.DataFrame( columns = branches )

        for feature in flats:

            feature_branch = root_numpy.tree2array( tree, branches=feature )
            if isinstance( feature_branch[0], ( np.ndarray) ):
                feature_branch = np.concatenate( feature_branch ).ravel().tolist()
            tempdf[ feature ] = feature_branch

        for feature in vectors:

            index = int(feature.split('[')[1].split(']')[0])
            feature = feature.split('[')[0]
            feature_branch = root_numpy.tree2array( tree, branches=feature )
            flatten = []
            for entry in progressbar.progressbar(range(feature_branch.shape[0])):
                flatten.append( feature_branch[entry][index] )
            feature_name = '{}_{}'.format( feature, index )
            tempdf[ feature_name ] = flatten

        pickle_name = '{}.pkl'.format( name )
        tempdf.to_pickle( pickle_name )

        return tempdf

    def load_rdf(self, input, tree, cuts, output):

        import ROOT
        from ROOT import RDataFrame

        # Load input file into RDataFrame
        df = RDataFrame( tree, '{}/{}'.format(self.file_dir, input) )
        # Apply cuts to RDataFrame
        df2 = df.Filter( cuts )
        # Loop over requested features
        new_branches = []
        for feature in self.features:

            if '[' in feature and ']' in feature: 

                index = feature.split('[')[1].split(']')[0]
                new_feature = feature.split('[')[0]
                new_branch = '{}_{}'.format( new_feature, index )
                new_branches.append( new_branch )
                df2 = df2.Define( new_branch, feature )

        # select branches for output file
        branchList = ROOT.vector('string')()
        # Choose whether to keep or throw away all branches from the input
        keep_all_branches = False
        if keep_all_branches:
            for branchName in df.GetColumnNames():
                branchList.push_back(branchName)
        # Add new branches
        for branchName in new_branches:
            branchList.push_back(branchName)
        # Save new dataframe to file
        output_root = './{}.root'.format( output )
        df2.Snapshot("events", output_root, branchList)
        # Pickle the dataframe for later use!
        tempdf = pd.DataFrame( columns = new_branches )
        for branch in new_branches:
            tempdf[branch] = df2.AsNumpy([branch])[branch]
        output_pkl = output_root.replace('.root','.pkl')
        tempdf.to_pickle(output_pkl)


