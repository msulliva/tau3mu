import uproot
import matplotlib.pyplot as plt
import numpy as np

file1 = uproot.open( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/comb_signal_predictions.root' )
file2 = uproot.open( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/hist-data_predictions.root' )
file3 = uproot.open( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/11June_allvars/comb_signal_predictions.root' )
file4 = uproot.open( '/bundle/data/ATLAS/msullivan/Tau3Mu/samples/11June_allvars/hist-data_predictions.root' )
nn_output1 = file1[ 'BaseSelection_tree_Final' ].arrays( ['nn_output'], library = 'np' )['nn_output']
nn_output2 = file2[ 'BaseSelection_tree_Final' ].arrays( ['nn_output'], library = 'np' )['nn_output']
nn_output3 = file3[ 'BaseSelection_tree_Final' ].arrays( ['nn_output'], library = 'np' )['nn_output']
nn_output4 = file4[ 'BaseSelection_tree_Final' ].arrays( ['nn_output'], library = 'np' )['nn_output']

# make histo

plt.figure()
plt.hist( nn_output1, bins=np.linspace(0,1,100), histtype='step', density=False, label = 'XGBoost (mixed, sig)' )
plt.hist( nn_output2, bins=np.linspace(0,1,100), histtype='step', density=False, label = 'XGBoost (mixed, bkg)' )
plt.hist( nn_output3, bins=np.linspace(0,1,100), histtype='step', density=False, label = 'XGBoost (Ds, sig)' )
plt.hist( nn_output4, bins=np.linspace(0,1,100), histtype='step', density=False, label = 'XGBoost (Ds, bkg)' )
plt.legend( loc='upper center' )
plt.yscale( 'log' )
plt.show()
input()