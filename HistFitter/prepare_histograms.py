from ROOT import TFile

tfile1 = TFile("data/hists_carl.root")

tfile2 = TFile("data/hf-hists.root",'recreate')

bkg_plot = tfile1.Get("exp_NN0__triplet_ref_m").Clone()
bkg_plot.SetDirectory(0)
sig_plot = tfile1.Get("double-gaus_NN0__triplet_ref_m").Clone()
sig_plot.SetDirectory(0)
data_plot = tfile1.Get("data_NN0__triplet_ref_m").Clone()
data_plot.SetDirectory(0)

blind_plot = tfile1.Get("exp_NN0__triplet_ref_m").Clone()
blind_plot.SetDirectory(0)

tfile1.Close()

bkg_plot.SetName("hexp__Nom_NN0_obs_triplet_ref_m")
sig_plot.SetName("hdouble-gaus__Nom_NN0_obs_triplet_ref_m")
data_plot.SetName("hdata___NN0_obs_triplet_ref_m")
blind_plot.SetName("hSPlusBdata__Blind_NN0_obs")

bkg_plot.Write()
sig_plot.Write()
data_plot.Write()
blind_plot.Write()

tfile2.Write()
tfile2.Close()