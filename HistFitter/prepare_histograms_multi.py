from ROOT import TFile

tfile1 = TFile("data/hists.root")

tfile2 = TFile("data/hf-hists_multi.root",'recreate')

bkg_plot0 = tfile1.Get("exp_NN0__triplet_ref_m").Clone()
bkg_plot0.SetDirectory(0)
sig_plot0 = tfile1.Get("double-gaus_NN0__triplet_ref_m").Clone()
sig_plot0.SetDirectory(0)
data_plot0 = tfile1.Get("data_NN0__triplet_ref_m").Clone()
data_plot0.SetDirectory(0)
blind_plot0 = tfile1.Get("exp_NN0__triplet_ref_m").Clone()
blind_plot0.SetDirectory(0)

bkg_plot1 = tfile1.Get("exp_NN1__triplet_ref_m").Clone()
bkg_plot1.SetDirectory(0)
sig_plot1 = tfile1.Get("double-gaus_NN1__triplet_ref_m").Clone()
sig_plot1.SetDirectory(0)
data_plot1 = tfile1.Get("data_NN1__triplet_ref_m").Clone()
data_plot1.SetDirectory(0)
blind_plot1 = tfile1.Get("exp_NN1__triplet_ref_m").Clone()
blind_plot1.SetDirectory(0)

bkg_plot2 = tfile1.Get("exp_NN2__triplet_ref_m").Clone()
bkg_plot2.SetDirectory(0)
sig_plot2 = tfile1.Get("double-gaus_NN2__triplet_ref_m").Clone()
sig_plot2.SetDirectory(0)
data_plot2 = tfile1.Get("data_NN2__triplet_ref_m").Clone()
data_plot2.SetDirectory(0)
blind_plot2 = tfile1.Get("exp_NN2__triplet_ref_m").Clone()
blind_plot2.SetDirectory(0)

tfile1.Close()

bkg_plot0.SetName("hexp__Nom_NN0_obs_triplet_ref_m")
sig_plot0.SetName("hdouble-gaus__Nom_NN0_obs_triplet_ref_m")
data_plot0.SetName("hdata___NN0_obs_triplet_ref_m")
blind_plot0.SetName("hSPlusBdata__Blind_NN0_obs")

bkg_plot1.SetName("hexp__Nom_NN1_obs_triplet_ref_m")
sig_plot1.SetName("hdouble-gaus__Nom_NN1_obs_triplet_ref_m")
data_plot1.SetName("hdata___NN1_obs_triplet_ref_m")
blind_plot1.SetName("hSPlusBdata__Blind_NN1_obs")

bkg_plot2.SetName("hexp__Nom_NN2_obs_triplet_ref_m")
sig_plot2.SetName("hdouble-gaus__Nom_NN2_obs_triplet_ref_m")
data_plot2.SetName("hdata___NN2_obs_triplet_ref_m")
blind_plot2.SetName("hSPlusBdata__Blind_NN2_obs")

bkg_plot0.Write()
sig_plot0.Write()
data_plot0.Write()
blind_plot0.Write()

bkg_plot1.Write()
sig_plot1.Write()
data_plot1.Write()
blind_plot1.Write()

bkg_plot2.Write()
sig_plot2.Write()
data_plot2.Write()
blind_plot2.Write()

tfile2.Write()
tfile2.Close()