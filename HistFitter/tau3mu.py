from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic 
from math import sqrt

import os

from ROOT import gROOT
import ROOT
#ROOT.SetAtlasStyle()

analysisName = 'tau3mu'

configMgr.analysisName = analysisName

configMgr.histCacheFile = "data/hf-hists.root"
configMgr.outputFileName = "results/" + configMgr.analysisName +".root"

bgdFiles = [configMgr.histCacheFile]

# https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LuminosityForPhysics#Proton_proton_data
lumiError = 0.017 	# Relative luminosity uncertainty for 2015-2018. Now done as an overallsys

# parameters of the hypothesis test
configMgr.fixSigXSec = True  # fix SigXSec: 0, +/-1sigma 
configMgr.calculatorType = 2 # 2 = asymptotic calculator, 0 = frequentist calculator
configMgr.testStatType = 3   # 3 = one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints = 400   # number of values scanned of signal-strength for upper-limit determination of signal strength.

configMgr.writeXML = False
        
doStatError = False

configMgr.cutsDict["NN0"] = "1."
configMgr.cutsDict["NN1"] = "1."
configMgr.cutsDict["NN2"] = "1."

# signalName
sigSampleNames = ["tau3mu"]

# background samples
bkgSample    = Sample("exp__", kBlue)
bkgSample.addInputs(bgdFiles)
sigSample    = Sample("double-gaus__", kRed)
bkgSample.addInputs(bgdFiles)
dataSample    = Sample("data__", kBlack)
dataSample.setData()

#floating normalisations

# # enable statistical errors per sample

bkgSample.setStatConfig(doStatError)
sigSample.setStatConfig(doStatError)

# Norm systematic for signal

#syst_size = 0.2
#signal_norm_syst = Systematic("signal_norm", configMgr.weights, 1+syst_size, 1-syst_size, "user", "userOverallSys")

########
# Background-only fit config
########

if ( myFitType == FitType.Background ):

    bkt = configMgr.addFitConfig('BkgOnly')
    bkt.addSamples(bkgSample)
    #bkt.addSamples(sigSample)
    bkt.addSamples(dataSample)

    meas = bkt.addMeasurement(name = "NormalMeasurement", lumi = 1.0, lumiErr = 0.017)
    meas.addPOI("mu_Sig")
    #meas.addParamSetting("Lumi",True,1)

    # add the channel(s)
    # do combined ee+mumu fit

    chanSR0 = bkt.addChannel("triplet_ref_m", ["NN0"],50,1500.0,2000.0)
    #chanSR1 = bkt.addChannel("triplet_ref_m", ["NN1"],50,1500.0,2000.0)
    #chanSR2 = bkt.addChannel("triplet_ref_m", ["NN2"],50,1500.0,2000.0)

##########################

if ( myFitType == FitType.Exclusion ):

    ex = configMgr.addFitConfig('SPlusB')
    ex.addSamples(bkgSample)
    ex.addSamples(dataSample)

    configMgr.blindSR = True

    meas = ex.addMeasurement(name = "NormalMeasurement", lumi = 1.0, lumiErr = 0.017)
    meas.addPOI("mu_Sig")
    meas.addParamSetting("Lumi",True,1)

    chanSR0 = ex.addChannel("triplet_ref_m", ["NN0"],50,1500.0,2000.0)
    #chanSR1 = ex.addChannel("triplet_ref_m", ["NN1"],50,1500.0,2000.0)
    #chanSR2 = ex.addChannel("triplet_ref_m", ["NN2"],50,1500.0,2000.0)

    ex.addSignalChannels([chanSR0])

    sigSample.setNormByTheory(False)
    sigSample.setNormFactor("mu_Sig", 1.0, 0.0, 1.0)
    sigSample.setStatConfig(doStatError)

    #sigSample.addSystematic( signal_norm_syst )

    ex.addSamples(sigSample)
    ex.setSignalSample(sigSample)
# cosmetics for before/after fit plots

doplots = True
if doplots:

    # legend
    c = ROOT.TCanvas()
    compFillStyle = 1001
    leg = ROOT.TLegend(0.42,0.6,0.88,0.85)
    leg.SetFillStyle(0)
    leg.SetFillColor(0)
    leg.SetBorderSize(0)
    leg.SetNColumns(2);
    entry = ROOT.TLegendEntry()

    chanSR0.titleX = "triplet_ref_m [MeV]"
    #chanSR1.titleX = "triplet_ref_m [MeV]"
    #chanSR2.titleX = "triplet_ref_m [MeV]"

    # axes labels, ATLAS label
    for chan in ex.channels:
        chan.titleY = "Events"
        chan.titleX = "E_{T}^{miss} bin"
        chan.minY = 0
        chan.ATLASLabelX = 0.135
        chan.ATLASLabelY = 0.8
        chan.ATLASLabelText = "Internal"
    #    chan.showLumi = False

    entry = leg.AddEntry("", "Data", "lp")
    entry.SetMarkerColor(ex.dataColor)
    entry.SetMarkerStyle(20)

    entry = leg.AddEntry("", "Signal", "lf")
    entry.SetLineColor(sigSample.color)
    entry.SetFillColor(sigSample.color)
    entry.SetFillStyle(compFillStyle)

    entry = leg.AddEntry("", "Background", "lf")
    entry.SetLineColor(bkgSample.color)
    entry.SetFillColor(bkgSample.color)
    entry.SetFillStyle(compFillStyle)

    ex.tLegend = leg
