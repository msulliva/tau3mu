import ROOT as R
from ROOT import RooFit as RF
from ROOT import RooStats as RS


def main():
    R.gROOT.SetBatch(True)
    R.gROOT.LoadMacro("Fitting/StandardHypoTestInvDemo.C")
    R.StandardHypoTestInvDemo("workspace.root", "final_workspace", "SBModelConfig",
                              "", "data", 2, 3, True ,20, 0, 0.01)    

if __name__ == "__main__":
    main()
    
