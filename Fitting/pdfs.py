from __future__ import print_function

import ROOT as R
from ROOT import RooFit as RF

class PDFError(Exception):
    pass

class PDF(object):
    "Base class for PDFs"

    def __init__(self, **kwargs):
        self.var = kwargs["var"]
        self.norm = kwargs.get("norm", None)
        self.poi = kwargs.get("poi", None)        
        self.extend = kwargs.get("extend", None)        
        pass

    def apply_norm(self, pdf):
        if not self.extend:
            return pdf

        print ("Extending PDF with norm parameter", self.norm)
        R.SetOwnership(pdf, False)
        norm = R.RooRealVar("norm", "norm", *self.norm)
        R.SetOwnership(norm, False)
        print (norm.isConstant())
        epdf = R.RooExtendPdf("e" + self.name, self.name + " (ext)", pdf, norm)

        return epdf

    def build(self):
        raise NotImplementedError

class PDFChebychev(PDF):
    def __init__(self, **kwargs):
        super(PDFChebychev, self).__init__(**kwargs)
        self.name = kwargs.get("name", "cheb")
        self.coefficients = kwargs["coefficients"]


    def build(self):
        coefficients = R.RooArgList()
        for coeff in enumerate(self.coefficients):
            name = "coeff_{:d}".format(coeff[0])
            roo_coeff = R.RooRealVar(name, name, *coeff[1])
            coefficients.add(roo_coeff)
            R.SetOwnership(roo_coeff, False)
        R.SetOwnership(coefficients, False)
        
        return R.RooChebychev(self.name, self.name, self.var, coefficients)

class PDFPolynomial(PDF):
    def __init__(self, **kwargs):
        super(PDFPolynomial, self).__init__(**kwargs)        
        self.name = kwargs.get("name", "poly")
        self.coefficients = kwargs["coefficients"]

    def build(self):
        coefficients = R.RooArgList()
        for coeff in enumerate(self.coefficients):
            name = "coeff_{:d}".format(coeff[0])
            roo_coeff = R.RooRealVar(name, name, *coeff[1])
            coefficients.add(roo_coeff)
            R.SetOwnership(roo_coeff, False)
        R.SetOwnership(coefficients, False)

        return R.RooPolynomial(self.name, self.name, self.var, coefficients)


class PDFGauss(PDF):
    def __init__(self, **kwargs):
        super(PDFGauss, self).__init__(**kwargs)                
        self.name = kwargs.get("name", "poly")
        self.mean = kwargs["mean"]
        self.sigma = kwargs["sigma"]

    def build(self):
        mean = R.RooRealVar("mean", "mean", *self.mean)
        sigma = R.RooRealVar("sigma", "sigma", *self.sigma)
        R.SetOwnership(mean, False)
        R.SetOwnership(sigma, False)

        pdf = R.RooGaussian(self.name, self.name, self.var, mean, sigma)
        return self.apply_norm(pdf)

class PDFCB(PDF):
    def __init__(self, **kwargs):
        super(PDFCB, self).__init__(**kwargs)                
        self.name = kwargs.get("name", "poly")
        self.mean = kwargs["mean"]
        self.sigma = kwargs["sigma"]
        self.n = kwargs["n"]
        self.alpha = kwargs["alpha"]        

    def build(self):
        mean = R.RooRealVar("cbmean", "cbmean", *self.mean)
        sigma = R.RooRealVar("cbsigma", "cbsigma", *self.sigma)
        n = R.RooRealVar("n", "n", *self.n)
        alpha = R.RooRealVar("alpha", "alpha", *self.alpha)                
        R.SetOwnership(mean, False)
        R.SetOwnership(sigma, False)
        R.SetOwnership(n, False)
        R.SetOwnership(alpha, False)

        pdf = R.RooCBShape(self.name, self.name, self.var, mean, sigma, alpha, n)
        return self.apply_norm(pdf)


class PDF2Gauss(PDF):
    def __init__(self, **kwargs):
        super(PDF2Gauss, self).__init__(**kwargs)                
        self.name = kwargs.get("name", "poly")
        self.mean = kwargs.get("mean", None)
        self.mean1 = kwargs.get("mean1", None)
        self.mean2 = kwargs.get("mean2", None)        
        self.sigma1 = kwargs["sigma1"]
        self.sigma2 = kwargs["sigma2"]
        self.frac1 = kwargs.get("frac1", None)                

    def build(self):
        if self.mean is not None:            
            mean = R.RooRealVar("mean", "mean", *self.mean)
            R.SetOwnership(mean, False)
        else:                   
            mean1 = R.RooRealVar("mean1", "mean1", *self.mean1)
            mean2 = R.RooRealVar("mean2", "mean2", *self.mean2)
            R.SetOwnership(mean1, False)
            R.SetOwnership(mean2, False)        
        
        sigma1 = R.RooRealVar("sigma1", "sigma1", *self.sigma1)
        sigma2 = R.RooRealVar("sigma2", "sigma2", *self.sigma2)
        R.SetOwnership(sigma1, False)
        R.SetOwnership(sigma2, False)        

        frac1 = R.RooRealVar("frac1", "frac1", *self.frac1)
        R.SetOwnership(frac1, False)        

        if self.mean is not None: 
            g1 = R.RooGaussian("gaus1", "gaus1", self.var, mean, sigma1)
            g2 = R.RooGaussian("gaus2", "gaus2", self.var, mean, sigma2)
        else:
            g1 = R.RooGaussian("gaus1", "gaus1", self.var, mean1, sigma1)
            g2 = R.RooGaussian("gaus2", "gaus2", self.var, mean2, sigma2)

        R.SetOwnership(g1, False)
        R.SetOwnership(g2, False)        

        pdf = R.RooAddPdf(self.name, self.name, g1, g2, frac1)


        return self.apply_norm(pdf)

class PDFCBAndGauss(PDF):
    def __init__(self, **kwargs):
        self.name = kwargs.get("name", "poly")
        self.var = kwargs["var"]
        self.mean = kwargs.get("mean", None)
        self.gmean = kwargs.get("gmean", None)
        self.cbmean = kwargs.get("cbmean", None)        
        self.gsigma = kwargs["gsigma"]
        self.cbsigma = kwargs["cbsigma"]        
        self.n = kwargs["n"]
        self.alpha = kwargs["alpha"]        
        self.norm = kwargs.get("norm", None)
        self.cbfrac = kwargs.get("cbfrac", None)                


    def build(self):
        if self.mean is not None:            
            mean = R.RooRealVar("mean", "mean", *self.mean)
            R.SetOwnership(mean, False)
        else:                   
            gmean = R.RooRealVar("gmean", "gmean", *self.mean1)
            cbmean = R.RooRealVar("cbmean", "cbmean", *self.mean2)
            R.SetOwnership(gmean, False)
            R.SetOwnership(cbmean, False)        
        
        gsigma = R.RooRealVar("gsigma", "gsigma", *self.gsigma)
        cbsigma = R.RooRealVar("cbsigma", "cbsigma", *self.cbsigma)
        R.SetOwnership(gsigma, False)
        R.SetOwnership(cbsigma, False)        

        n = R.RooRealVar("n", "n", *self.n)
        alpha = R.RooRealVar("alpha", "alpha", *self.alpha)                
        R.SetOwnership(n, False)
        R.SetOwnership(alpha, False)

        cbfrac = R.RooRealVar("cbfrac", "cbfrac", *self.frac1)
        R.SetOwnership(frac1, False)        

        if self.mean is not None: 
            g = R.RooGaussian("gaus", "gaus", self.var, mean, gsigma)
            cb = R.RooCBShape("CB", "CB", self.var, mean, cbsigma, alpha, n)
        else:
            g = R.RooGaussian("gaus", "gaus", self.var, gmean, gsigma)
            cb = R.RooCBShape("CB", "CB", self.var, cbmean, cbsigma, alpha, n)            

        R.SetOwnership(g, False)
        R.SetOwnership(cb, False)        

        pdf = R.RooAddPdf(self.name, self.name, cb, g, cbfrac)

        return self.apply_norm(pdf)


class PDFHist(PDF):
    def __init__(self, **kwargs):
        super(PDFHist, self).__init__(**kwargs)
        self.name = kwargs.get("name", "hist")
        self.hist = kwargs["hist"]


    def build(self):
        dh = R.RooDataHist("hist", self.hist.GetTitle(), R.RooArgList(self.var), RF.Import(self.hist, 0))
        R.SetOwnership(dh, False)
        return R.RooHistPdf(self.name, self.name, R.RooArgSet(self.var), dh, 0)

class PDFAdd(PDF):
    "Add 2 or more previously defined PDFs, extending them with the Number of events"

    def __init__(self, pdfs):
        self.pdfs = pdfs

    def build(self):

        name = ""
        models = R.RooArgList()
        norms = R.RooArgList()


        for p in self.pdfs:
            if name:
                name += "+"
            name += p.name


            # Create normalisation for given PDF
            if p.extend:
                n = R.RooRealVar("n"+ p.name, p.name + "norm", *p.poi)
            elif p.poi:
                norm = R.RooRealVar("n"+ p.name, p.name + "norm", *p.norm)
                poi = R.RooRealVar("poi", "poi", *p.poi)
                R.SetOwnership(norm, False)
                R.SetOwnership(poi, False)                
                n = R.RooProduct("normxpoi", "normxpoi", R.RooArgList(poi,norm))
            else:
                n = R.RooRealVar("n"+ p.name, p.name + "norm", *p.norm)            
            R.SetOwnership(n, False)
            norms.add(n)

            # Build individual PDF
            model = p.build()
            model.Print()
            R.SetOwnership(model, False)            
            models.add(model)

        # Add individual PDFs
        return R.RooAddPdf(name, name, models, norms)
        
