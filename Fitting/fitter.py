from __future__ import print_function

import ROOT as R
from ROOT import RooFit as RF
from ROOT import RooStats as RS

import os
import argparse

from Plotting import sample, plotutils
from Fitting.pdfs import *

class Fitter:

    def __init__(self, binned = True, signal_only = False, maxevents = None, ncpus = 5, blind = False, blind_plot = True, read_cache = False):
        """
        Fitter class:
            - binned:        performed binned fit [True]
            - signal_only    just load and fit the signal to parameterise it [False]
            - maxevents:     limit number of events in unbinned case for testing [None]
            - ncpus:         number of CPUs to use for fit
            - blind:         blind the signal region (given in set_var) for the fit [False]
            - blind_plot:    blind the signal region (given in set_var) only for plot not fit [False]
            - read_cache:    read dataset and variable (but not model) from cache [False]

        """


        # DEFINE JSON FILES
        
        self.REGIONS_FILE = 'Plotting/regions.json'
        self.SAMPLES_FILE = 'Plotting/samples.json'

        # NORMALISATIONS

        self.lumi= 139./1000
        #self.MCWEIGHTSTRING = '{} * weight * xsec / sumofweights'.format(self.lumi)
        self.mc_weight_branch = "lumi_scale"
        self.MCWEIGHTSTRING = "{} * {}".format(self.lumi,self.mc_weight_branch)  

        # SETTINGS

        self.binned = binned
        self.signal_only = signal_only
        self.maxevents = maxevents
        self.injection = 0
        self.blind = blind
        self.blind_plot = blind_plot
        self.blind_range = None
        self.ncpus = ncpus
        self.cache = True
        self.from_cache = read_cache
        if self.binned:
            self.cache_name = "roocache_binned.root"
        else:
            self.cache_name = "roocache.root"            

        R.gROOT.SetBatch(True)

        # Create instance of plot utils
        self.pu = plotutils.plotutils()

        # Load signal and data (no need for bkg)
        _, tmpsignals, self.data = self.pu.loadsamples(self.SAMPLES_FILE)
        self.signals = [s for s in tmpsignals if "HF" in s.name]

        self.pdf_comps = []
        self.model = None
        
        

    def applyselections(self, tempbackgrounds, tempsignals, tempdata, variable, roovar, cuts, weights, nbins, xmin, xmax, forcebins):
        "Apply selections to create a histogram (binned) or a RooDataSet (unbinned)"

        # Currently weights not working for binned

        weight = R.RooRealVar("weight", "weight", 1)

        R.gInterpreter.ProcessLine('# include "Plotting/cppfuncs.cxx"')

#         # Don't need background for the fits
#         for background in tempbackgrounds:
#             if not self.binned:
#                 background.__getroodataset__(variable, roovar, cuts, self.mc_weight_branch, xmin, xmax, self.lumi, self.maxevents)
#             else:
#                 background.__gethist__(variable, cuts, weights, nbins, xmin, xmax, forcebins)


        if self.injection or self.signal_only or self.binned:
            for signal in tempsignals:
                if not self.binned:
                    if self.injection:
                        signal.__getroodataset__(variable, roovar, cuts, weight, self.mc_weight_branch, xmin, xmax, self.lumi * self.injection, self.maxevents)
                    else:
                        signal.__getroodataset__(variable, roovar, cuts, weight, self.mc_weight_branch, xmin, xmax, self.lumi, self.maxevents)
                else:
                    signal.__gethist__(variable, cuts, weights, nbins, xmin, xmax, forcebins)
            print (">>> Selected Signal")

        if not self.signal_only:
            for data in tempdata:
                if not self.binned:            
                    data.__getroodataset__(variable, roovar, cuts, weight, 1.0, xmin, xmax, 1.0, self.maxevents)
                else: 
                    data.__gethist__(variable, cuts, weights, nbins, xmin, xmax, forcebins)

        print (">>> Selected data")

        return
    
    def set_region(self, region = "base_selection"):
        self.cuts, self.forceblind, self.forcebins = self.pu.loadregions(self.REGIONS_FILE, region)
        return

    def set_var(self, var = "triplet_ref_m", nbins = 50, xmin = 1500, xmax = 2000, blind = (1700,1850)):
        "Create RooRealVar over correct range and apply blinding if required."

        self.var = var
        self.nbins = nbins
        self.xmin = xmin
        self.xmax = xmax

        if self.from_cache:
            self.read_cache()
            return

        self.roovar = R.RooRealVar(var, var, xmin, xmax)

        self.roovar.setRange("full", xmin, xmax)

        if blind:
            self.blind_range = blind
            self.roovar.setRange("left", xmin, blind[0])
            self.roovar.setRange("right", blind[1], xmax)

        self.applyselections([], self.signals, self.data, self.var, self.roovar, self.cuts, self.MCWEIGHTSTRING, self.nbins, self.xmin, self.xmax, self.forcebins)

        # Create roo var and dataset

        if self.binned:
            if self.signal_only:
                hdata = self.signals[0].hist
                self.roodata = R.RooDataHist("data", hdata.GetTitle(), R.RooArgList(self.roovar), RF.Import(hdata, 0))
            else: 
                hdata = self.data[0].hist

                if self.injection:
                    # Add signal with correct stength
                    hsig = self.signals[0].hist
                    hsig.Scale(self.injection)
                    hdata.Add(hsig)

                # Convert hist to RooDataHist over the RooRealVar
                self.roodata = R.RooDataHist("data", hdata.GetTitle(), R.RooArgList(self.roovar), RF.Import(hdata, 0))

        else:
            if self.signal_only:
                self.roodata = self.signals[0].dataset
            else:
                self.roodata = self.data[0].dataset            
            
                if self.injection:
                    # Add signal with stength set in apply_selection
                    sig = self.signals[0].dataset
                    self.roodata.append(sig) 

        self.write_cache()

        return

    def inject_signal(self, mu = 1):
        "Inject signal with a stenght mu"
        self.blind = self.blind_plot = False
        self.injection = mu

    def add_pdf(self, **kwargs):
        "Add PDFs based on name.  All kwargs are passed to the PDF"
        
        name = kwargs["name"]
        kwargs["var"] = self.roovar

        if name == "cheb":
            pdf = PDFChebychev(**kwargs)
        elif name == "poly":
            pdf = PDFPolynomial(**kwargs)
        elif name == "gaus":
            pdf = PDFGauss(**kwargs)
        elif name == "CB":
            pdf = PDFCB(**kwargs)            
        elif name == "double-gaus":
            pdf = PDF2Gauss(**kwargs)
        elif name == "CB-gaus":
            pdf = PDFCBAndGauss(**kwargs)
        elif name == "signal" and self.binned:
            h = self.signals[0].hist
            if self.injection:
                h.Scale(1/self.injection)
            kwargs["hist"] = h
            pdf = PDFHist(**kwargs)

        else:
            raise PDFError("No such PDF {}".format(pdf.name))

        self.pdf_comps.append(pdf)

        return


    def build_model(self):
        "Build the model from the PDF, adding them if more than 1 PDF"

        
        if len(self.pdf_comps) == 1:
            self.model = self.pdf_comps[0].build()
        else:
            self.model = PDFAdd(self.pdf_comps).build()

        print (">>> Built model")
        self.model.Print()

        return self.model            

    def write_cache(self):
        "Cache dataset and vars to file"

        if not self.cache: return

        w = R.RooWorkspace("workspace", "workspace")
        getattr(w, "import")(self.roodata)
        getattr(w, "import")(self.roovar)
        w.writeToFile(self.cache_name) 

        return

    def read_cache(self):
        "Read roofit object from file"

        if not self.from_cache: return

        print (">>> Reading from cache")

        f = R.TFile.Open(self.cache_name)
        w = f.Get("workspace")
        self.roovar = w.var(self.var)
        self.roodata = w.data("data")

        return

    def write(self, poi):
        w = R.RooWorkspace("final_workspace", "workspace")

        data = self.roodata
        getattr(w, "import")(data)

        if self.model is None:
            self.build_model()

        model = self.model
        getattr(w, "import")(model)

        mcSB = RS.ModelConfig("SBModelConfig", w)
        mcSB.SetPdf(model)        
        mcSB.SetParametersOfInterest(R.RooArgSet(model.getVariables()[poi]))
        poi = mcSB.GetParametersOfInterest().first()
        poi.setVal(1)
        mcSB.SetSnapshot(R.RooArgSet(poi))
        mcSB.SetObservables(R.RooArgSet(self.roovar))
        w.defineSet("nuisParams", "npoly,coeff_0,coeff_1")
        nps = getattr(w, "set")("nuisParams")
        mcSB.SetNuisanceParameters(nps)
        getattr(w, "import")(mcSB)


#         mcB = mcSB.Clone()
#         mcB.SetName("BModelConfig")
#         poi.setVal(0)
#         #poi.setConstant(True)        
#         mcB.SetSnapshot(R.RooArgSet(poi))
#         getattr(w, "import")(mcB)    

        w.writeToFile("workspace.root")

        return

    def convert_pdf_to_hist(self, pdf, name, norm):
        "generate a histogram from a PDF"
        #self.roovar.setBins(self.bins)
        #self.roovar.setRange(self.xmin, self.xmax)
        #dh = pdf.generateBinned(R.RooArgSet(self.roovar), nevents)
        #ds = pdf.generate(R.RooArgSet(self.roovar), nevents)
        hist = pdf.createHistogram(name, self.roovar, RF.Binning(self.nbins, self.xmin, self.xmax))
        n = self.model.getVariables()[norm].getVal()
        hist.Scale(n)        
        return hist
        
    def write_to_hist(self, norms = []):
        f = R.TFile.Open("hists.root", "RECREATE")

        itr = self.model.pdfList().createIterator()
        i = 0
        m = itr.Next()
        while m:
            comp = m
            try:
                norm = norms[i]
            except IndexError:
                norm = "n" + comp.GetName()
            h = self.convert_pdf_to_hist(comp, comp.GetName(), norm)
            h.Write()
            m = itr.Next()        
            i += 1

        self.roodata.createHistogram("data", self.roovar, RF.Binning(self.nbins, self.xmin, self.xmax)).Write()

        f.Close() 
        return
    
        
    def run(self):
        "Run the fitting"

        # Build the model
        model = self.build_model()

        # Setup the data (with injected signal if requested) and perform fit ...

        print (">>> Starting fit")

        if self.binned:

            if self.blind:
                # Blind the data based on ranges defined in set_var()

                # Fit blinded data, including the hitogram weights
                self.result = model.fitTo(self.roodata, RF.Range("left,right"), RF.Save(), RF.NumCPU(self.ncpus), RF.SumW2Error(True))
            else:
                # Fit full data, including the hitogram weights                
                self.result = model.fitTo(self.roodata, RF.Save(), RF.NumCPU(self.ncpus), RF.SumW2Error(True)) #, RF.Extended(True))
                        
        else:

            if self.blind:
                # Fit blinded data, including the hitogram weights
                # Will round to bin edges automatically if needed
                self.result = model.fitTo(self.roodata, RF.Range("left,right"), RF.Save(), RF.NumCPU(self.ncpus))                
            else:
                # Fit full data
                self.result = model.fitTo(self.roodata, RF.Save(), RF.NumCPU(self.ncpus))

        # Plot the data, blinding if requested ...


        frame = self.roovar.frame()
        if self.binned:
            if self.blind or self.blind_plot:
                self.roodata.plotOn(frame, RF.CutRange("left,right")) #, RF.VisualizeError(self.result, 1))
            else:
                self.roodata.plotOn(frame) #, RF.VisualizeError(self.result, 1))            
            
        else:
            # Recreate the binning just for the plot in unbinned fit case
            binning = R.RooBinning(self.nbins, self.xmin, self.xmax)

            if self.blind or self.blind_plot:
                # TODO: blinding data on plot doesn't seem to work even though fit is to blinded data
                #blindedData.plotOn(frame, R.RooFit.Binning(binning)) #,RF.VisualizeError(self.result, 1))
                self.roodata.plotOn(frame, RF.CutRange("left,right"), R.RooFit.Binning(binning))                
            else:
                self.roodata.plotOn(frame, R.RooFit.Binning(binning)) #,RF.VisualizeError(self.result, 1))                

        # Plot the model, including subcomponents, blinding if required ...


        if self.blind or self.blind_plot:
            # Plot the model in the blinded range as dashed line, getting the norm correct
            #model.plotOn(frame, RF.LineColor(4))
            model.plotOn(frame, RF.LineColor(2), RF.Range("left"), RF.NormRange("full"))
            model.plotOn(frame, RF.LineColor(2), RF.Range("right"), RF.NormRange("full"))
            model.plotOn(frame, RF.LineColor(2), RF.Range("full"), RF.NormRange("full"), RF.LineStyle(2))                        
        else:
            model.plotOn(frame, RF.LineColor(2))

            # Plot the subcomponents
            if len(self.pdf_comps) > 1:
                itr = self.model.pdfList().createIterator()
                m = itr.Next()

                i = 2
                while m:
                    comp = R.RooArgSet(m)
                    model.plotOn(frame, RF.Components(comp), RF.LineColor(i), RF.LineStyle(2)) #, RF.VisualizeError(self.result))
                    m = itr.Next()
                    i += 1



        can = R.TCanvas()
        frame.Draw()
        #ndata = self.roodata.numEntries()
        nparam = model.getParameters(self.roodata).selectByAttrib("Constant", False).getSize()
        latex = R.TLatex();
        latex.SetNDC(1);
        latex.SetTextSize(0.04);
        latex.DrawLatex(0.15,0.85,"#chi^{2}/ndf = " + str(round(frame.chiSquare(nparam), 2)));
        can.Print(self.var + ".eps")

        # Need to norm correctly: signal to before fit and bkg to npoly etc
        #self.write_to_hist()
        
        return self.result

if __name__ == "__main__":

    # Limit stats for unbined fit (None = all)
    maxn = None

    # Run on just signal or data (potentially with intected mock signal)
    signal_only = False

    # Setup fitter for binned or unbinned
    f = Fitter(binned = True, signal_only = signal_only, maxevents = None, blind = False, read_cache = False)

    # Set region (i.e. selection)
    f.set_region()
    #f.set_region("conor")

    # Inject mock signal
    ####f.inject_signal(0.05) # Must be called before set_var 

    # Set the variable to fit (default is 3mu mass)
    f.set_var()

    # Add the signal/background PDFs
    # Each one has the form (start value, min value, max value)

    if dosig:
        # Fit just signal to get parameters of double-guass, which seems to do an OK job.
        # Extended adds a normalisation to a single PDF that is otherwise normalised to unit area
        
        
        #f.add_pdf(name="gaus", mean = (1760, 1700, 1800), sigma = (25, 10, 50))
        #f.add_pdf(name="CB", mean = (1760, 1740, 1780), sigma = (25, 10, 30), n = (5, 0, 20), alpha = (10, -100, 100), norm = (10000, 100, 1e9))
        #f.add_pdf(name="gaus", mean = (1760, 1740, 1780), sigma = (50, 25, 100), norm = (100, 0, 1e4))
        #f.add_pdf(name="double-gaus", mean = (1760, 1740, 1780), sigma1 = (25, 10, 50), norm1 = (10000, 100, 1e9), sigma2 = (50, 25, 100), norm = (100, 0, 1e4))
        f.add_pdf(name="double-gaus", mean = (1760, 1740, 1780), sigma1 = (25, 10, 50), sigma2 = (50, 25, 100), frac1 = (0.8, 0.5, 1.), norm = (10000, 100, 1e9), extend=True)
    else:
        # Fit signal + backround with polynominal for background and the fixed double-gaus signal template from above
        # The signal normalisation is fixed to above but multiplied by an extra fraction (poi)
        
        
        # Signal has an extra relative floating norm (poi)
        #f.add_pdf(name="poly", coefficients=[(0, 0., 100.), (0, -100., 100.)], norm = (10000, 0, 1e9))
        #f.add_pdf(name="gaus", mean = (1760,), sigma = (25,) , norm = (100, 0, 1e9))
        f.add_pdf(name="poly", coefficients=[(1e-3, 0., 100.), (0, -10., 10.)], norm = (1e5, 1e4, 1e6))
        #f.add_pdf(name="poly", coefficients=[(1e-3, 0., 100.), (0, -10., 10.)], norm = (1e5, 1e4, 1e6))        
        f.add_pdf(name="double-gaus", mean = (1.77709e+03,), sigma1 = (2.83170e+01,), sigma2 = (7.65051e+01,), frac1 = (8.40660e-01,), norm = (9.67151e+04,), poi = (0.05, 0, 2))
        #f.add_pdf(name="double-gaus", mean = (1.77709e+03,), sigma1 = (2.83170e+01,), sigma2 = (7.65051e+01,), frac1 = (8.40660e-01,), norm = (1000,0,1e5))
        #f.add_pdf(name="gaus", mean = (1760, 1740, 1780), sigma = (50, 25, 100), norm = (100, 0, 1e4))
        #f.add_pdf(name="signal", norm = (0.5, 0, 1))

    # Run the fit
    f.run()
    f.write("poi")

