# https://github.com/mattleblanc/RooCustomPdfs

CFLAGS=`root-config --cflags`
LDFLAGS=`root-config --ldflags --glibs` -lRooFit -lRooFitCore -lMinuit

libRooDSCBShape.so : libRooDSCBShape.so.1.0
	ln -sf libRooDSCBShape.so.1.0 libRooDSCBShape.so

libRooDSCBShape.so.1.0 : RooDSCBShape.o RooDSCBShapeDict.o
	gcc -shared -Wl,-soname,libRooDSCBShape.so.1 -o libRooDSCBShape.so.1.0 RooDSCBShape.o RooDSCBShapeDict.o

RooDSCBShapeDict.o : RooDSCBShapeDict.cxx
	g++ -c RooDSCBShapeDict.cxx -fPIC $(CFLAGS) $(LDFLAGS)

RooDSCBShapeDict.cxx : RooDSCBShape.h
	rootcling RooDSCBShape.h > RooDSCBShapeDict.cxx

RooDSCBShape.o : RooDSCBShape.cxx RooDSCBShape.h
	g++ -c RooDSCBShape.cxx -fPIC $(CFLAGS) $(LDFLAGS)

.PHONY: clean
clean :
	rm libRooDSCBShape.so libRooDSCBShape.so.1.0 RooDSCBShapeDict.o RooDSCBShapeDict.cxx _rdict.pcm RooDSCBShape.o
