from ROOT import TFile, TH1D
import os
import argparse
import pandas as pd
import numpy as np

import CommonStats
sigma = 0.15

def maketable():

    #setup significance
    cs = CommonStats.CommonStats()
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--files')
    parser.add_argument('-s', '--sig')
    parser.add_argument('-b', '--bkg')
    parser.add_argument('--doSignificance', action='store_true')
    parser.add_argument('--doEfficiency', action='store_true')
    args = parser.parse_args()

    # get things setup
    files = args.files.split(',')
    temp_tfile = TFile(files[0])
    temp_hist = temp_tfile.Get('hist')
    n_bins = temp_hist.GetNbinsX()

    # create dataframe
    columns = ['Cuts']
    for file in files:
        file_name = file.replace('.root','').replace('output_','')
        columns.append(file_name)
        if args.doEfficiency:
            #columns.append('{} error'.format(file_name))
            columns.append('{} efficiency'.format(file_name))
    if args.doSignificance:
        columns.append('Simplified significance')
        columns.append('ATLAS significance')
        columns.append('SR significance')
    df = pd.DataFrame(columns=columns)

    # fill dataframe
    bkgs = []
    sigs = []
    for file in files:
        file_name = file.replace('.root','').replace('output_','').strip()
        if file_name in args.bkg.split(','):
            bkgs.append(file_name)
        if file_name in args.sig.split(','):
            sigs.append(file_name)
        temp_file = TFile(file)
        temp_hist = temp_file.Get('hist')
        temp_eff = temp_file.Get('hist_eff')
        for bin in range(1,n_bins+1):
            df.at[bin, 'Cuts'] = temp_hist.GetXaxis().GetBinLabel(bin)
            df.at[bin, file_name] = round(temp_hist.GetBinContent(bin), 3)
            #df.at[bin, '{} error'.format(file_name)] = round(temp_hist.GetBinError(bin), 3)
            df.at[bin, '{} efficiency'.format(file_name)] = round(temp_eff.GetBinContent(bin), 3)

    # calculate significance
    if args.doSignificance:
        for row in range(1,n_bins):
            b = 0
            db = 0
            s = 0
            for bkg in bkgs:
                b += df.loc[row,bkg]
            for sig in sigs:
                s += df.loc[row,sig]
            sig = s / np.sqrt(b + (sigma*sigma*b*b))
            sig_atlas = cs.AsymptoticPoissonPoissonModel( s, b, sigma )
            df.at[row, 'Simplified significance'] = round(sig, 3)
            df.at[row, 'ATLAS significance'] = round(sig_atlas, 3)

            sig_tfile = TFile('significance.root')
            sig_hist = sig_tfile.Get('sig_hist')
            df.at[row, 'SR significance'] = round( sig_hist.GetBinContent( row ), 3)

    create_table( df )

def create_table( df ):

    output = open('table.tex','w+')
    output.write('\\documentclass{article}\n')
    output.write('\\usepackage{lscape}\n')
    output.write('\\begin{document}\n')
    output.write('\\begin{landscape}\n')
    output.write('\\begin{table}\n')
    output.write('\\tiny\n')
    output.write('\\centering\n')
    tabdimensions = '{'
    for column in df:
        tabdimensions += 'c'
    output.write('\\begin{{tabular}}{}}}\n'.format(tabdimensions))
    line = ''
    for column in df.columns:
        line += column
        if column != df.columns[-1]:
            line += ' & '
    line += '\\\\ \n'
    output.write(line)
    for row in range(1, df.shape[0]):
        line = ''
        for column in df.columns:
            line += str(df.loc[row, column] )
            if column != df.columns[-1]:
                line += ' & '
            else:
                line += '\\\\ \n' 
        output.write(format(line))
    output.write('\\hline\n')  
    output.write('\\end{tabular}\n')
    output.write('\\end{table}\n') 
    output.write('\\end{landscape}\n') 
    output.write('\\end{document}')

def format(line):

    line = line.replace( '$', '\\$')
    line = line.replace('_','\\textunderscore ')
    line = line.replace(' >= ', ' $\\geq$ ')
    line = line.replace(' <= ', ' $\\leq$ ')
    line = line.replace(' > ', ' $ > $ ')
    line = line.replace(' < ', ' $ < $ ')
    return line

if __name__=='__main__':
    maketable()    
