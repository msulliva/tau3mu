#!/bin/bash

source /batchsoft/gcc/gccSetup.sh 6.2.0
source /user/software/cuda/cudnn-7.0-x86_64/setup.sh
source /batchsoft/python/pythonSetup.sh 3.6.0tfgpu
source /batchsoft/root/root-6.10.08-x86_64-cc7-62-py36/bin/thisroot.sh
