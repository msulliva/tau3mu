# Cutflow code explained

The cutflow code is relatively simple. The inputs are lists, defining which files and trees to use, along with the cuts to test. Equal numbers of files, trees and names must be defined, and must be structed as:
```
files = [FILE1, FILE2]
trees = [TREE1, TREE2]
names = [NAME1, NAME2]
```
The cuts must be listed in the order you wish to have the cuts tested. Finally, if using MC, include a list of the weights you wish to use. For MC, use:
```
weights = ['WEIGHT1', 'WEIGHT2', ..., 'WEIGHTN']
```
For data, simply use:
```
weights = ['1']
```
An example setup is given below: 
```
files = ['/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data.root']
trees = ['Nominal/BaseSelection_tree_Final']
names = ['data']
cuts = ['pass_trigger == 1', 'abs(triplet_refit_trk_eta[0]) < 2.5', 'abs(triplet_refit_trk_eta[1]) < 2.5', 'abs(triplet_refit_trk_eta[2]) < 2.5', 'triplet_refit_trk_pt[0] > 5500', 'triplet_refit_trk_pt[1] > 3500', 'triplet_refit_trk_pt[2] > 2500', 'triplet_vertex_pval > 0.1', 'triplet_mos1_refitted > 325', 'triplet_mos2_refitted > 325', 'triplet_mss_refitted > 325', 'triplet_mos1_refitted < 2900', 'triplet_mos2_refitted < 2900', 'abs(triplet_charge) == 1', 'colljet_pt > 20000']
weights = ['1']
```

# Producing a table

Producing a table is (hopefully) straightforward. Once you have run a cutflow using Cutflow.py, there will be a number of .root files named output_NAME.root, where NAME corresponds to each of the input samples used in the cutflow. To produce a table, you must first source the environment setup script, by doing:  
```
source setup.sh
```
This is necessary as the python library Pandas is used in the background, and is not included in the default python install. To run the script which produces the table, there are several options:
```
-f: This is a comma-separated list of the root files, without spaces, that you wish to include from the cutflow code. 
--doSignificance: If this argument is included, the significance will be calculated at each entry.
--doEfficiency: If this argument is included, the efficiency at each point will be calculated. This is calculated relative to the the first entry in the cutflow. 
--bkg: This is a comma-separated list of the files you wish to include in the background contributions of the significance calculation. The names of these backgrounds must match the names given in the cutflow code!
--sig: This is a comma-separated list of the signals you wish to include in the significance calculation. The names of these backgrounds must match the names given in the cutflow code!
```
An example of running this script is given below:  
```
python maketable_new.py -f output_data.root,output_HF.root --doSignificance --doEfficiency --bkg data --sig HF
```