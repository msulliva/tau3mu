from ROOT import TFile, TH1D
import os
import argparse

def maketable():

    #setup significance
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sig')
    parser.add_argument('-b', '--bkg')
    args = parser.parse_args()

    doSignificance = False

    if args.sig != None and args.bkg != None:
        doSignificance = True

    # Output file
    output = open('table.txt','w+')

    # Files
    files = []
    for file in os.listdir('.'):
        if '.root' in file:
            files.append(file)
    # Cuts        
    cuts = []
    file0 = TFile(files[0])
    hist0 = file0.Get('hist')
    for entry in range(1,hist0.GetNbinsX()+1):
        cuts.append(hist0.GetXaxis().GetBinLabel(entry))
    # Make table    
    output.write('\\begin{table}\n')
    output.write('\\centering\n')
    tabdimensions = '{|c|'
    for file in files:
        tabdimensions += 'c'
    if doSignificance:
        tabdimensions += 'c'
    output.write('\\begin{{tabular}}{}}}\n'.format(tabdimensions))    
    samplenames = 'Cuts '
    for file in files:
        samplenames += ' & {} '.format(file.split('output_')[1].split('.root')[0])
    samplenames += '\\\\ \\hline \n'    
    output.write(samplenames)    
    for cut in range(1,len(cuts)):
        line = ''
        line += cuts[cut]
        for file in files:
            line += ' & '
            tempfile = TFile(file)
            temphist = tempfile.Get('hist')
            line += str(round(temphist.GetBinContent(cut),2))
            line += ' $\\pm$ '
            line += str(round(temphist.GetBinError(cut),2))
        line += '\\\\ \n'    
        output.write(format(line))
    output.write('\\hline\n')  
    output.write('\\end{tabular}\n')
    output.write('\\end{table}\n')

def format(line):

    line = line.replace('_','\\textunderscore ')
    line = line.replace(' >= ', ' $\\geq$ ')
    line = line.replace(' <= ', ' $\\leq$ ')
    line = line.replace(' > ', ' $ > $ ')
    line = line.replace(' < ', ' $ < $ ')
    return line


if __name__=='__main__':
    maketable()    
