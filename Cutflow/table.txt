\begin{table}
\centering
\begin{tabular}{|c|c}
Cuts  & data \\ \hline 
abs(triplet\textunderscore refit\textunderscore trk\textunderscore eta[1]) $ < $ 2.5 & 4139499.0 $\pm$ 2034.58\\ 
abs(triplet\textunderscore refit\textunderscore trk\textunderscore eta[2]) $ < $ 2.5 & 4139126.0 $\pm$ 2034.48\\ 
triplet\textunderscore refit\textunderscore trk\textunderscore pt[0] $ > $ 5500 & 4138039.0 $\pm$ 2034.22\\ 
triplet\textunderscore refit\textunderscore trk\textunderscore pt[1] $ > $ 3500 & 4136084.0 $\pm$ 2033.74\\ 
triplet\textunderscore refit\textunderscore trk\textunderscore pt[2] $ > $ 2500 & 4136084.0 $\pm$ 2033.74\\ 
triplet\textunderscore vertex\textunderscore pval $ > $ 0.1 & 4136084.0 $\pm$ 2033.74\\ 
triplet\textunderscore mos1\textunderscore refitted $ > $ 325 & 4136084.0 $\pm$ 2033.74\\ 
triplet\textunderscore mos2\textunderscore refitted $ > $ 325 & 1849114.0 $\pm$ 1359.82\\ 
triplet\textunderscore mss\textunderscore refitted $ > $ 325 & 1705491.0 $\pm$ 1305.94\\ 
triplet\textunderscore mos1\textunderscore refitted $ < $ 2900 & 1535142.0 $\pm$ 1239.01\\ 
triplet\textunderscore mos2\textunderscore refitted $ < $ 2900 & 1463889.0 $\pm$ 1209.91\\ 
abs(triplet\textunderscore charge) == 1 & 1463889.0 $\pm$ 1209.91\\ 
colljet\textunderscore pt $ > $ 20000 & 1463889.0 $\pm$ 1209.91\\ 
 & 1463889.0 $\pm$ 1209.91\\ 
\hline
\end{tabular}
\end{table}
