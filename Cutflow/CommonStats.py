import numpy as np

class CommonStats():

    def AsymptoticPoissonPoissonModel(self, s, b, sigma):

        n = s+b
        sigma *= b

        f1 = n * np.log((n * (b+sigma**2)) / (b**2 + (n*sigma**2)))
        f2 = -(b**2/sigma**2) * np.log(1 + ((sigma**2 * (n-b)) / (b*(b+sigma**2))))

        Zn = np.sqrt(2*(f1 + f2))

        if n < b:
            return -1*Zn
        else:
            return Zn
