import numpy as np
import matplotlib.pyplot as plt
import ROOT
from ROOT import TFile, TTree, TH1D

class background_estimate:

    def __init__(self, cuts, index):

        self.cuts = cuts #cuts
        self.weights = '139 * weight * xsec * 1/(1000 * sumofweights)' #weights
        self.low_cut = 1700
        self.high_cut = 1850
        self.name = 'test{}.png'.format(index)
        self.index = index

    def run(self):

        mc_xs, mc_ws, mc_yield = self.get_mc('/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf.root', 50, 1500, 2000)
        data_xs, data_ws, data_yield = self.get_data('/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data.root', 50, 1500, 2000)

        sig = round(mc_yield / np.sqrt(data_yield + (0.15*0.15*data_yield*data_yield)), 3)

        plt.hist( mc_xs, bins=mc_xs, weights = mc_ws, label = 'HF [{}]'.format(int(mc_yield)), alpha=0.5 )
        plt.scatter( data_xs, data_ws, label = 'Data [{}]'.format(int(data_yield)), color='black' )
        plt.legend(loc='best')
        plt.ylabel( 'Events / bin' )
        plt.xlabel( 'Triplet_ref_m' )
        plt.savefig(self.name)
        print( 'Created plot: {}'.format(self.name))
        plt.gcf().clear()

        return sig

    def get_mc(self, file, nbins, bin_low, bin_high):

        file = TFile(file)
        tree = file.Get( 'Nominal/BaseSelection_tree_Final' )
        temphist = TH1D( 'temphist', 'temphist', nbins, bin_low, bin_high)
        tree.Draw( 'triplet_ref_m>>temphist', '({}1)*({})'.format( self.cuts, self.weights ), 'HIST' )

        x = []
        y = []
        errs = []

        for bin in range(1,temphist.GetNbinsX()+1):
            x.append(temphist.GetBinLowEdge(bin))
            y.append(temphist.GetBinContent(bin))
            errs.append(temphist.GetBinError(bin))

        # Convert to Pythonic stuff
        bin_spacing = (bin_high - bin_low) / nbins
        xs = np.linspace(bin_low+(bin_spacing/2), bin_high-(bin_spacing/2), nbins)
        ys = np.ones(nbins)
        ws = np.asarray(y)

        low_bin = temphist.FindBin(self.low_cut)
        high_bin = temphist.FindBin(self.high_cut)
        total = temphist.Integral(low_bin, high_bin)

        file.Close()

        return xs, ws, total

    def get_data(self, file, nbins, bin_low, bin_high):

        file = TFile(file)
        tree = file.Get( 'Nominal/BaseSelection_tree_Final' )
        temphist = TH1D( 'temphist', 'temphist', nbins, bin_low, bin_high)
        tree.Draw( 'triplet_ref_m>>temphist', '({}1)*(triplet_ref_m <= {} || triplet_ref_m >= {})'.format( self.cuts, self.low_cut, self.high_cut ), 'HIST' )

        x = []
        y = []
        errs = []

        for bin in range(1,temphist.GetNbinsX()+1):
            x.append(temphist.GetBinLowEdge(bin))
            y.append(temphist.GetBinContent(bin))
            errs.append(temphist.GetBinError(bin))

        # Convert to Pythonic stuff
        bin_spacing = (bin_high - bin_low) / nbins
        xs = np.linspace(bin_low+(bin_spacing/2), bin_high-(bin_spacing/2), nbins)
        ys = np.ones(nbins)
        ws = np.asarray(y)

        low_bin = temphist.FindBin(self.low_cut)
        high_bin = temphist.FindBin(self.high_cut)
        low_yield = temphist.GetBinContent(low_bin-1)
        high_yield = temphist.GetBinContent(high_bin)
        av_per_bin = abs(high_yield + low_yield) / 2
        total = av_per_bin * (self.high_cut - self.low_cut)/bin_spacing 

        file.Close()

        return xs, ws, total

if __name__ == '__main__':
    background_estimate().run()
