import multiprocessing as mp 
from ROOT import TFile, TTree, TH1D
import os
import background_estimate

files = ['/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-data.root','/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-Wincl-taunu.root','/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf.root']
trees = ['Nominal/BaseSelection_tree_Final','Nominal/BaseSelection_tree_Final','Nominal/BaseSelection_tree_Final']
names = ['data','W','HF']
#cuts = ['1','n_lep_signal == 1','n_lep == 1','lep1_pt >= 30000','Trigger_SingleLep == 1','lep1_match_singleLep == 1','n_jet >= 3','jet1_pt >= 50000','jet2_pt >= 50000','n_bjet > 0','bjet1_pt >= 50000','bjet2_pt < 50000','dPhimin_4 >= 0.5','met >= 250000','mt >= 150000','mt < 200000','amt2 >= 220000','ReclusteredW_Mass >= 60000']
#cuts = ['pass_trigger == 1', 'abs(triplet_refit_trk_eta[0]) < 2.5', 'abs(triplet_refit_trk_eta[1]) < 2.5', 'abs(triplet_refit_trk_eta[2]) < 2.5', 'triplet_refit_trk_pt[0] > 5500', 'triplet_refit_trk_pt[1] > 3500', 'triplet_refit_trk_pt[2] > 2500', 'triplet_vertex_pval > 0.1', 'triplet_mos1_refitted > 325', 'triplet_mos2_refitted > 325', 'triplet_mss_refitted > 325', 'triplet_mos1_refitted < 2900', 'triplet_mos2_refitted < 2900', 'abs(triplet_charge) == 1', 'colljet_pt > 20000']
#cuts = ['Sum$(triplet_muon_quality < 4) == 3', 'object_n == 1', '(triplet_mos1_refitted<1400)', '(triplet_mos2_refitted<1500)', '(triplet_mss_refitted<1600)', '(triplet_mos1_refitted>350)']
cuts = ['Sum$(triplet_muon_quality < 4) == 3', 'object_n == 1', '(triplet_mss_refitted+triplet_mos1_refitted>1400)' , '(triplet_mss_refitted+triplet_mos1_refitted<2600)' , '(triplet_mss_refitted+triplet_mos2_refitted>1200)', '(triplet_mss_refitted+triplet_mos2_refitted<2550)', '(triplet_mss_refitted+triplet_mos2_refitted<2550)','triplet_mos1_refitted<1600 ','(triplet_mos1_refitted <748.69 || triplet_mos1_refitted> 816.61)', '(triplet_mos1_refitted <985.469 || triplet_mos1_refitted> 1053.453)', '(triplet_mos2_refitted<1500)', '(triplet_mos2_refitted<1500)' ]
weights = ['139.', 'weight', 'xsec', '1/(1000 * sumofweights)']

def setup():

    sigs = []

    for cut in range(len(cuts)):

        cutstring = ''
        for i in range(cut):
            cutstring += '({}) * '.format(cuts[i])
        bkg = background_estimate.background_estimate( cuts=cutstring, index=cut ) 
        sig = bkg.run()
        sigs.append( sig )

    significance = TFile('significance.root','RECREATE')
    sig_hist = TH1D('sig_hist','sig_hist',len(cuts),1,len(cuts))
    for index, entry in enumerate(sigs):
        sig_hist.SetBinContent( index+1, entry )
    sig_hist.Write()
    significance.Close()

    pool = mp.Pool(len(files))
    results = [pool.apply_async(cutflow, (files[i], trees[i], i)) for i in range(len(files))]
    pool.close()
    pool.join()  

def cutflow(file, tree, index):

    tfile = TFile(file)
    ttree = tfile.Get(tree)

    output_name = 'output_{}.root'.format(names[index].replace(' ','_'))

    weightstring = ''
    if 'data' not in names[index] and 'Data' not in names[index]:
        for weight in range(len(weights)):
            weightstring += ' {} * '.format(weights[weight])   
    weightstring += '1' 

    tfile_out = TFile(output_name, 'recreate')
    histname = 'hist'
    hist = TH1D(histname, histname, len(cuts), 1, len(cuts))
    eff = TH1D('{}_eff'.format(histname), '{}_eff'.format(histname), len(cuts), 1, len(cuts))
    hist.Sumw2()

    temphist = TH1D("hist","hist",1,-1000,1000)
    temphist.Sumw2()

    initial_sum = -99

    for entry in range(0,len(cuts)):

        cutstring = ''
        for cut in range(entry+1):
            cutstring += '(' + cuts[cut] + ')' + ' * '
        cutstring += weightstring 

        ttree.Draw('1>>hist', cutstring)
        value = temphist.GetBinContent(1)
        if entry == 0:
            initial_sum = value
        error = temphist.GetBinError(1)
        bin = hist.GetBin(entry+1)
        hist.SetBinContent(bin, value)
        eff.SetBinContent(bin, value/initial_sum)
        hist.SetBinError(bin, error)
        hist.GetXaxis().SetBinLabel(bin, cuts[entry]) 
        eff.GetXaxis().SetBinLabel(bin, cuts[entry]) 


    
    hist.SetTitle(names[index])
    eff.SetTitle('{}_eff'.format(names[index]))
        
    tfile.Close()    
    hist.Write()
    eff.Write()
    tfile_out.Close()   

if __name__=='__main__':
    setup()            