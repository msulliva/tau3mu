from ROOT import TFile, TTree, TH2D
import root_numpy
import numpy as np
import matplotlib.pyplot as plt

cuts = ''
weights = '(139. * weight * xsec / ( 1000 * sumofweights ))'

def main():

    tfile = TFile( '/hepstore/msullivan/Tau3Mu/tuples/20190318/hist-hf.root' )
    ttree = tfile.Get( 'Nominal/BaseSelection_tree_Final' )

    #make_correlation( ttree, 'triplet_mos1_refitted', 'triplet_mos2_refitted' )
    make_correlation( ttree, 'triplet_ref_eta', 'triplet_ref_m', bins_x=[-3,3,60], bins_y=[1400,2000,60] )

def make_correlation( ttree, variable1, variable2, bins_x = [0,3000,100], bins_y = [0,3000,100] ):

    var1 = root_numpy.tree2array( ttree, branches=variable1, selection=cuts )
    var2 = root_numpy.tree2array( ttree, branches=variable2, selection=cuts )
    var1 = np.concatenate( var1 ).ravel().tolist()
    var2 = np.concatenate( var2 ).ravel().tolist()

    binsx = np.linspace( bins_x[0], bins_x[1], bins_x[2]+1 )
    binsy = np.linspace( bins_y[0], bins_y[1], bins_y[2]+1 )

    plt.gcf().clear()
    plt.hist2d( var1, var2, bins=[binsx,binsy], cmap=plt.get_cmap('plasma') )
    plt.xlabel( variable1 )
    plt.ylabel( variable2 )
    plt.savefig( '{}_{}.png'.format( variable1, variable2 ), dpi=300 )

if __name__ == '__main__':
    main()