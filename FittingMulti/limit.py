import sys
import ROOT as R
from ROOT import RooFit as RF
from ROOT import RooStats as RS

def main(name = "workspace.root"):
    R.gROOT.SetBatch(True)
    R.gROOT.LoadMacro("FittingMulti/StandardHypoTestInvDemo.C")
    # filename, workspace name, S+B model, B-only model (if empty will create from S+B with poi=0), data name,
    # limit calc type, test stat type, number of steps, min poi, max poi
    r = R.StandardHypoTestInvDemo(name, "final_workspace", "SBModelConfig", "", "data",
                                  2, 3, True ,100, 1e-9, 1e-5)    

    if r:
         return (r.GetExpectedUpperLimit(-1),
                 r.GetExpectedUpperLimit(0),
                 r.GetExpectedUpperLimit(1))


if __name__ == "__main__":
    ws = sys.argv[1]
    print(main(ws))
    
