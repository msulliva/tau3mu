from __future__ import print_function
import sys
import json
from collections import OrderedDict as odict

import numpy as np

import ROOT as R
from ROOT import RooFit as RF
from ROOT import RooStats as RS

from Plotting import sample, plotutils
from FittingMulti.pdfs import *
from FittingMulti.region import *
from FittingMulti.fitter import *
from FittingMulti.limit import main as limit

def run(cuts, name = None, signal_only = False, signal_and_background = True):
    #signal_only=True #remove this later
    nreg = len(cuts)
    if name is None:
        name = "n{}".format(nreg)

    regions = []
    if signal_only or signal_and_background:
        for n,c in cuts.items():
            # Construct regions ... can take a cut, a region config name, or a cache file 
            r = Region(n, c)
            r.set_var()                 
            #r.add_pdf(name="double-gaus", mean = (1760, 1740, 1780), sigma1 = (25, 10, 50), sigma2 = (75, 25, 100), frac1 = (0.8, 0.5, 1.), norm = (1e7, 100, 1e9), extend=True) #original
            #r.add_pdf(name="double-gaus", mean = (1760, 1740, 1780), sigma1 = (25, 10, 50), sigma2 = (75, 25, 100), frac1 = (0.4, 0.2, 1.2), norm = (1e7, 100, 1e9), extend=True)
            r.add_pdf(name="DSCB", mean = (1760, 1740, 1780), sigma = (25, 10, 50),  n1 = (10, 0, 30), alpha1 = (10, 0, 20), n2 = (10, 0, 30), alpha2 = (10, 0, 20), norm = (1e7, 100, 1e9), extend=True)  
            regions.append(r)
            
        # Make fitter ... add regegions, load data
        fsig = Fitter(name = name, binned = True, signal_only = True, maxevents = None, blind = False, blind_plot = False, read_cache = False)
        fsig.add_regions(regions) #, corr_params = ["mean"])
        fsig.load_data()
        
        # Run fitter (N times) ... and save signal output
        fsig.run(2)
        fsig.save_signals("signals_{}.root".format(name))
        del fsig

        if signal_only: sys.exit()


    mu = 0 # 0.05

    regions = []
    for n,c in cuts.items():
        # Construct regions ... can take a cut, a region config name, or a cache file 
        r = Region(n, c)
        r.set_var()        
        r.add_pdf(name="exp", n=[-1e-4, -5, 5], norm=(1e3,50,1e5))
        ##r.add_pdf(file = "signals_{}.root".format(name), name="double-gaus", norm = "norm", poi = (mu,0,1))
        r.add_pdf(file = "signals_{}.root".format(name), name="DSCB", norm = "norm", poi = (mu,0,1))
        regions.append(r)

    # Make fitter ... add regegions, load data
    f = Fitter(name = name, binned = True, signal_only = False, maxevents = None, blind = False, blind_plot = True, read_cache = False)
    f.add_regions(regions)

    # Inject if requested (must be before load data)
    if mu > 0:
        f.inject_signal(mu)

    f.load_data()

    # Run fitter (N times) ... and save output for limit
    f.run(2)
    f.write("poi", "workspace_{}.root".format(name))
    f.write_to_hist()
    del f

    # Run limit ...
    lim = limit("workspace_{}.root".format(name))

    return lim

def deunicode(pairs):
    "Decode unicode strings"
    
    new_pairs = []
    for k, v in pairs:
        if isinstance(k, unicode):
            k = k.encode()        
        if isinstance(v, unicode):
            v = v.encode()

        new_pairs.append((k, v))

    return dict(new_pairs)

def multifit_to_dict(mutlifit_bins, extracut, eta = False): 
    #takes the multifit name and convert to a dictionary for using in fit
    if extracut== '' or extracut== ' ':
        extracut= ' ' + extracut
    else:
        extracut= " && " + extracut  

    mutlifit_bins= mutlifit_bins.replace('-', '_')
    bins= mutlifit_bins.split('_')

    n_bins= len(bins)
    elements= (n_bins-1)/2
    fit_name= list(range(elements+1))
    del fit_name[0]
    
    del bins[0], bins[1]
    bins= sorted(list(set(bins)), reverse= True) 

    if eta !=False:
       fit_name2=fit_name
       fit_name.extend(fit_name2)
       bins2=bins
       bins.extend(bins2)  

    half_way = (len(bins)/2)  

    cuts_dict= {}
    if eta == False:
        for i, number in enumerate(fit_name):
            if i==0:
                cuts_dict.update({'NN{}'.format(i) : 'nn_output > {}'.format(bins[i]) + extracut})
            else:
                cuts_dict.update({'NN{}'.format(i) : 'nn_output > {} & nn_output <= {}'.format(bins[i], bins[i-1]) + extracut}) 
    else:
        for i, number in enumerate(fit_name):
            if i==0:
                cuts_dict.update({'NN{}'.format(i) : 'nn_output > {} && abs(triplet_ref_eta)<{}'.format(bins[i], eta) + extracut})
            elif i< half_way:
                cuts_dict.update({'NN{}'.format(i) : 'nn_output > {} & nn_output <= {} && abs(triplet_ref_eta)<{}'.format(bins[i], bins[i-1], eta) + extracut}) 
            if i==half_way:
                cuts_dict.update({'NN{}'.format(i) : 'nn_output > {} && abs(triplet_ref_eta)>={}'.format(bins[i], eta) + extracut})
            elif  i>= half_way:
                cuts_dict.update({'NN{}'.format(i) : 'nn_output > {} & nn_output <= {} && abs(triplet_ref_eta)>={}'.format(bins[i], bins[i-1], eta) + extracut}) 
    return(cuts_dict)
   
if __name__ == "__main__":

    if len(sys.argv) == 3:
        name = sys.argv[1]
        cuts = json.loads(sys.argv[2], object_pairs_hook = deunicode)        

        print(">>>", cuts)
        print(run(cuts, name))
    else:
        extracut = "(triplet_mos1_refitted <748.69 || triplet_mos1_refitted> 816.61) * (triplet_mos1_refitted <985.469 ||  triplet_mos1_refitted> 1053.453) * (triplet_mos2_refitted <748.69 ||  triplet_mos2_refitted> 816.61) * (triplet_mos2_refitted <985.469 ||  triplet_mos2_refitted> 1053.453)"        
        #cuts = {"NN0"  : "nn_output > 0.95 &&" + extracut,
        #        "NN1"  : "nn_output > 0.9 && nn_output <= 0.95 &&" + extracut,
        #        "NN2"  : "nn_output > 0.75 && nn_output <= 0.9 &&" + extracut}
        #cuts= {"NN0" : "test"}
        ##cuts = {"NN0"  : "nn_output > 0.89 &&" + extracut}
        #cuts = {"NN0"  : "nn_output > 0.89 && abs(triplet_ref_eta)<1.2 &&" + extracut,
        #       "NN1"  : "nn_output > 0.89 && abs(triplet_ref_eta)>=1.2 &&" + extracut} 
        # cuts = {"NN0"  : "nn_output > 0.91 && abs(triplet_ref_eta)<1.2 &&" + extracut,
        #        "NN1"  : "nn_output > 0.76 && nn_output <= 0.91 && abs(triplet_ref_eta)<1.2 &&" + extracut,
        #        "NN2"  : "nn_output > 0.61 && nn_output <= 0.76 && abs(triplet_ref_eta)<1.2 &&" + extracut,
        #        "NN3"  : "nn_output > 0.91 && abs(triplet_ref_eta)>1.2 &&" + extracut,
        #        "NN4"  : "nn_output > 0.76 && nn_output <= 0.91 && abs(triplet_ref_eta)>1.2 &&" + extracut,
        #        "NN5"  : "nn_output > 0.61 && nn_output <= 0.76 && abs(triplet_ref_eta)>1.2 &&" + extracut} 
        #cuts = {'NN2': 'nn_output > 0.7 & nn_output <= 0.85 && (triplet_mos1_refitted <748.69 || triplet_mos1_refitted> 816.61) * (triplet_mos1_refitted <985.469 ||  triplet_mos1_refitted> 1053.453) * (triplet_mos2_refitted <748.69 ||  triplet_mos2_refitted> 816.61) * (triplet_mos2_refitted <985.469 ||  triplet_mos2_refitted> 1053.453)', 'NN1': 'nn_output > 0.85 & nn_output <= 0.9 && (triplet_mos1_refitted <748.69 || triplet_mos1_refitted> 816.61) * (triplet_mos1_refitted <985.469 ||  triplet_mos1_refitted> 1053.453) * (triplet_mos2_refitted <748.69 ||  triplet_mos2_refitted> 816.61) * (triplet_mos2_refitted <985.469 ||  triplet_mos2_refitted> 1053.453)', 'NN0': 'nn_output > 0.9 && (triplet_mos1_refitted <748.69 || triplet_mos1_refitted> 816.61) * (triplet_mos1_refitted <985.469 ||  triplet_mos1_refitted> 1053.453) * (triplet_mos2_refitted <748.69 ||  triplet_mos2_refitted> 816.61) * (triplet_mos2_refitted <985.469 ||  triplet_mos2_refitted> 1053.453)'}
        #cuts= multifit_to_dict('n3_0.95-1_0.9-0.95_0.75-0.9', extracut)
        cuts= multifit_to_dict('n3_0.95-1_0.9-0.95_0.75-0.9', extracut, eta=1.2) 

        
        run(cuts)

