from __future__ import print_function

import sys
import logging
logging.basicConfig(level = logging.INFO, stream=sys.stdout, format = ">>> [%(levelname)s] %(module)s%(name)s: %(message)s")
log = logging.getLogger("Fitter")
log.name = ""

import ROOT as R
from ROOT import RooFit as RF
from ROOT import RooStats as RS

import os
import argparse
from copy import deepcopy

from Plotting import sample, plotutils
from FittingMulti.pdfs import *
from FittingMulti.region import *


        
# TODO:
#   - Fit difference in signals by hand or from file (not quite same initial params, mulitple fit iterations seem to get same result)
#   - How to use different var for each region?


class Fitter:

    def __init__(self, name = "Fitter", binned = True, signal_only = False, maxevents = None, ncpus = 5, blind = False, blind_plot = True, read_cache = False, decor_poi = False):
        """
        Fitter class:
            - binned:        performed binned fit [True]
            - signal_only    just load and fit the signal to parameterise it [False]
            - maxevents:     limit number of events in unbinned case for testing [None]
            - ncpus:         number of CPUs to use for fit
            - blind:         blind the signal region (given in set_var) for the fit [False]
            - blind_plot:    blind the signal region (given in set_var) only for plot not fit [False]
            - read_cache:    read dataset and variable (but not model) from cache [False]
            - decorr_poi:    decorrelate POI across regions

        """


        # SETTINGS
        self.name = name
        
        self.binned = binned

        self.signal_only = signal_only
        if self.signal_only:
            self.name += ".signal"

        self.maxevents = maxevents

        self.injection = 0

        self.blind = blind
        self.blind_plot = blind_plot
        self.show_sb_splusb = False

        self.ncpus = ncpus

        self.cache = True
        self.from_cache = read_cache
        self.done_cache = False
        if self.binned:
            self.cache_name = "roocache_binned.root"
        else:
            self.cache_name = "roocache.root"
        if self.signal_only:
            self.cache_name.replace(".root", ".signal.root")

        R.gROOT.SetBatch(True)

        self.regions = []
        self.pdf_comps = []
        self.model = None
        self.decorr_poi = False

        name 

        
    def add_regions(self, regions, corr_params = []):
        "Add pre-defined regions to list of regions to be fit, identifying any parameters to be correlated"

        log.info("Adding Regions ...")

        if len([r.name for r in regions]) != len(set([r.name for r in regions])):
            log.fatal("Duplicate regions found")
            sys.exit(-1)
        
        # Deep copy regions to allow to be able to change it after and reuse
        self.regions = deepcopy(regions)

        # Create a category for each region, with the name of the region
        self.categories = R.RooCategory("cat", "cat")

        for r in self.regions:
            # Set the choice of binned and maxevents based on the fitter choice
            r.binned = self.binned
            r.maxevents = self.maxevents
            r.corr_params = corr_params
            
            self.categories.defineType(r.name)            
            
        return

    def inject_signal(self, mu = 1):
        "Inject signal with a stenght mu for every region"
        self.blind = self.blind_plot = False
        self.injection = mu


    def load_data(self):
        "Apply selections to each region create a histogram (binned) or a RooDataSet (unbinned)"

        log.info("Loading {} data ...".format("binned" if self.binned else "unbinned"))

        if self.from_cache:
            self.read_cache()
            return


        for r in self.regions:
            r.apply_selections(self.injection, dosig = self.injection or self.signal_only or self.binned, dodata = not self.signal_only)

        if self.binned:
            #TODO: Allow different vars not correlated 
            varList = R.RooArgList([r.roovar for r in self.regions][0])

            # For binned, think there is a bug in the RooDataHist map ctor as doubles number of entries, giving half bin width and half entries 0
            # work arround by using TH1 version
            
            #dataMap = R.std.map("std::string, RooDataHist*")()
            dataMap = R.std.map("std::string, TH1*")()                            
            for r in self.regions:
                #dataMap.insert(R.std.pair("const std::string, RooDataHist*")(r.name, r.roodata))
                dataMap.insert(R.std.pair("const std::string, TH1*")(r.name, r.hist))                                
                
            self.roodata = R.RooDataHist("data", "data", varList, self.categories, dataMap)
        else:
            # Variables that the datasets depend on, including weights
            # (currently assumes one weight variable across all samples)
            varSet = R.RooArgSet(*[r.roovar for r in self.regions])
            for r in self.regions:                
                varSet.add(r.rooweight)

            #datavars = [r.roovar for r in self.regions]
            #datavars.extend([r.rooweight for r in self.regions])
            #varSet = R.RooArgSet(*datavars)

            # Create a combined dataset, where data from each region is labelled with its category
            dataMap = R.std.map("std::string, RooDataSet*")()                
            for r in self.regions:
                dataMap.insert(R.std.pair("const std::string, RooDataSet*")(r.name, r.roodata))

            self.roodata = R.RooDataSet("data", "data", varSet, RF.Index(self.categories), RF.Import(dataMap), RF.WeightVar(self.regions[0].rooweight.GetName())) 
            

        self.write_cache()

        return
    

    def build_model(self):
        "Build the model from the PDF for each region, combining them to a simulatneous PDF"

        if self.model:
            return self.model

        log.info("Building simultaneous model ...")

        self.model = R.RooSimultaneous("sim pdf", "sim pdf", self.categories)

        for r in self.regions:
            # Allow sngle PoI so is the same (i.e. correlated) across regions
            r.build_model(self.decorr_poi)
            self.model.addPdf(r.model, r.name)                

        return self.model

    def write_cache(self):
        "Cache dataset and vars to file"

        if not self.cache: return

        w = R.RooWorkspace("workspace", "workspace")

        getattr(w, "import")(self.roodata)

        if len(self.regions) > 1:
            for r in self.regions:
                r.roodata.SetName(r.roodata.GetName() + "_" + r.name)
                getattr(w, "import")(r.roodata)
                if hasattr(r, "hist"):
                    r.hist.SetName(r.hist.GetName()  + "_" + r.name)
                    getattr(w, "import")(r.hist)

        w.writeToFile(self.cache_name) 

        return
 
    def read_cache(self):
        "Read roofit object from file"

        if self.done_cache: return

        print (">>> Reading from cache")

        f = R.TFile.Open(self.cache_name)
        w = f.Get("workspace")

        self.roodata = w.data("data")

        self.done_cache = True

        return

    def write(self, poi_name, name = "workspace.root"):

        log.info("Writing model to file ...")
        
        w = R.RooWorkspace("final_workspace", "workspace")
        w.autoImportClassCode(True)        

        data = self.roodata
        getattr(w, "import")(data)

        if self.model is None:
            self.build_model()

        model = self.model
        getattr(w, "import")(model)

        mcSB = RS.ModelConfig("SBModelConfig", w)
        mcSB.SetPdf(model)        
        mcSB.SetParametersOfInterest(R.RooArgSet(model.getVariables()[poi_name]))
        poi = mcSB.GetParametersOfInterest().first()
        poi.setVal(1)
        mcSB.SetSnapshot(R.RooArgSet(poi))
        obs = R.RooArgSet(*[r.roovar for r in self.regions])
        obs.add(self.categories)
        mcSB.SetObservables(obs)

        non_nuis = [self.categories.GetName(), poi_name]
        non_nuis.extend([r.roovar.GetName() for r in self.regions])
        non_nuis = set(non_nuis)

        params = model.getVariables().selectByAttrib("Constant", False)
        itr = params.createIterator()
        m = itr.Next()
        
        nuis = []
        while m:
            n = m.GetName()
            if not n in non_nuis:
                nuis.append(n)
            m = itr.Next()

#         nuis = []
#         params = ["npoly", "coeff_0", "coeff_1"]
#         for r in self.regions:
#             nuis.extend([p + "_" + r.name for p in params])

        w.defineSet("nuisParams", ",".join(nuis))
        nps = getattr(w, "set")("nuisParams")
        mcSB.SetNuisanceParameters(nps)
        getattr(w, "import")(mcSB)

        w.writeToFile(name)

        return

    def save_signals(self, name = "signals.root"):

        if not self.signal_only: return

        log.info("Saving signals ...")

        w = R.RooWorkspace("workspace", "workspace")
        w.autoImportClassCode(True)
        
        for r in self.regions:
            pdf = self.model.getPdf(r.name)
            getattr(w, "import")(pdf)
        
        w.writeToFile(name)

    def convert_pdf_to_hist(self, r, pdf, name, norm):
        "generate a histogram from a PDF"
        #self.roovar.setBins(self.bins)
        #self.roovar.setRange(self.xmin, self.xmax)
        #dh = pdf.generateBinned(R.RooArgSet(self.roovar), nevents)
        #ds = pdf.generate(R.RooArgSet(self.roovar), nevents)
        hist = pdf.createHistogram(name, r.roovar, RF.Binning(r.nbins, r.xmin, r.xmax))
        n = self.model.getVariables()[norm].getVal()
        hist.Scale(n)        
        return hist
        
    def write_to_hist(self, norms = [], name = "hists.root"):
        f = R.TFile.Open(name, "RECREATE")

        for r in self.regions:
            model = self.model.getPdf(r.name)

            itr = model.pdfList().createIterator()
            i = 0
            m = itr.Next()
            while m:
                comp = m
                try:
                    norm = norms[i]
                except IndexError:
                    norm = "n" + comp.GetName()
                h = self.convert_pdf_to_hist(r, comp, comp.GetName(), norm)
                h.Write()
                m = itr.Next()        
                i += 1

            r.roodata.createHistogram("data_" + r.name, r.roovar, RF.Binning(r.nbins, r.xmin, r.xmax)).Write()

        f.Close() 
        return
    

    def runFromWs(self, fname, wsname = "final_workspace", nfit = 1):
        ""

        f = R.TFile.Open(fname)
        w = f.Get(wsname)
        self.roodata = w.data("data")
        self.model = w.pdf("sim pdf")
        self.categories = w.cat("cat")    

        self.regions = []
        for i in range(self.categories.numTypes()):
            self.categories.setIndex(i)
            r = Region(self.categories.getLabel(), noload = True)
            r.var = "triplet_ref_m"
            r.roovar = w.var(r.var)
            r.roodata = self.roodata            
            self.regions.append(r)

        self.run(nfit, plot = True)

        
    def run(self, nfit = 1, plot = True):
        "Run the fitting and make plots.  Fit can be run several times to make sure get best minima"

        # Build the model
        self.model = self.build_model()
        #model.Print()

        # Setup the data (with injected signal if requested) and perform fit ...

        log.info("Running fit ...")

        for i in range(0, nfit):
            if self.binned:                
                if self.blind:
                    # Fit blinded data, including the hitogram weights
                    self.result = self.model.fitTo(self.roodata, RF.Range("left,right"), RF.Save(), RF.NumCPU(self.ncpus), RF.SumW2Error(True))
                else:
                    # Fit full data, including the hitogram weights                
                    self.result = self.model.fitTo(self.roodata, RF.Save(), RF.NumCPU(self.ncpus), RF.SumW2Error(True)) #, RF.Extended(True))
                        
            else:

                if self.blind:
                    # Fit blinded data, including the hitogram weights
                    # Will round to bin edges automatically if needed
                    self.result = self.model.fitTo(self.roodata, RF.Range("left,right"), RF.Save(), RF.NumCPU(self.ncpus))                
                else:
                    # Fit full data
                    self.result = self.model.fitTo(self.roodata, RF.Save(), RF.NumCPU(self.ncpus))

        if not plot:
            log.info("Result summary ... ")
            self.result.Print("v")

            return self.result

        log.info("Plotting ...")

        # Loop over regions ...

        can_tot = R.TCanvas()
        can_tot._objs = []
        nX = nY = int(round(len(self.regions)**0.5 + 0.5, 0))
        can_tot.Divide(nX, nY) 

        for ican,r in enumerate(self.regions):
            frame = r.roovar.frame(RF.Title("{} ({})".format(r.name, r.roovar.GetName())))

            # Plot the data, blinding if requested ...
            
            if self.binned:
                if self.blind or self.blind_plot:
                    self.roodata.plotOn(frame, RF.Cut("cat == cat::" + r.name), RF.CutRange("left,right")) #, RF.VisualizeError(self.result, 1))
                else:
                    self.roodata.plotOn(frame, RF.Cut("cat == cat::" + r.name)) #, RF.VisualizeError(self.result, 1))            
            
            else:
                # Recreate the binning just for the plot in unbinned fit case
                binning = R.RooBinning(r.nbins, r.xmin, r.xmax)

                if self.blind or self.blind_plot:
                    self.roodata.plotOn(frame, RF.Cut("cat == cat::" + r.name), RF.CutRange("left,right"), R.RooFit.Binning(binning))                
                else:
                    self.roodata.plotOn(frame, RF.Cut("cat == cat::" + r.name), R.RooFit.Binning(binning)) #,RF.VisualizeError(self.result, 1))                

            # Plot the model, including subcomponents, blinding if required ...


            if self.blind or self.blind_plot:
                # Plot the model in the blinded range as dashed line, getting the norm correct and projecting on to correct category it multiple regions

                # S+B fit in blinded region
                if self.show_sb_splusb:
                    self.model.plotOn(frame, RF.Slice(self.categories, r.name), RF.ProjWData(R.RooArgSet(self.categories), self.roodata), RF.LineColor(2), RF.Range("left"), RF.NormRange("full"))
                    self.model.plotOn(frame, RF.Slice(self.categories, r.name), RF.ProjWData(R.RooArgSet(self.categories), self.roodata), RF.LineColor(2), RF.Range("right"), RF.NormRange("full"))

                # if blind plot only show the background component (the one with free params) in the blinded region, else the full signal + background
                if self.blind_plot and len(r.pdf_comps) > 1:
                    #m = self.model.getPdf(r.name).pdfList().find("exp_" + r.name)
                    #comp = R.RooArgSet(m)

                    itr = self.model.getPdf(r.name).pdfList().createIterator()
                    m = itr.Next()
                    while m:
                        nfree = m.getParameters(r.roodata).selectByAttrib("Constant", False).getSize()
                        if nfree:
                            comp = R.RooArgSet(m)                                           
                            self.model.plotOn(frame, RF.Components(comp), RF.Slice(self.categories, r.name), RF.ProjWData(R.RooArgSet(self.categories), self.roodata), RF.LineColor(2), RF.Range("full"), RF.NormRange("full"), RF.LineStyle(2))
                        m = itr.Next()
                else:                
                    self.model.plotOn(frame, RF.Slice(self.categories, r.name), RF.ProjWData(R.RooArgSet(self.categories), self.roodata), RF.LineColor(2), RF.Range("full"), RF.NormRange("full"), RF.LineStyle(2))                        
            else:
                self.model.plotOn(frame, RF.Slice(self.categories, r.name), RF.ProjWData(R.RooArgSet(self.categories), self.roodata), RF.LineColor(2))

                # Plot the subcomponents
                if len(r.pdf_comps) > 1:
                    itr = self.model.getPdf(r.name).pdfList().createIterator()
                    m = itr.Next()

                    i = 2
                    while m:
                        comp = R.RooArgSet(m)
                        self.model.plotOn(frame, RF.Components(comp), RF.Slice(self.categories, r.name), RF.ProjWData(R.RooArgSet(self.categories), self.roodata), RF.LineColor(i), RF.LineStyle(2)) #, RF.VisualizeError(self.result))
                        m = itr.Next()
                        i += 1



            can = R.TCanvas()
            frame.Draw()
            nparam = self.model.getParameters(self.roodata).selectByAttrib("Constant", False).getSize()
            latex = R.TLatex();
            latex.SetNDC(1);
            latex.SetTextSize(0.04);
            latex.DrawLatex(0.15,0.85,"#chi^{2}/ndf = " + str(round(frame.chiSquare(nparam), 2)))
            can.Print("plots_" + self.name + "_" + r.name + "_" + r.var + ".eps")

            can_tot.cd(ican+1)
            frame.Draw()
            latex.DrawLatex(0.15,0.85,"#chi^{2}/ndf = " + str(round(frame.chiSquare(nparam), 2)))

        can_tot.Print("plots_" + self.name + "_all_regions.eps")

        #self.write_to_hist()

        log.info("Result summary ... ")
        self.result.Print("v")
        
        return self.result

if __name__ == "__main__":

    signal_only = False

    if signal_only:
        # Construct regions ... can take a cut, a region config name, or a cache file 
        r1 = Region("NNTight", "nn_output > 0.90") 
        r1.set_var() 
        r1.add_pdf(name="double-gaus", mean = (1760, 1740, 1780), sigma1 = (25, 10, 50), sigma2 = (75, 25, 100), frac1 = (0.8, 0.5, 1.), norm = (1e7, 100, 1e9), extend=True)

        r2 = Region("NNLoose", "nn_output > 0.70 && nn_output <= 0.9") 
        r2.set_var()
        r2.add_pdf(name="double-gaus", mean = (1760, 1740, 1780), sigma1 = (25, 10, 50), sigma2 = (75, 25, 100), frac1 = (0.8, 0.5, 1.), norm = (1e7, 100, 1e9), extend=True)        

        # Make fitter ... add regegions, load data
        f = Fitter(binned = True, signal_only = True, maxevents = None, blind = False, blind_plot = False, read_cache = False)
        f.add_regions([r1, r2]) #, corr_params = ["mean"])
        f.load_data()

        # Run fitter ... and save signal output
        f.run()
        f.save_signals()
        sys.exit()


    # Decide whether to do injection or not
    mu = 0 # 0.05
    
    # Construct regions ... can take a cut, a region config name, or a cache file 

    r1 = Region("NNTight", "nn_output > 0.90") 
    r1.set_var()
    # Pdfs can be added by hand or read from a root tile (e.g. for signal)
    #r1.add_pdf(name="poly", coefficients=[(1, -100., 100.), (1, -10., 10.)], norm = (1000, 10, 1e6))
    r1.add_pdf(name="double-gaus", mean = (1.7764e+03,), sigma1 = (2.8261e+01,), sigma2 = (7.5227e+01,), frac1 = (8.3231e-01,), norm = (3.0074e+07,), poi = (mu,0,1)) # 0.9 test
    r1.add_pdf(name="exp", n=[-1e-4, -5, 5], norm=(1e3,0,1e5))
    ###r1.add_pdf(file = "signals.root", name="double-gaus", norm = "norm", poi = (0.05,0,1)) 

    r2 = Region("NNLoose", "nn_output > 0.70 && nn_output <= 0.9")
    #r2 = Region("NNLoose", "roocache.root")
    #r2 = Region("NNLoose", "roocache_binned.root")    
    r2.set_var()     
    #r2.add_pdf(name="poly", coefficients=[(1, -100., 100.), (1, -10., 10.)], norm = (10000, 10, 1e6))
    r2.add_pdf(name="double-gaus", mean = (1.7766e+03,), sigma1 = (2.8445e+01,), sigma2 = (7.7858e+01,), frac1 = (8.4105e-01,), norm = (8.1699e+07,), poi = (mu,0,1)) # 0.7 - 0.9 test
    r2.add_pdf(name="exp", n=[-1e-4, -5, 5], norm=(1e4,0,1e5))
    ###r2.add_pdf(file = "signals.root", name="double-gaus", norm = "norm", poi = (0.05,0,1)) 

    # Make fitter ... add regegions, load data
    f = Fitter(binned = True, signal_only = False, maxevents = None, blind = False, blind_plot = False, read_cache = False)
    f.add_regions([r1,r2])

    # Inject if requested (must be before load data)
    if mu > 0:
        f.inject_signal(mu)

    f.load_data()

    # Run fitter ... and save output for limit
    f.run()
    f.write("poi")
