import ROOT as R
from ROOT import RooFit as RF
from ROOT import RooStats as RS

import sys

from Plotting import sample, plotutils
from FittingMulti.pdfs import *
from FittingMulti.region import *
from FittingMulti.fitter import *
from FittingMulti.limit import main as limit

if __name__ == "__main__":

    signal_only = False
    signal_and_background = True

    cuts = {"W_NNCut_Region1": "nn_output > 0.98",
            "W_NNCut_Region2": "nn_output > 0.9 && nn_output <= 0.98",
            "HF_NNCut_Region1_B": "nn_output > 0.91 && abs(triplet_ref_eta) < 1.2",
            "HF_NNCut_Region2_B": "nn_output > 0.76 && nn_output <= 0.91 && abs(triplet_ref_eta) < 1.2",
            "HF_NNCut_Region3_B": "nn_output > 0.61 && nn_output <= 0.76 && abs(triplet_ref_eta) < 1.2",
            "HF_NNCut_Region1_E": "nn_output > 0.91 && abs(triplet_ref_eta) >= 1.2",
            "HF_NNCut_Region2_E": "nn_output > 0.76 && nn_output <= 0.91 && abs(triplet_ref_eta) >= 1.2",
            "HF_NNCut_Region3_E": "nn_output > 0.61 && nn_output <= 0.76 && abs(triplet_ref_eta) >= 1.2"                                
            }

#     cuts = { "NN0" : "nn_output > 0.95",
#              "NN1" : "nn_output > 0.60 && nn_output <= 0.95",
#              "NN2" : "nn_output > 0.45 && nn_output <= 0.6"}             


#     cuts = {"NN0" : "nn_output > 0.90",
#             "NN1" : "nn_output > 0.80 && nn_output <= 0.9",
#             "NN2" : "nn_output > 0.70 && nn_output <= 0.8", 
#             "NN3" : "nn_output > 0.60 && nn_output <= 0.7"}

#     cuts = {"Barrel" : "nn_output > 0.75 && abs(triplet_ref_eta) < 1.2",
#             "Endcap" : "nn_output > 0.75 && abs(triplet_ref_eta) >= 1.2"}
# 
# 
#     cuts = {"Barrel" : "barrel", 
#             "Endcap" : "endcap"}

#     cuts = {"Barrel_Tight" : "nn_output > 0.9 && abs(triplet_ref_eta) < 1.2",
#             "Barrel_Medium" : "nn_output > 0.8 &&  nn_output <= 0.9 && abs(triplet_ref_eta) < 1.2",
#             "Barrel_Loose" : "nn_output > 0.7 &&  nn_output <= 0.8 && abs(triplet_ref_eta) < 1.2",
#             "Barrel_VLoose" : "nn_output > 0.6 &&  nn_output <= 0.7 && abs(triplet_ref_eta) < 1.2",                        
#             "Endcap_Tight" : "nn_output > 0.9 && abs(triplet_ref_eta) >= 1.2",
#             "Endcap_Medium" : "nn_output > 0.8 &&  nn_output <= 0.9 && abs(triplet_ref_eta) >= 1.2",
#             "Endcap_Loose" : "nn_output > 0.7 &&  nn_output <= 0.8 && abs(triplet_ref_eta) >= 1.2",
#             "Endcap_VLoose" : "nn_output > 0.6 &&  nn_output <= 0.7 && abs(triplet_ref_eta) >= 1.2"}


    nreg = len(cuts)
    regions = []

    if signal_only or signal_and_background:
        for n,c in cuts.items():

            # Construct regions ... can take a cut, a region config name, or a cache file 

            if n.startswith("W"):
                r = Region(n, c, regions_file = 'Plotting/regions_julia.json', samples_file = 'Plotting/samples_julia.json')
            else:
                r = Region(n, c, regions_file = 'Plotting/regions.json', samples_file = 'Plotting/samples_matt_new.json')
                
            r.set_var()                 
            r.add_pdf(name="double-gaus", mean = (1760, 1740, 1780), sigma1 = (25, 10, 50), sigma2 = (75, 25, 100), frac1 = (0.8, 0.5, 1.), norm = (1e7, 100, 1e9), extend=True)
            #r.add_pdf(name="CB-gaus", mean = (1760, 1740, 1780), cbsigma = (25, 10, 50), gsigma = (75, 25, 100), cbfrac = (0.9, 0.7, 1.), norm = (1e7, 100, 1e9), n = (5, 0, 20), alpha = (10, -100, 100), extend=True)
            #r.add_pdf(name="DSCB", mean = (1760, 1740, 1780), sigma = (25, 10, 50),  n1 = (5, 0, 20), alpha1 = (10, -100, 100), n2 = (5, 0, 20), alpha2 = (10, -100, 100), norm = (1e7, 100, 1e9), extend=True)
            regions.append(r)
            
        # Make fitter ... add regegions, load data
        fsig = Fitter(binned = True, signal_only = True, maxevents = None, blind = False, blind_plot = False, read_cache = False)
        fsig.add_regions(regions) #, corr_params = ["mean"])
        fsig.load_data()
        
        # Run fitter ... and save signal output
        fsig.run(2)
        fsig.save_signals("signals{}.root".format(nreg))
        del fsig

        if signal_only: sys.exit()


    mu = 0# 1e-8 # 0.05

    regions = []
    for n,c in cuts.items():
        if n.startswith("W"):
            r = Region(n, c, regions_file = 'Plotting/regions_julia.json', samples_file = 'Plotting/samples_julia.json')
        else:
            r = Region(n, c, regions_file = 'Plotting/regions.json', samples_file = 'Plotting/samples_matt_new.json')
        
        r.set_var()
        r.add_pdf(name="exp", n=[-1e-4, -5, 5], norm=(1e4,50,1e5))
        #r.add_pdf(name="signal")
        r.add_pdf(file = "signals{}.root".format(nreg), name="double-gaus", norm = "norm", poi = (mu,0,1))
        #r.add_pdf(file = "signals{}.root".format(nreg), name="DSCB", norm = "norm", poi = (mu,0,1))        
        regions.append(r)

    # Make fitter ... add regegions, load data
    f = Fitter(binned = True, signal_only = False, maxevents = None, blind = False, blind_plot = True, read_cache = False)
    f.add_regions(regions)

    # Inject if requested (must be before load data)
    if mu > 0:
        f.inject_signal(mu)

    f.load_data()

    # Run fitter (N times) ... and save output for limit
    f.run(2)
    f.write("poi", "workspace{}.root".format(nreg))

    # Run limit ...
    limit("workspace{}.root".format(nreg))
