from __future__ import print_function

import ROOT as R
from ROOT import RooFit as RF

class PDFError(Exception):
    pass

class PDF(object):
    "Base class for PDFs"

    _params = {}

    def __init__(self, **kwargs):

        # RooRealVar to fit
        self.var = kwargs["var"]           

        # Normalisation of PDF if extend or combine with another
        self.norm = kwargs.get("norm", None)

        # PoI relative to normalisation
        self.poi = kwargs.get("poi", None)        

        # Extend a fit by adding a normalisation (only works for PDFs that are extendable)
        self.extend = kwargs.get("extend", None)

        # Add a label to all parameters, this allows them to be decorrelated from other regions
        self.label = kwargs.get("label", "")

        # Parameter names to correlate across regions
        self.correlated = kwargs.get("correlated", [])

        pass

    def create_param(self, name, val):
        "Make a RooRealVar, dealing with ownership, label and correlations"

        if self.label and not name in self.correlated:
            name += "_" + self.label

        # if name exists get that var rather than a new one with same name to allow correlating
        if name in self._params:
            return self._params[name]

        var = R.RooRealVar(name, name, *val)
        R.SetOwnership(var, False)

        self._params[name] = var

        return var

    def apply_norm(self, pdf):
        "Extend PDF with normalisation if requested"

        if not self.extend:
            return pdf

        print ("Extending PDF with norm parameter", self.norm)
        R.SetOwnership(pdf, False)
        norm = self.create_param("norm", self.norm) # R.RooRealVar("norm", "norm", *self.norm)
        R.SetOwnership(norm, False)
        epdf = R.RooExtendPdf("e" + self.name, self.name + " (ext)", pdf, norm)

        return epdf

    def build(self):
        "Method to be implemented by all subclasses to return the RooAbsPDF"
        raise NotImplementedError

class PDFChebychev(PDF):
    def __init__(self, **kwargs):
        super(PDFChebychev, self).__init__(**kwargs)
        self.name = kwargs.get("name", "cheb")
        self.coefficients = kwargs["coefficients"]


    def build(self):
        coefficients = R.RooArgList()
        for coeff in enumerate(self.coefficients):
            name = "coeff_{:d}".format(coeff[0])
            roo_coeff = self.create_param(name, coeff[1])
            coefficients.add(roo_coeff)

        R.SetOwnership(coefficients, False)
        
        return R.RooChebychev(self.name, self.name, self.var, coefficients)

class PDFPolynomial(PDF):
    def __init__(self, **kwargs):
        super(PDFPolynomial, self).__init__(**kwargs)        
        self.name = kwargs.get("name", "poly")
        self.coefficients = kwargs["coefficients"]

    def build(self):
        coefficients = R.RooArgList()
        for coeff in enumerate(self.coefficients):
            name = "coeff_{:d}".format(coeff[0])
            roo_coeff = self.create_param(name, coeff[1])
            coefficients.add(roo_coeff)

        R.SetOwnership(coefficients, False)

        return R.RooPolynomial(self.name, self.name, self.var, coefficients)

class PDFExp(PDF):
    def __init__(self, **kwargs):
        super(PDFExp, self).__init__(**kwargs)        
        self.name = kwargs.get("name", "exp")
        self.n = kwargs["n"]

    def build(self):
        n = self.create_param("n", self.n) 
        return R.RooExponential(self.name, self.name, self.var, n)

class PDFGauss(PDF):
    def __init__(self, **kwargs):
        super(PDFGauss, self).__init__(**kwargs)                
        self.name = kwargs.get("name", "gaus")
        self.mean = kwargs["mean"]
        self.sigma = kwargs["sigma"]

    def build(self):
        mean = self.create_param("mean", self.mean)
        sigma = self.create_param("sigma", self.sigma)

        pdf = R.RooGaussian(self.name, self.name, self.var, mean, sigma)
        return self.apply_norm(pdf)

class PDFCB(PDF):
    def __init__(self, **kwargs):
        super(PDFCB, self).__init__(**kwargs)                
        self.name = kwargs.get("name", "CB")
        self.mean = kwargs["mean"]
        self.sigma = kwargs["sigma"]
        self.n = kwargs["n"]
        self.alpha = kwargs["alpha"]        

    def build(self):
        mean = self.create_param("cbmean", self.mean)
        sigma = self.create_param("cbsigma", self.sigma)
        n = self.create_param("n", self.n)
        alpha = self.create_param("alpha", self.alpha)                

        pdf = R.RooCBShape(self.name, self.name, self.var, mean, sigma, alpha, n)
        return self.apply_norm(pdf)


class PDF2Gauss(PDF):
    def __init__(self, **kwargs):
        super(PDF2Gauss, self).__init__(**kwargs)                
        self.name = kwargs.get("name", "double-gaus")
        self.mean = kwargs.get("mean", None)
        self.mean1 = kwargs.get("mean1", None)
        self.mean2 = kwargs.get("mean2", None)        
        self.sigma1 = kwargs["sigma1"]
        self.sigma2 = kwargs["sigma2"]
        self.frac1 = kwargs.get("frac1", None)                

    def build(self):
        if self.mean is not None:            
            mean = self.create_param("mean", self.mean)
        else:                   
            mean1 = self.create_param("mean1", self.mean1)
            mean2 = self.create_param("mean2", self.mean2)
        
        sigma1 = self.create_param("sigma1", self.sigma1)
        sigma2 = self.create_param("sigma2", self.sigma2)

        frac1 = self.create_param("frac1", self.frac1)

        # Label is important here too to decorrelate between regions
        if self.label:
            name1 = "gaus1_" + self.label
            name2 = "gaus2_" + self.label            

        if self.mean is not None:
            g1 = R.RooGaussian(name1, name1, self.var, mean, sigma1)
            g2 = R.RooGaussian(name2, name2, self.var, mean, sigma2)
        else:
            g1 = R.RooGaussian(name1, name2, self.var, mean1, sigma1)
            g2 = R.RooGaussian(name1, name2, self.var, mean2, sigma2)

        R.SetOwnership(g1, False)
        R.SetOwnership(g2, False)        

        pdf = R.RooAddPdf(self.name, self.name, g1, g2, frac1)
        
        return self.apply_norm(pdf)

class PDFCBAndGauss(PDF):
    def __init__(self, **kwargs):
        super(PDFCBAndGauss, self).__init__(**kwargs) 
        self.name = kwargs.get("name", "CB-gaus")
        self.var = kwargs["var"]
        self.mean = kwargs.get("mean", None)
        self.gmean = kwargs.get("gmean", None)
        self.cbmean = kwargs.get("cbmean", None)        
        self.gsigma = kwargs["gsigma"]
        self.cbsigma = kwargs["cbsigma"]        
        self.n = kwargs["n"]
        self.alpha = kwargs["alpha"]        
        self.norm = kwargs.get("norm", None)
        self.cbfrac = kwargs.get("cbfrac", None)                


    def build(self):
        if self.mean is not None:            
            mean = self.create_param("mean", self.mean)
        else:                   
            gmean = self.create_param("gmean", self.mean1)
            cbmean = self.create_param("cbmean", self.mean2)

        gsigma = self.create_param("gsigma", self.gsigma)
        cbsigma = self.create_param("cbsigma", self.cbsigma)

        n = self.create_param("n", self.n)
        alpha = self.create_param("alpha", self.alpha)                

        cbfrac = self.create_param("cbfrac", self.cbfrac)

        # Label is important here too to decorrelate between regions
        if self.label:
            gname = "gaus_" + self.label
            cbname = "gaus_" + self.label        

        if self.mean is not None: 
            g = R.RooGaussian(gname, gname, self.var, mean, gsigma)
            cb = R.RooCBShape(cbname, cbname, self.var, mean, cbsigma, alpha, n)
        else:
            g = R.RooGaussian(gname, gname, self.var, gmean, gsigma)
            cb = R.RooCBShape(cbname, cbname, self.var, cbmean, cbsigma, alpha, n)            

        R.SetOwnership(g, False)
        R.SetOwnership(cb, False)        

        pdf = R.RooAddPdf(self.name, self.name, cb, g, cbfrac)

        return self.apply_norm(pdf)


class PDFDSCB(PDF):

    @staticmethod
    def load():
        R.gSystem.CompileMacro("RooCustomPdfs/RooDSCBShape.cxx", "", "libRooDSCBShape")
        R.gROOT.ProcessLine('#include "RooCustomPdfs/RooDSCBShape.h"')
        R.gSystem.Load("RooCustomPdfs/libRooDSCBShape.so")

        
    
    def __init__(self, **kwargs):
        self.load()
        
        super(PDFDSCB, self).__init__(**kwargs)                
        self.name = kwargs.get("name", "DSCB")
        self.mean = kwargs["mean"]
        self.sigma = kwargs["sigma"]
        self.n1 = kwargs["n1"]
        self.alpha1 = kwargs["alpha1"]        
        self.n2 = kwargs["n2"]
        self.alpha2 = kwargs["alpha2"]        


    def build(self):
        mean = self.create_param("cbmean", self.mean)
        sigma = self.create_param("cbsigma", self.sigma)
        n1 = self.create_param("n1", self.n1)
        n2 = self.create_param("n2", self.n2)        
        alpha1 = self.create_param("alpha1", self.alpha1)
        alpha2 = self.create_param("alpha2", self.alpha2)                        

        pdf = R.RooDSCBShape(self.name, self.name, self.var, mean, sigma, alpha1, n1, alpha2, n2)
        return self.apply_norm(pdf)


class PDFHist(PDF):
    # TODO: check this works with regions
    def __init__(self, **kwargs):
        super(PDFHist, self).__init__(**kwargs)
        self.name = kwargs.get("name", "hist")
        self.hist = kwargs["hist"]


    def build(self):
        dh = R.RooDataHist("hist", self.hist.GetTitle(), R.RooArgList(self.var), RF.Import(self.hist, 0))
        R.SetOwnership(dh, False)
        return R.RooHistPdf(self.name + "_" + self.label, self.name + "_" + self.label, R.RooArgSet(self.var), dh, 0)

class PDFFile(PDF):
    "Load pdf with given name from specified file, along with norm if name given"

    def __init__(self, **kwargs):
        super(PDFFile, self).__init__(**kwargs)
        self.name = kwargs.get("name", "")
        self.fname = kwargs["file"]

        # Hack
        #if "DSCB" in self.name:
        #     PDFDSCB.load()

        f = R.TFile.Open(self.fname)
        self.ws = f.Get("workspace")
        f.Close()

        if "norm" in kwargs:
            norm = kwargs["norm"]
            if self.label:
                norm += "_" + self.label
                
            self.norm = (self.ws.var(norm).getVal(),)

    def build(self):
        pdf = self.ws.pdf(self.name)
        # Rest all parameters constant
        pdf.getVariables().setAttribAll("Constant", True)
        return pdf
    

class PDFAdd(PDF):
    "Add 2 or more previously defined PDFs, extending them with the number of events"

    def __init__(self, pdfs):
        self.pdfs = pdfs

    def build(self, decorr_poi = False):

        name = ""
        models = R.RooArgList()
        norms = R.RooArgList()
        
        for p in self.pdfs:
            if name:
                name += "+"

            name += p.name
                
            # Create normalisation for given PDF
            
            if p.extend:
                # If already extended (i.e. has used "norm") use poi as an extra norm
                n = R.RooRealVar("n"+ p.name, p.name + "norm", *p.poi)
            elif p.poi:
                # If not extended but has a poi defined, then use the product of the norm and poi                
                norm = R.RooRealVar("n"+ p.name, "n"+ p.name, *p.norm)

                # if decorrelate poi give them separate names; else if poi exists get that var rather than a new one with same name to allow correlating
                if decorr_poi:
                    poi = R.RooRealVar("poi_" + p.label, "poi_" + p.label, *p.poi)
                elif "poi" in self._params:
                    poi = self._params["poi"]
                else:                
                    poi = R.RooRealVar("poi", "poi", *p.poi)
                    self._params["poi"] = poi

                R.SetOwnership(norm, False)
                R.SetOwnership(poi, False)                

                n = R.RooProduct("normxpoi_"+ p.label, "normxpoi_" + p.label, R.RooArgList(poi,norm))
            else:
                # Else just use the norm as is
                n = R.RooRealVar("n"+ p.name, "n" + p.name, *p.norm)            

            R.SetOwnership(n, False)
            norms.add(n)

            # Build individual PDF
            model = p.build()
            #model.Print("v")
            R.SetOwnership(model, False)            
            models.add(model)

        # Add individual PDFs
        return R.RooAddPdf(name, name, models, norms)
        
