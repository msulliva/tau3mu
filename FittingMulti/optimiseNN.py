from __future__ import print_function
from collections import OrderedDict as odict

import numpy as np

from Plotting import sample, plotutils
from FittingMulti.pdfs import *
from FittingMulti.region import *
from FittingMulti.fitter import *
from FittingMulti.limit import main as limit

# Define the various cuts on the NN to test for different numbers of regions

step = 0.1
eps = step/10

region_defs = {
    1 : [(round(x,2),) for x in np.arange(0.7, 0.95 + eps, step)],
    2 :  [(round(x,2),round(y,2)) for x in np.arange(0.7, 0.95+eps, step) for y in np.arange(0.5, x-step+eps, step)],
    3 : [(round(x,2),round(y,2),round(z,2)) for x in np.arange(0.7, 0.95+eps, step) for y in np.arange(0.5, x-step+eps, step) for z in np.arange(0.4, y-step+eps, step)],
    }

step = 0.05
eps = step/10

#region_defs = {
#    3 : [(round(x,2),round(y,2),round(z,2)) for x in np.arange(0.8, 0.95+eps, step) for y in np.arange(0.6, x-step+eps, step) for z in np.arange(0.5, y-step+eps, step)],
#    }    

num = sum([len(b) for b in region_defs.values()])
print ("#combos =", num)
print (region_defs)

# Loop over the different number of regions ...

out = odict()
for n, combos in region_defs.items():
    print ("#regions", n)

    # Loop over each combo for that Nreg ...

    #print (combos)
    for bounds in combos:
        cuts = odict()
        print (bounds)

        # Translate the bounds into a cuts dict with a unique name and avoiding any overlap in regions
        name = "n{}".format(n)
        for i,b in enumerate(bounds):
            name += "_" + str(b)
            if i == 0:
                cuts["n{}_{}".format(n, b)] = "nn_output > {}".format(b)
            else:
                cuts["n{}_{}".format(n, b)] = "nn_output > {} && nn_output <= {}".format(b, bounds[i-1])

        print(cuts)

        # Run the signal-only fit for this cut setup

        regions = []
        for ncut, scut in cuts.items():
            reg = Region(ncut, scut)
            reg.set_var()
            reg.add_pdf(name="double-gaus", mean = (1760, 1740, 1780), sigma1 = (25, 10, 50), sigma2 = (75, 25, 100), frac1 = (0.8, 0.5, 1.), norm = (1e7, 100, 1e9), extend=True)
            regions.append(reg)
            
        fsig = Fitter(binned = True, signal_only = True, maxevents = None, blind = False, blind_plot = False, read_cache = False)
        fsig.add_regions(regions) 
        fsig.load_data()
        fsig.run(2)
        fsig.save_signals("signals-{}.root".format(name))

        # TODO: print the fit plots to a different name (with signal in) to avoid being overwritten by the S+B ones below

        del fsig
        regions = []
        mu = 0

        # Run the S+B fit for this cut setup

        for ncut, scut in cuts.items():
            reg = Region(ncut, scut)
            reg.set_var()
            reg.add_pdf(name="exp", n=[-1e-4, -5, 5], norm=(1e4,50,1e5))
            reg.add_pdf(file = "signals-{}.root".format(name), name="double-gaus", norm = "norm", poi = (mu,0,1))  
            regions.append(reg)
            
        f = Fitter(binned = True, signal_only = False, maxevents = None, blind = False, blind_plot = True, read_cache = False)
        f.add_regions(regions) 
        f.load_data()
        f.run(2)
        f.write("poi", "workspace-{}.root".format(name))

        # Run the limit for this cut setup, where the limit code now returns (-1, 0, +1) limits

        lim = limit("workspace-{}.root".format(name))
        out[name] = lim

        del f

# Print the dictionary of results
print(out)

# Convert to a list of tuples and sort based on the expected limit in reverse so best is last
sout = sorted([(k,v) for k,v in out.items()], key = lambda x : x[1][1], reverse = True)

# Print this
for k,v in sout:
    print ("{:15s}: {:10.3g} ({:.3g}, {:.3g})".format(k, v[1], v[0], v[2]))
