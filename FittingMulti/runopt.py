from __future__ import print_function
import subprocess as sub
import os
import json
import numpy as np
from collections import OrderedDict as odict
from glob import glob
import shutil

def make_combos(regions, extracut = ""):
    "Make the combinations of all regions"

    if extracut:
        extracut = " && " + extracut

    out = odict()    
    for n, combos in regions.items():
        print ("#regions", n)

        # Loop over each combo for that Nreg ...

        # print (combos)
        for bounds in combos:
            cuts = {}
            # print (">>>", bounds)

            # Translate the bounds into a cuts dict with a unique name and avoiding any overlap in regions
            name = "n{}".format(n)
            for i,b in enumerate(bounds):
                if i == 0:
                    name += "_{}-1".format(b)
                    cuts["n{}_{}".format(n, b)] = "nn_output > {} {}".format(b, extracut)
                else:
                    name += "_{}-{}".format(b, bounds[i-1])
                    cuts["n{}_{}".format(n, b)] = "nn_output > {} && nn_output <= {} {}".format(b, bounds[i-1], extracut)

            out[name] = cuts
    return out


if __name__ == "__main__":

    extracut = "(triplet_mos1_refitted <748.69 ||  triplet_mos1_refitted> 816.61) * (triplet_mos1_refitted <985.469 ||  triplet_mos1_refitted> 1053.453) * (triplet_mos2_refitted <748.69 ||  triplet_mos2_refitted> 816.61) * (triplet_mos2_refitted <985.469 ||  triplet_mos2_refitted> 1053.453)"

    step = 0.05
    eps = step/10
    
    region_defs = {
        2 : [(round(x,2),round(y,2)) for x in np.arange(0.85, 0.95+eps, step) for y in np.arange(0.7, x-step+eps, step)],
        3 : [(round(x,2),round(y,2),round(z,2)) for x in np.arange(0.8, 0.95+eps, step) for y in np.arange(0.6, x-step+eps, step) for z in np.arange(0.5, y-step+eps, step)],
        4: [(round(x,2),round(y,2),round(z,2), round(a, 2)) for x in np.arange(0.85, 0.95+eps, step) for y in np.arange(0.75, x-step+eps, step) for z in np.arange(0.7, y-step+eps, step) for a in np.arange(0.5, z-step+eps, step)]
    }
    
    #region_defs = {3 : [(0.95, 0.75, 0.65), (0.95, 0.85, 0.65)]}

    combos = make_combos(region_defs, extracut)
    
    
    out = odict() 
    for name, cuts in combos.items():
        print ("="*50)
        print (name)
        print ("="*50)            
        print (cuts)

        # Call single fit as script to avoid memory/caching issues causing non-reproducibility
        res = sub.check_output(["python", "FittingMulti/runfit.py", name, json.dumps(cuts)])
        with open("output_{}.txt".format(name), "w") as f:
            f.write(res)
        out[name] = eval(res.strip().split("\n")[-1])

        # Move outputs
        files = glob("*{}*".format(name))

        if os.path.exists(name):
            shutil.move(name, "{}.back".format(name))
        os.mkdir(name)
                                
        for f in files:
            shutil.move(f, name)

        
    # Print the dictionary of results
    print(out)
    
    # Convert to a list of tuples and sort based on the expected limit in reverse so best is last
    sout = sorted([(k,v) for k,v in out.items()], key = lambda x : x[1][1], reverse = True)
    
    # Print this
    for k,v in sout:
        print ("{:15s}: {:10.3g} ({:.3g}, {:.3g})".format(k, v[1], v[0], v[2]))
        
    
