from __future__ import print_function
import sys

import logging
logging.basicConfig(level = logging.INFO, stream=sys.stdout, format = ">>> [%(levelname)s] %(module)s%(name)s: %(message)s")
rlog = logging.getLogger("temp")

import ROOT as R
from ROOT import RooFit as RF
from ROOT import RooStats as RS

from Plotting import sample, plotutils
from FittingMulti.pdfs import *


class Region:


    def __init__(self, name, selection = "", noload = False,
                 regions_file = 'Plotting/regions.json', samples_file = 'Plotting/samples.json',
                 lumi = 139., mcweightbranch = "lumi_scale"): 

        self.name = name
        self.selection = selection
        rlog.name = "({})".format(name)

        # Samples and lumi
        self.regions_file = regions_file        
        self.samples_file = samples_file

        self.lumi = lumi # 139 # ./1000
        self.mcweightbranch = mcweightbranch #"lumi_scale"
        self.mcweightstring = "{} * {}".format(lumi, mcweightbranch) 

        # Create instance of plot utils
        self.pu = plotutils.plotutils()
        if not noload:
            self.set_region()

        self.blind_range = None


        # To be set by fitter
        self.binned = None 
        self.maxevents = None
        self.corr_params = []

        self.pdf_comps = []

    @property
    def log(self):
        rlog.name = "({})".format(self.name)
        return rlog

    def __repr__(self):
        return "Region({},{})".format(self.name, self.cuts)

    def __str__(self):
        return self.__repr__()

    def set_region(self):
        """
        A region can be one specified in the regions.json or directly by a cut itself
        It can also be a file, in which case it is read from the cache
        """ 

        self.log.info("Setting Region")

        self.forceblind = False
        self.forcebins = []

        if any([s in self.selection for s in "> < == !".split()]):
            self.cuts = self.selection
        elif ".root" in self.selection:
            self.cuts = self.selection
        else:
            self.cuts, _, _ = self.pu.loadregions(self.regions_file, self.selection)
            print ("---->", self.cuts)

        return

    def set_var(self, var = "triplet_ref_m", nbins = 50, xmin = 1500, xmax = 2000, blind = (1700,1850)):
        "Create RooRealVar over correct range and apply blinding if required."

        self.log.info("Setting Variable")

        self.var = var
        self.nbins = nbins
        self.xmin = xmin
        self.xmax = xmax

        self.roovar = R.RooRealVar(var, var, xmin, xmax)
        self.roovar.setRange("full", xmin, xmax)

        if blind:
            self.blind_range = blind
            self.roovar.setRange("left", xmin, blind[0])
            self.roovar.setRange("right", blind[1], xmax)

        self.rooweight = R.RooRealVar("weight", "weight", 1) 

        return

    def apply_selections(self, injection = 0, dosig = True, dodata = True):
        "Apply selections to create a histogram (binned) or a RooDataSet (unbinned)"

        # Read this region from a cache file
        if ".root" in self.cuts:
            self.log.info("Reading from cache")
            f = R.TFile.Open(self.cuts)
            w = f.Get("workspace")
            self.roodata = w.data("data_" + self.name)
            if self.binned:
                self.hist = w.obj("hist_" + self.name)
            f.Close()
            return

        self.log.info("Applying selection")

        # Load signal and data (no need for bkg)
        _, tmpsignals, self.data = self.pu.loadsamples(self.samples_file)
        self.signals = [s for s in tmpsignals] # if "HF" in s.name]
        

#         # Don't need background for the fits
#         for background in tempbackgrounds:
#             if not self.binned:
#                 background.__getroodataset__(variable, roovar, cuts, self.mc_weight_branch, xmin, xmax, self.lumi, self.maxevents)
#             else:
#                 background.__gethist__(variable, cuts, weights, nbins, xmin, xmax, forcebins)


        if dosig:
            for signal in self.signals:
                if not self.binned:
                    if injection:
                        signal.__getroodataset__(self.var, self.roovar, self.cuts, self.rooweight, self.mcweightbranch, self.xmin, self.xmax, self.lumi * injection, self.maxevents)
                    else:
                        signal.__getroodataset__(self.var, self.roovar, self.cuts, self.rooweight, self.mcweightbranch, self.xmin, self.xmax, self.lumi, self.maxevents)
                else:
                    signal.__gethist__(self.var, self.cuts, self.mcweightstring, self.nbins, self.xmin, self.xmax, self.forcebins)


        if dodata:
            for data in self.data:
                if not self.binned:            
                    data.__getroodataset__(self.var, self.roovar, self.cuts, self.rooweight, 1.0, self.xmin, self.xmax, 1.0, self.maxevents)
                else: 
                    data.__gethist__(self.var, self.cuts, 1.0, self.nbins, self.xmin, self.xmax, self.forcebins)


        # Create roovar and dataset
        # For binned, think there is a bug in the RooDataHist map ctor as doubles number of entries, giving half bin width and half entries 0
        # work arround by using TH1 version -> hence save hist too

        if self.binned:
            if not dodata:
                self.hist = hdata = self.signals[0].hist
                self.hist.SetName("hist")
                #self.roodata = hdata
                self.roodata = R.RooDataHist("data", "{} ({})".format(hdata.GetTitle(), self.name), R.RooArgList(self.roovar), RF.Import(hdata, 0))
            else: 
                hdata = self.data[0].hist

                if injection:
                    # Add signal with correct stength
                    hsig = self.signals[0].hist
                    hsig.Scale(injection)
                    hdata.Add(hsig)

                # Convert hist to RooDataHist over the RooRealVar
                self.hist = hdata
                self.hist.SetName("hist")
                self.roodata = R.RooDataHist("data", "{} ({})".format(hdata.GetTitle(), self.name), R.RooArgList(self.roovar), RF.Import(hdata, 0))
                #self.roodata = R.RooDataHist("data", "{} ({})".format(hdata.GetTitle(), self.name), R.RooArgList(self.roovar), hdata)                

        else:
            if not dodata:
                self.roodata = self.signals[0].dataset
            else:
                self.roodata = self.data[0].dataset            
            
                if injection:
                    # Add signal with stength set in apply_selection
                    sig = self.signals[0].dataset
                    self.roodata.append(sig)

        return
    

    def add_pdf(self, **kwargs):
        "Add PDFs based on name.  All kwargs are passed to the PDF"

        
        name = kwargs["name"]

        self.log.info("Adding PDF " + name)

        kwargs["var"] = self.roovar
        kwargs["label"] = self.name

        if "file" in kwargs:
            pdf = PDFFile(**kwargs)
        elif name == "cheb":
            pdf = PDFChebychev(**kwargs)
        elif name == "poly":
            pdf = PDFPolynomial(**kwargs)
        elif name == "exp":
            pdf = PDFExp(**kwargs)
        elif name == "gaus":
            pdf = PDFGauss(**kwargs)
        elif name == "CB":
            pdf = PDFCB(**kwargs)            
        elif name == "double-gaus":
            pdf = PDF2Gauss(**kwargs)
        elif name == "CB-gaus":
            pdf = PDFCBAndGauss(**kwargs)
        elif name == "DSCB":
            pdf = PDFDSCB(**kwargs)            
#         elif name == "signal":
#             h = self.signals[0].hist
#             if self.injection:
#                 h.Scale(1/self.injection)
#             kwargs["hist"] = h
#             pdf = PDFHist(**kwargs)
        else:
            raise PDFError("No such PDF {}".format(kwargs[name]))

        # Add region name to PDF to make unique model per region
        pdf.name += "_" + self.name
        self.pdf_comps.append(pdf)

        return

    def build_model(self, decorr_poi = False):
        "Build the model from the PDF, adding them if more than 1 PDF"

        self.log.info("Building model")

        for p in self.pdf_comps:            
            p.correlated = self.corr_params

        if len(self.pdf_comps) == 1:
            self.model = self.pdf_comps[0].build()
        else:
            self.model = PDFAdd(self.pdf_comps).build(decorr_poi)

        #self.model.Print()

        return self.model   
